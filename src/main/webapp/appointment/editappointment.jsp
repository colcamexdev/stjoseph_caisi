<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="org.oscarehr.casemgmt.service.CaseManagementManager"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_appointment" rights="u" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_appointment");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>

<%@page import="org.oscarehr.common.dao.ProviderDataDao"%>
<%@page import="org.oscarehr.managers.DemographicManager"%>

<%@page import="oscar.appt.status.service.impl.AppointmentStatusMgrImpl"%>
<%
  if (session.getAttribute("user") == null)    response.sendRedirect("../logout.jsp");
  String curProvider_no = request.getParameter("provider_no");
  String appointment_no = request.getParameter("appointment_no");
  String curUser_no = (String) session.getAttribute("user");
  String userfirstname = (String) session.getAttribute("userfirstname");
  String userlastname = (String) session.getAttribute("userlastname");
  String deepcolor = "#CCCCFF", weakcolor = "#EEEEFF";
  String origDate = null;

  boolean bFirstDisp = true; //this is the first time to display the window
  if (request.getParameter("bFirstDisp")!=null) bFirstDisp = (request.getParameter("bFirstDisp")).equals("true");
%>

<%@page import="oscar.oscarDemographic.data.*, java.util.*, java.sql.*, oscar.appt.*, oscar.*, oscar.util.*, java.text.*, java.net.*, org.oscarehr.common.OtherIdManager"%>
<%@ page import="oscar.appt.status.service.AppointmentStatusMgr"%>
<%@ page import="org.oscarehr.common.model.AppointmentStatus"%>
<%@page import="org.oscarehr.common.dao.BillingONCHeader1Dao"%>
<%@page import="org.oscarehr.common.model.BillingONCHeader1"%>
<%@ page import="org.oscarehr.common.dao.DemographicDao, org.oscarehr.PMmodule.dao.ProviderDao, org.oscarehr.common.model.*, org.oscarehr.util.SpringUtils"%>
<%@ page import="oscar.oscarEncounter.data.EctFormData"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<jsp:useBean id="providerBean" class="java.util.Properties" scope="session" />
<%@page import="org.oscarehr.common.model.DemographicCust" %>
<%@page import="org.oscarehr.common.dao.DemographicCustDao" %>
<%@ page import="org.oscarehr.common.model.EncounterForm" %>
<%@ page import="org.oscarehr.common.dao.EncounterFormDao" %>
<%@page import="org.oscarehr.common.model.ProviderPreference"%>
<%@page import="org.oscarehr.common.model.ProviderData"%>
<%@page import="org.oscarehr.util.SessionConstants"%>
<%@page import="org.oscarehr.common.model.Appointment" %>
<%@page import="org.oscarehr.common.dao.OscarAppointmentDao" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.PMmodule.model.Program" %>
<%@ page import="org.oscarehr.PMmodule.model.ProgramProvider" %>
<%@ page import="org.oscarehr.common.model.Facility" %>
<%@ page import="org.oscarehr.PMmodule.service.ProviderManager" %>
<%@ page import="org.oscarehr.PMmodule.service.ProgramManager" %>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="org.oscarehr.managers.LookupListManager"%>
<%@ page import="org.oscarehr.common.model.LookupList"%>
<%@ page import="org.oscarehr.common.model.LookupListItem"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="oscar.oscarBilling.ca.on.data.BillingDataHlp" %>
<%@page import="org.oscarehr.common.dao.BillingONExtDao" %>
<%@page import="org.oscarehr.billing.CA.ON.dao.*" %>
<%@page import="java.math.*" %>
<%
    String mrpName = "";
	DemographicCustDao demographicCustDao = (DemographicCustDao)SpringUtils.getBean("demographicCustDao");
	EncounterFormDao encounterFormDao = SpringUtils.getBean(EncounterFormDao.class);
    ProviderPreference providerPreference=(ProviderPreference)session.getAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE);
    DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
    OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
    ProviderDataDao providerDao = SpringUtils.getBean(ProviderDataDao.class);
    SiteDao siteDao = SpringUtils.getBean(SiteDao.class);
	ProviderDao pDao = SpringUtils.getBean(ProviderDao.class);
	BillingONCHeader1Dao cheader1Dao = (BillingONCHeader1Dao)SpringUtils.getBean("billingONCHeader1Dao"); 
	
    ProviderManager providerManager = SpringUtils.getBean(ProviderManager.class);
	ProgramManager programManager = SpringUtils.getBean(ProgramManager.class);
	//String demographic_nox = (String)session.getAttribute("demographic_nox");
	String demographic_nox = request.getParameter("demographic_no");
	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
    Demographic demographicTmp=demographicManager.getDemographic(loggedInInfo,demographic_nox);
    String proNoTmp = demographicTmp==null?null:demographicTmp.getProviderNo();
    if (demographicTmp!=null&&proNoTmp!=null&&proNoTmp.length()>0) {
            Provider providerTmp=pDao.getProvider(demographicTmp.getProviderNo());
        if (providerTmp != null) {
            mrpName = providerTmp.getFormattedName();
        }
    }
	
	String providerNo = loggedInInfo.getLoggedInProviderNo();
	Facility facility = loggedInInfo.getCurrentFacility();
	
    List<Program> programs = programManager.getActiveProgramByFacility(providerNo, facility.getId());

    LookupListManager lookupListManager = SpringUtils.getBean(LookupListManager.class);
    LookupList reasonCodes = lookupListManager.findLookupListByName(loggedInInfo, "reasonCode");
	pageContext.setAttribute("reasonCodes", reasonCodes);
	
    ApptData apptObj = ApptUtil.getAppointmentFromSession(request);

 List<BillingONCHeader1> cheader1s = null;
 if("ON".equals(OscarProperties.getInstance().getProperty("billregion", "ON"))) {
 	cheader1s = cheader1Dao.getBillCheader1ByDemographicNo(Integer.parseInt(demographic_nox));
 }
 
 BillingONExtDao billingOnExtDao = (BillingONExtDao)SpringUtils.getBean(BillingONExtDao.class);
    oscar.OscarProperties pros = oscar.OscarProperties.getInstance();
    String strEditable = pros.getProperty("ENABLE_EDIT_APPT_STATUS");
  String apptStatusHere = pros.getProperty("appt_status_here");

    AppointmentStatusMgr apptStatusMgr =  new AppointmentStatusMgrImpl();
    List allStatus = apptStatusMgr.getAllActiveStatus();

    Boolean isMobileOptimized = session.getAttribute("mobileOptimized") != null;
    
    String useProgramLocation = OscarProperties.getInstance().getProperty("useProgramLocation");
    String moduleNames = OscarProperties.getInstance().getProperty("ModuleNames");
    boolean caisiEnabled = moduleNames != null && org.apache.commons.lang.StringUtils.containsIgnoreCase(moduleNames, "Caisi");
    boolean locationEnabled = caisiEnabled && (useProgramLocation != null && useProgramLocation.equals("true"));

	String annotation_display = org.oscarehr.casemgmt.model.CaseManagementNoteLink.DISP_APPOINTMENT;
	CaseManagementManager caseManagementManager = (CaseManagementManager) SpringUtils.getBean("caseManagementManager");
%>
<%@page import="org.oscarehr.common.dao.SiteDao"%>
<%@page import="org.oscarehr.common.model.Site"%><html:html locale="true">
<head>
<% if (isMobileOptimized) { %>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, width=device-width" />
    <link rel="stylesheet" href="../mobile/appointmentstyle.css" type="text/css">
<% } else { %>
    <link rel="stylesheet" href="appointmentstyle.css" type="text/css">
    <style type="text/css">
        .deep { background-color: <%= deepcolor %>; }
        .weak { background-color: <%= weakcolor %>; }
    </style>
<% } %>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<title><bean:message key="appointment.editappointment.title" /></title>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>
   <script>
     jQuery.noConflict();
   </script>
<oscar:customInterface section="editappt"/>
<script language="javascript">
<!-- // start javascript
function toggleView() {
    showHideItem('editAppointment');
    showHideItem('viewAppointment');
}
function demographicdetail(vheight,vwidth) {
  if(document.forms['EDITAPPT'].demographic_no.value=="") return;
  self.close();
  var page = "../demographic/demographiccontrol.jsp?demographic_no=" + document.forms['EDITAPPT'].demographic_no.value+"&displaymode=edit&dboperation=search_detail";
  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=600,screenY=200,top=0,left=0";
  var popup=window.open(page, "demographic", windowprops);
}
function onButRepeat() {
	if(calculateEndTime()) {
		document.forms[0].action = "appointmenteditrepeatbooking.jsp" ;
		document.forms[0].submit();
	}
}

var saveTemp=0;

function setfocus() {
  this.focus();
  document.EDITAPPT.keyword.focus();
  document.EDITAPPT.keyword.select();
}

function onBlockFieldFocus(obj) {
  obj.blur();
  document.EDITAPPT.keyword.focus();
  document.EDITAPPT.keyword.select();
  window.alert("<bean:message key="Appointment.msgFillNameField"/>");
}
function labelprint(vheight,vwidth,varpage) {
  var page = "" + varpage;
  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=600,screenY=200,top=0,left=0";
  var popup=window.open(page, "encounterhist", windowprops);
}
function onButDelete() {
  saveTemp=1;
}
function onButUpdate() {
  saveTemp=2;
}

function onButCancel(){
   var aptStat = document.EDITAPPT.status.value;
   if (aptStat.indexOf('B') == 0){
       var agree = confirm("<bean:message key="appointment.editappointment.msgCanceledBilledConfirmation"/>") ;
       if (agree){
          window.location='appointmentcontrol.jsp?buttoncancel=Cancel Appt&displaymode=Update Appt&appointment_no=<%=appointment_no%>';
       }
   }else{
      window.location='appointmentcontrol.jsp?buttoncancel=Cancel Appt&displaymode=Update Appt&appointment_no=<%=appointment_no%>';
   }
}
function upCaseCtrl(ctrl) {
	ctrl.value = ctrl.value.toUpperCase();
}
function onSub() {
  if( saveTemp==1 ) {
    var aptStat = document.EDITAPPT.status.value;
    if (aptStat.indexOf('B') == 0){
       return (confirm("<bean:message key="appointment.editappointment.msgDeleteBilledConfirmation"/>")) ;
    }else{
       return (confirm("<bean:message key="appointment.editappointment.msgDeleteConfirmation"/>")) ;
    }
  }
  if( saveTemp==2 ) {
    if (document.EDITAPPT.notes.value.length > 255) {
      window.alert("<bean:message key="appointment.editappointment.msgNotesTooBig"/>");
      return false;
    }
    return calculateEndTime() ;
  } else
      return true;
}



function calculateEndTime() {
  var stime = document.EDITAPPT.start_time.value;
  var vlen = stime.indexOf(':')==-1?1:2;

  if(vlen==1 && stime.length==4 ) {
    document.EDITAPPT.start_time.value = stime.substring(0,2) +":"+ stime.substring(2);
    stime = document.EDITAPPT.start_time.value;
  }
  
  if(stime.length!=5) {
    alert("<bean:message key="Appointment.msgInvalidDateFormat"/>");
    return false;
  }

  var shour = stime.substring(0,2) ;
  var smin = stime.substring(stime.length-vlen) ;
  var duration = document.EDITAPPT.duration.value ;
  
  if(isNaN(duration)) {
	  alert("<bean:message key="Appointment.msgFillTimeField"/>");
	  return false;
  }
  
  if(eval(duration) == 0) { duration =1; }
  if(eval(duration) < 0) { duration = Math.abs(duration) ; }

  var lmin = eval(smin)+eval(duration)-1 ;
  var lhour = parseInt(lmin/60);

  if((lmin) > 59) {
    shour = eval(shour) + eval(lhour);
    shour = shour<10?("0"+shour):shour;
    smin = lmin - 60*lhour;
  } else {
    smin = lmin;
  }
  smin = smin<10?("0"+ smin):smin;
  document.EDITAPPT.end_time.value = shour +":"+ smin;
  if(shour > 23) {
    alert("<bean:message key="Appointment.msgCheckDuration"/>");
    return false;
  }
  return true;
}

function checkTypeNum(typeIn) {
	var typeInOK = true;
	var i = 0;
	var length = typeIn.length;
	var ch;

	// walk through a string and find a number
	if (length>=1) {
	  while (i <  length) {
		  ch = typeIn.substring(i, i+1);
		  if (ch == ":") { i++; continue; }
		  if ((ch < "0") || (ch > "9") ) {
			  typeInOK = false;
			  break;
		  }
	    i++;
      }
	} else typeInOK = false;
	return typeInOK;
}

function checkTimeTypeIn(obj) {
  var colonIdx;
  if(!checkTypeNum(obj.value) ) {
	  alert ("<bean:message key="Appointment.msgFillTimeField"/>");
  } else {
      colonIdx = obj.value.indexOf(':');
      if(colonIdx==-1) {
        if(obj.value.length < 3) alert("<bean:message key="Appointment.msgFillValidTimeField"/>");
        obj.value = obj.value.substring(0, obj.value.length-2 )+":"+obj.value.substring( obj.value.length-2 );
  }
}
          
  var hours = "";
  var minutes = "";  

  colonIdx = obj.value.indexOf(':');  
  if (colonIdx < 1)
      hours = "00";     
  else if (colonIdx == 1)
      hours = "0" + obj.value.substring(0,1);
  else
      hours = obj.value.substring(0,2);
  
  minutes = obj.value.substring(colonIdx+1,colonIdx+3);
  if (minutes.length == 0)
	    minutes = "00";
	else if (minutes.length == 1)
		minutes = "0" + minutes;
	else if (minutes > 59)
		minutes = "00";

  obj.value = hours + ":" + minutes;    
}

<% if (apptObj!=null) { %>
function pasteAppt(multipleSameDayGroupAppt) {

        var warnMsgId = document.getElementById("tooManySameDayGroupApptWarning");

        if (multipleSameDayGroupAppt) {
           warnMsgId.style.display = "block";
           if (document.EDITAPPT.updateButton) {
              document.EDITAPPT.updateButton.style.display = "none";
           }
           if (document.EDITAPPT.groupButton) {
              document.EDITAPPT.groupButton.style.display = "none";
           }
           if (document.EDITAPPT.deleteButton){
              document.EDITAPPT.deleteButton.style.display = "none";
           }
           if (document.EDITAPPT.cancelButton){
              document.EDITAPPT.cancelButton.style.display = "none";
           }
           if (document.EDITAPPT.noShowButton){
              document.EDITAPPT.noShowButton.style.display = "none";
           }
           if (document.EDITAPPT.labelButton) {
                document.EDITAPPT.labelButton.style.display = "none";
           }
           if (document.EDITAPPT.repeatButton) {
                document.EDITAPPT.repeatButton.style.display = "none";
           }
        }
        //else {
        //   warnMsgId.style.display = "none";
        //}
	document.EDITAPPT.status.value = "<%=apptObj.getStatus()%>";
	document.EDITAPPT.duration.value = "<%=apptObj.getDuration()%>";
	document.EDITAPPT.chart_no.value = "<%=apptObj.getChart_no()%>";
	document.EDITAPPT.keyword.value = "<%=apptObj.getName()%>";
	document.EDITAPPT.demographic_no.value = "<%=apptObj.getDemographic_no()%>";
	document.forms[0].reason.value = "<%= StringEscapeUtils.escapeJavaScript(apptObj.getReason()) %>"; 
        document.forms[0].notes.value = "<%= StringEscapeUtils.escapeJavaScript(apptObj.getNotes()) %>"; 
	document.EDITAPPT.location.value = "<%=apptObj.getLocation()%>";
	document.EDITAPPT.resources.value = "<%=apptObj.getResources()%>";
	document.EDITAPPT.type.value = "<%=apptObj.getType()%>";
	if('<%=apptObj.getUrgency()%>' == 'critical') {
		document.EDITAPPT.urgency.checked = "checked";
	}
}
<% } %>
function onCut() {
  document.EDITAPPT.submit();
}


function openTypePopup () {
    windowprops = "height=170,width=500,location=no,scrollbars=no,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=100,left=100";
    var popup=window.open("appointmentType.jsp?type="+document.forms['EDITAPPT'].type.value, "Appointment Type", windowprops);
    if (popup != null) {
      if (popup.opener == null) {
        popup.opener = self;
      }
      popup.focus();
    }
}

function setType(typeSel,reasonSel,locSel,durSel,notesSel,resSel) {
  document.forms['EDITAPPT'].type.value = typeSel;
  document.forms['EDITAPPT'].reason.value = reasonSel;
  document.forms['EDITAPPT'].duration.value = durSel;
  document.forms['EDITAPPT'].notes.value = notesSel;
  document.forms['EDITAPPT'].duration.value = durSel;
  document.forms['EDITAPPT'].resources.value = resSel;
  var loc = document.forms['EDITAPPT'].location;
  if(loc.nodeName == 'SELECT') {
          for(c = 0;c < loc.length;c++) {
                  if(loc.options[c].innerHTML == locSel) {
                          loc.selectedIndex = c;
                          loc.style.backgroundColor=loc.options[loc.selectedIndex].style.backgroundColor;
                          break;
                  }
          }
  } else if (loc.nodeName == "INPUT") {
	  document.forms['EDITAPPT'].location.value = locSel;
  }
}

// stop javascript -->
</script>
</head>
<body onload="setfocus()" bgproperties="fixed"
      topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
<!-- The mobile optimized page is split into two sections: viewing and editing an appointment
     In the mobile version, we only display the edit section first if we are returning from a search -->
<div id="editAppointment" style="display:<%= (isMobileOptimized && bFirstDisp) ? "none":"block"%>;">
<FORM NAME="EDITAPPT" METHOD="post" ACTION="appointmentcontrol.jsp"
	onSubmit="return(onSub())"><INPUT TYPE="hidden"
	NAME="displaymode" value="">
    <div class="header deep">
        <div class="title">
            <!-- We display a shortened title for the mobile version -->
            <% if (isMobileOptimized) { %><bean:message key="appointment.editappointment.msgMainLabelMobile" />
            <% } else { %><bean:message key="appointment.editappointment.msgMainLabel" />
            <% } %>
        </div>
        <a href="javascript:toggleView();" id="viewButton" class="leftButton top">
            <bean:message key="appointment.editappointment.btnView" />
        </a>
    </div>

<%
	Appointment appt = null;
	String demono="", chartno="", phone="", rosterstatus="", alert="", doctorNo="";
	String strApptDate = bFirstDisp?"":request.getParameter("appointment_date") ;


	if (bFirstDisp) {
		appt = appointmentDao.find(Integer.parseInt(appointment_no));
		pageContext.setAttribute("appointment", appt);

		if (appt == null) {
%>
<bean:message key="appointment.editappointment.msgNoSuchAppointment" />
<%
			return;
		}
	}


	if (bFirstDisp) {
		demono = String.valueOf(appt.getDemographicNo());
	} else if (request.getParameter("demographic_no")!=null && !request.getParameter("demographic_no").equals("")) {
		demono = request.getParameter("demographic_no");
	}

	//get chart_no from demographic table if it exists
	if (!demono.equals("0") && !demono.equals("")) {
		Demographic d = demographicManager.getDemographic(loggedInInfo, demono);
		if(d != null) {
			chartno = d.getChartNo();
			phone = d.getPhone();
			rosterstatus = d.getRosterStatus();
		}
   		
		DemographicCust demographicCust = demographicCustDao.find(Integer.parseInt(demono));
		if(demographicCust != null) {
			alert = demographicCust.getAlert();
		}

	}

        OscarProperties props = OscarProperties.getInstance();
        String displayStyle="display:none";
        String myGroupNo = providerPreference.getMyGroupNo();
        boolean bMultipleSameDayGroupAppt = false;
        if (props.getProperty("allowMultipleSameDayGroupAppt", "").equalsIgnoreCase("no")) {

            if (!bFirstDisp && !demono.equals("0") && !demono.equals("")) {
                String [] sqlParam = new String[3] ;
                sqlParam[0] = myGroupNo; //schedule group
                sqlParam[1] = demono;
                sqlParam[2] = strApptDate;

               List<Appointment> aa = appointmentDao.search_group_day_appt(myGroupNo,Integer.parseInt(demono),ConversionUtils.fromDateString(strApptDate));
                
                long numSameDayGroupAppts = aa.size() > 0 ? new Long(aa.size()) : 0;
                bMultipleSameDayGroupAppt = (numSameDayGroupAppts > 0);
            }

            if (bMultipleSameDayGroupAppt){
                displayStyle="display:block";
            }
%>

<div id="tooManySameDayGroupApptWarning" style="<%=displayStyle%>">
    <table width="98%" BGCOLOR="red" border=1 align='center'>
        <tr>
            <th>
                <font color='white'>
                    <bean:message key='appointment.addappointment.titleMultipleGroupDayBooking'/><br/>
                    <bean:message key='appointment.addappointment.MultipleGroupDayBooking'/>
                </font>
            </th>
        </tr>
    </table>
</div>
 <%
        }

    //RJ 07/12/2006
    //If page is loaded first time hit db for patient's family doctor
    //Else if we are coming back from search this has been done for us
    //Else how did we get here?
    if( bFirstDisp ) {
    		oscar.oscarDemographic.data.DemographicData dd = new oscar.oscarDemographic.data.DemographicData();
        org.oscarehr.common.model.Demographic demo = dd.getDemographic(loggedInInfo, String.valueOf(appt.getDemographicNo()));
        doctorNo = demo!=null ? (demo.getProviderNo()) : "";
    } else if (!request.getParameter("doctor_no").equals("")) {
        doctorNo = request.getParameter("doctor_no");
    }
%>

 <%
    	if(appt != null && !StringUtils.isEmpty(appt.getProviderNo())) {
     		ProviderData prov = providerDao.find(appt.getProviderNo());
     		if(prov != null) {
   				String providerName = prov.getLastName() + ","+ prov.getFirstName();
   		%>
		<div>
		    <table width="100%" BGCOLOR="lightblue" border=1 align='center'>
		        <tr>
		            <th>
		                <font>
		   					<%=providerName %>
		                </font>
		            </th>
		        </tr>
		    </table>
		</div>	                    	
<%}
	}
%>
                    


<div class="panel">
    <ul>
        <li class="row weak">
            <div class="label">
                <bean:message key="Appointment.formDate" />:
            </div>
            <div class="input">
		<INPUT TYPE="TEXT"
					NAME="appointment_date"
					VALUE="<%=bFirstDisp?ConversionUtils.toDateString(appt.getAppointmentDate()):strApptDate%>"
                    WIDTH="25" HEIGHT="20" border="0">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formStatus" />:</div>
            <div class="input">
<%
              String statusCode = request.getParameter("status");
			  String importedStatus = null;
              if (bFirstDisp){
                  statusCode =appt.getStatus();
                  importedStatus = appt.getImportedStatus();
              }


              String signOrVerify = "";
              if (statusCode.length() >= 2){
                  signOrVerify = statusCode.substring(1,2);
                  statusCode = statusCode.substring(0,1);
              }
              if (strEditable!=null && strEditable.equalsIgnoreCase("yes")) { %>
                <select name="status" STYLE="width: 154px">
					<% for (int i = 0; i < allStatus.size(); i++) { %>
					<option
						value="<%=((AppointmentStatus)allStatus.get(i)).getStatus()+signOrVerify%>"
						<%=((AppointmentStatus)allStatus.get(i)).getStatus().equals(statusCode)?"SELECTED":""%>><%=((AppointmentStatus)allStatus.get(i)).getDescription()%></option>
					<% } %>
				</select> <%
              } else {
              	if (importedStatus==null || importedStatus.trim().equals("")) { %>
              	<INPUT TYPE="TEXT" NAME="status" VALUE="<%=statusCode%>" WIDTH="25"> <%
              	} else { %>
                <INPUT TYPE="TEXT" NAME="status" VALUE="<%=statusCode%>" WIDTH="25">
                <INPUT TYPE="TEXT" TITLE="Imported Status" VALUE="<%=importedStatus%>" WIDTH="25" readonly> <%
              	}
              }
%>
            </div>
        </li>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formStartTime" />:</div>
            <div class="input">
                <INPUT TYPE="TEXT"
					NAME="start_time"
					VALUE="<%=bFirstDisp?ConversionUtils.toTimeStringNoSeconds(appt.getStartTime()):request.getParameter("start_time")%>"
                    WIDTH="25"
                    HEIGHT="20" border="0" onChange="checkTimeTypeIn(this)" >
            </div>
            <div class="space">&nbsp;</div>

	    <div class="label">
            <%
                        // multisites start ==================
                boolean bMultisites = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable();

            
            List<Site> sites = siteDao.getActiveSitesByProviderNo((String) session.getAttribute("user"));
            // multisites end ==================

            boolean bMoreAddr = bMultisites? true : props.getProperty("scheduleSiteID", "").equals("") ? false : true;

            String loc = bFirstDisp?(appt.getLocation()):request.getParameter("location");
            String colo = bMultisites
                                        ? ApptUtil.getColorFromLocation(sites, loc)
                                        : bMoreAddr? ApptUtil.getColorFromLocation(props.getProperty("scheduleSiteID", ""), props.getProperty("scheduleSiteColor", ""),loc) : "white";
            %>
            
				<INPUT TYPE="button" NAME="typeButton" VALUE="<bean:message key="Appointment.formType"/>" onClick="openTypePopup()">

            </div>

            <div class="input">
                <INPUT TYPE="TEXT" NAME="type"
					VALUE="<%=bFirstDisp?appt.getType():request.getParameter("type")%>"
                    WIDTH="25">
            </div>
        </li>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formDuration" />:</div>
            <div class="input">
				<%
  int everyMin = 1;
  StringBuilder nameSb = new StringBuilder();
  if(bFirstDisp) {
	  Calendar startCal = Calendar.getInstance();
	  startCal.setTime(appt.getStartTime());
	  Calendar endCal = Calendar.getInstance();
	  endCal.setTime(appt.getEndTime());
	  
    int endtime = (endCal.get(Calendar.HOUR_OF_DAY) )*60 + (endCal.get(Calendar.MINUTE)) ;
    int starttime = (startCal.get(Calendar.HOUR_OF_DAY) )*60 + (startCal.get(Calendar.MINUTE)) ;
    everyMin = endtime - starttime +1;

    if (!demono.equals("0") && !demono.equals("") && (demographicManager != null)) {
        Demographic demo = demographicManager.getDemographic(loggedInInfo, demono);
        nameSb.append(demo.getLastName())
              .append(",")
              .append(demo.getFirstName());
    }
    else {
        nameSb.append(appt.getName());
    }
  }
%> <INPUT TYPE="hidden" NAME="end_time"
					VALUE="<%=bFirstDisp?ConversionUtils.toTimeStringNoSeconds(appt.getEndTime()):request.getParameter("end_time")%>"
					WIDTH="25" HEIGHT="20" border="0" onChange="checkTimeTypeIn(this)">
				<%--              <INPUT TYPE="hidden" NAME="end_time" VALUE="<%=request.getParameter("end_time")%>" WIDTH="25" HEIGHT="20" border="0" onChange="checkTimeTypeIn(this)">--%>
				<INPUT TYPE="TEXT" NAME="duration"
					VALUE="<%=request.getParameter("duration")!=null?(request.getParameter("duration").equals(" ")||request.getParameter("duration").equals("")||request.getParameter("duration").equals("null")?(""+everyMin) :request.getParameter("duration")):(""+everyMin)%>"
                    WIDTH="25">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formChartNo" />:</div>
            <div class="input">
                <input type="TEXT" name="chart_no"
                    readonly value="<%=bFirstDisp?StringUtils.trimToEmpty(chartno):request.getParameter("chart_no")%>"
                    width="25">
            </div>
        </li>
			<% if(providerBean != null && doctorNo != null && providerBean.getProperty(doctorNo)!=null) {%>
        <li class="row weak">
            <div class="label"></div>
            <div class="input"></div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formDoctor" />:</div>
            <div class="input">
                <INPUT type="TEXT" name="doctorNo" readonly
                   value="<%=providerBean.getProperty(doctorNo)%>"
                   width="25">
            </div>
        </li>
			<% } %>
        <li class="row weak">
            <div class="label"><a href="#"onclick="demographicdetail(550,700)">
                    <bean:message key="Appointment.formName" /></a>:
            </div>
            <div class="input">
                <INPUT TYPE="TEXT" NAME="keyword"
					tabindex="1"
					VALUE="<%=bFirstDisp?nameSb.toString():request.getParameter("name")%>"
                    width="25">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label">
		<INPUT TYPE="hidden" NAME="orderby" VALUE="last_name, first_name">
<%
    String searchMode = request.getParameter("search_mode");
    if (searchMode == null || searchMode.isEmpty()) {
        searchMode = OscarProperties.getInstance().getProperty("default_search_mode","search_name");
    }
%>
                <INPUT TYPE="hidden" NAME="search_mode" VALUE="<%=searchMode%>">
                <INPUT TYPE="hidden" NAME="originalpage" VALUE="../appointment/editappointment.jsp">
                <INPUT TYPE="hidden" NAME="limit1" VALUE="0">
                <INPUT TYPE="hidden" NAME="limit2" VALUE="5">
                <INPUT TYPE="hidden" NAME="ptstatus" VALUE="active">
                <!--input type="hidden" name="displaymode" value="Search " -->
                <input type="submit" style="width:auto;"
					onclick="document.forms['EDITAPPT'].displaymode.value='Search '"
                    value="<bean:message key="appointment.editappointment.btnSearch"/>">
            </div>
            <div class="input">
                <input type="TEXT"
					name="demographic_no" onFocus="onBlockFieldFocus(this)" readonly
					value="<%=bFirstDisp?( (appt.getDemographicNo())==0?"":(""+appt.getDemographicNo()) ):request.getParameter("demographic_no")%>"
                    width="25">
            </div>
        </li>
        <li class="row weak">
            <div class="label">Coded Reason:</div>
            <div class="input">
				<select name="reasonCode">
				<%
				String rCode = bFirstDisp && appt.getReasonCode() != null ?appt.getReasonCode().toString():request.getParameter("reasonCode");
				pageContext.setAttribute("rCode",rCode);
				
				%>				
					<c:choose>
	                	<c:when test="${ not empty reasonCodes  }">
	                		<c:forEach items="${ reasonCodes.items }" var="reason" >
	                		<c:if test="${ reason.active }">
	                			<option value="${ reason.id }" id="${ reason.value }" ${ rCode eq reason.id ? 'selected="selected"' : '' } >
	                				<c:out value="${ reason.label }" />
	                			</option>
	                		</c:if>
	                		</c:forEach>     
	                	</c:when>
	                	<c:otherwise>
	                		<option value="-1">Other</option>
	                	</c:otherwise>
	                </c:choose>
				</select>
 				
            </div>
            <div class="space">&nbsp;</div>
            <input type="hidden" name="oceanReferralRef" value="<%=appt.getOceanReferralRef() %>"/>
            <%
            	if(!StringUtils.isEmpty(appt.getOceanReferralRef())) {
            		%>
            		 <div class="label">eReferral:</div>
            <div class="input">
				CONNECTED
            </div>
            		<%
            	} else {
            		%>
            		 <div class="label">&nbsp;</div>
            <div class="input">
				&nbsp;
            </div>
            		<%
            	}
   
            %>
           
        </li>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formReason" />:</div>
            <div class="input">
				<textarea id="reason" name="reason" tabindex="2" rows="2" wrap="virtual"
					cols="18"><%=bFirstDisp?appt.getReason():request.getParameter("reason")%></textarea>
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formNotes" />:</div>
            <div class="input">
				<textarea name="notes" tabindex="3" rows="2" wrap="virtual"
					cols="18"><%=bFirstDisp?appt.getNotes():request.getParameter("notes")%></textarea>
            </div>
        </li>
			<% if (pros.isPropertyActive("mc_number")) {
		String mcNumber = OtherIdManager.getApptOtherId(appointment_no, "appt_mc_number");
%>
        <li class="row weak">
            <div class="label">M/C number :</div>
            <div class="input">
                <input type="text" name="appt_mc_number" tabindex="4"
                    value="<%=bFirstDisp?mcNumber:request.getParameter("appt_mc_number")%>" />
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"></div>
            <div class="input"></div>
        </li>
<% } %>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formLocation" />:</div>
            <div class="input">

<% // multisites start ==================
boolean isSiteSelected = false;
if (bMultisites) { %>
				<select tabindex="4" name="location" style="background-color: <%=colo%>" onchange='this.style.backgroundColor=this.options[this.selectedIndex].style.backgroundColor'>
				<%
					StringBuilder sb = new StringBuilder();
					for (Site s:sites) {
						if (s.getName().equals(loc)) isSiteSelected = true; // added by vic
						sb.append("<option value=\"").append(s.getName()).append("\" style=\"background-color: ").append(s.getBgColor()).append("\" ").append(s.getName().equals(loc)?"selected":"").append(">").append(s.getName()).append("</option>");
					}
					if (isSiteSelected) {
						out.println(sb.toString());
					} else {
						out.println("<option value='"+loc+"'>"+loc+"</option>");
					}
				%>

				</select>
<% } else {
	isSiteSelected = true;
	// multisites end ==================
	if (locationEnabled) {
%>           					
		<select name="location" >
               <%
               String location = bFirstDisp?(appt.getLocation()):request.getParameter("location");
               if (programs != null && !programs.isEmpty()) {
		       	for (Program program : programs) {
		       	    String description = StringUtils.isBlank(program.getLocation()) ? program.getName() : program.getLocation();
		   	%>
		        <option value="<%=program.getId()%>" <%=(program.getId().toString().equals(location) ? "selected='selected'" : "") %>><%=StringEscapeUtils.escapeHtml(description)%></option>
		    <%	}
               }
		  	%>
           </select>
	<% } else { %>
		<INPUT TYPE="TEXT" NAME="location" tabindex="4" VALUE="<%=bFirstDisp?appt.getLocation():request.getParameter("location")%>" WIDTH="25">
	<% } %>           
<% } %>
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formResources" />:</div>
            <div class="input">
                <input type="TEXT"
					name="resources" tabindex="5"
					value="<%=bFirstDisp?appt.getResources():request.getParameter("resources")%>"
                    width="25">
            </div>
        </li>
        <li class="weak row">
            <div class="label">Creator:</div>
            <div class="input">
            <% String lastCreatorNo = bFirstDisp?(appt.getCreator()):request.getParameter("user_id"); %>
                <INPUT TYPE="TEXT" NAME="user_id" VALUE="<%=lastCreatorNo%>" readonly WIDTH="25">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formLastTime" />:</div>
            <div class="input">
				<%
                 origDate =  bFirstDisp ? ConversionUtils.toTimestampString(appt.getCreateDateTime()) : request.getParameter("createDate");
                 String lastDateTime = bFirstDisp?ConversionUtils.toTimestampString(appt.getUpdateDateTime()):request.getParameter("updatedatetime");
                 if (lastDateTime == null){ lastDateTime = bFirstDisp?ConversionUtils.toTimestampString(appt.getCreateDateTime()):request.getParameter("createdatetime"); }

				GregorianCalendar now=new GregorianCalendar();
				String strDateTime=now.get(Calendar.YEAR)+"-"+(now.get(Calendar.MONTH)+1)+"-"+now.get(Calendar.DAY_OF_MONTH)+" "
					+	now.get(Calendar.HOUR_OF_DAY)+":"+now.get(Calendar.MINUTE)+":"+now.get(Calendar.SECOND);

                 String remarks = "";
                 if (bFirstDisp && appt.getRemarks()!=null) {
                     remarks = appt.getRemarks();
                 }

%>
                <INPUT TYPE="TEXT" NAME="lastcreatedatetime" readonly
                    VALUE="<%=bFirstDisp?lastDateTime:request.getParameter("lastcreatedatetime")%>"
                    WIDTH="25">
                <INPUT TYPE="hidden" NAME="createdatetime" VALUE="<%=strDateTime%>">
				<INPUT TYPE="hidden" NAME="provider_no" VALUE="<%=curProvider_no%>">
				<INPUT TYPE="hidden" NAME="dboperation" VALUE="">
                <INPUT TYPE="hidden" NAME="creator" VALUE="<%=userlastname+", "+userfirstname%>">
                <INPUT TYPE="hidden" NAME="remarks" VALUE="<%=remarks%>">
                <INPUT TYPE="hidden" NAME="appointment_no" VALUE="<%=appointment_no%>">
            </div>
        </li>
        <%  lastCreatorNo = request.getParameter("user_id");
	if( bFirstDisp ) {
		if( appt.getLastUpdateUser() != null ) {
	    
	    	ProviderData provider = providerDao.findByProviderNo(appt.getLastUpdateUser());
	    	if( provider != null ) {
			lastCreatorNo = provider.getLastName() + ", " + provider.getFirstName();
	    	}
		}
		else {
		    lastCreatorNo = appt.getCreator();
		}
	}
%>  
      <li class="row weak">
      		<div class="label">&nbsp;</div>
            <div class="input">&nbsp;</div>
            <div class="space">&nbsp;</div>
            <div class="label">Last Editor:</div>
            <div class="input">
                <INPUT TYPE="TEXT" readonly
					VALUE="<%=lastCreatorNo%>" WIDTH="25">
            </div>           
        </li>
          
        <li class="row weak">
            <div class="label">Create Date:</div>
            <div class="input">
                <INPUT TYPE="TEXT" NAME="createDate" readonly
					VALUE="<%=origDate%>" WIDTH="25">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formCritical" />:</div>
            <div class="input">
            	<%
           			String urgencyChecked=new String();
            		if(bFirstDisp) {
            			if(appt.getUrgency() != null && appt.getUrgency().equals("critical")) {
            				urgencyChecked=" checked=\"checked\" ";
            			}
            		} else {
            			if(request.getParameter("urgency") != null) {
            				if(request.getParameter("urgency").equals("critical")) {
            					urgencyChecked=" checked=\"checked\" ";
            				}
            			}
            		}
            	%>
            	<input type="checkbox" name="urgency" value="critical" <%=urgencyChecked%>/>
            </div>
        </li>
        
        
         <li class="row weak">
			<div class="label">Referral Source Prior to Ambulatory Care Visit: </div>
            <div class="input">
            <%
           			String referralSource=new String();
            		if(bFirstDisp) {
            			if(appt.getReferralSource() != null) {
            				referralSource = appt.getReferralSource();
            			}
            		} else {
            			if(request.getParameter("referralSourcePriorToAmbulatoryCareVisit") != null) {
            				referralSource = request.getParameter("referralSourcePriorToAmbulatoryCareVisit");
            			}
            		}
            	%>
            	<select name="referralSourcePriorToAmbulatoryCareVisit">
					<option value=""></option>
					<option value="Self/family member, caretaker, guardian" <%=("Self/family member, caretaker, guardian".equals(referralSource)?" selected=\"selected\" " : "") %>>Self/family member, caretaker, guardian</option>
					<option value="Inpatient service (reporting or other facility)" <%=("Inpatient service (reporting or other facility)".equals(referralSource)?" selected=\"selected\" " : "") %>>Inpatient service (reporting or other facility)</option>
					<option value="Ambulatory care service (reporting or other facility)" <%=("Ambulatory care service (reporting or other facility)".equals(referralSource)?" selected=\"selected\" " : "") %>>Ambulatory care service (reporting or other facility)</option>
					<option value="Private practice" <%=("Private practice".equals(referralSource)?" selected=\"selected\" " : "") %>>Private practice</option>
					<option value="Drug dependency program" <%=("Drug dependency program".equals(referralSource)?" selected=\"selected\" " : "") %>>Drug dependency program</option>
					<option value="Community health service" <%=("Community health service".equals(referralSource)?" selected=\"selected\" " : "") %>>Community health service</option>
					<option value="Residential care facility" <%=("Residential care facility".equals(referralSource)?" selected=\"selected\" " : "") %>>Residential care facility</option>
					<option value="Legal service" <%=("Legal service".equals(referralSource)?" selected=\"selected\" " : "") %>>Legal service</option>
					<option value="Educational agency" <%=("Educational agency".equals(referralSource)?" selected=\"selected\" " : "") %>>Educational agency</option>
					<option value="Home care" <%=("Home care".equals(referralSource)?" selected=\"selected\" " : "") %>>Home care</option>
					<option value="Mental health facility" <%=("Mental health facility".equals(referralSource)?" selected=\"selected\" " : "") %>>Mental health facility</option>
					<option value="Other" <%=("Other".equals(referralSource)?" selected=\"selected\" " : "") %>>Other</option>
					<option value="Unknown/unavailable" <%=("Unknown/unavailable".equals(referralSource)?" selected=\"selected\" " : "") %>>Unknown/unavailable</option>
				</select>
            </div>
            
            <div class="space">&nbsp;</div>
			<div class="label">Date ready for rehab/Referral date (yyyy-mm-dd):</div>
            <div class="input">
            <%
           			String referralDate=new String();
            		if(bFirstDisp) {
            			if(appt.getReferralDate() != null) {
            				referralDate = appt.getReferralDate();
            			}
            		} else {
            			if(request.getParameter("referralDate") != null) {
            				referralDate = request.getParameter("referralDate");
            			}
            		}
            	%>            
            	<input type="text" name="referralDate" value="<%=referralDate%>"/>
            </div>
        </li>
         <li class="row weak">
			<div class="label">InstitutionFrom: </div>
            <div class="input">
            <%
           			String institutionFrom=new String();
            		if(bFirstDisp) {
            			if(appt.getInstitutionFrom() != null) {
            				institutionFrom = appt.getInstitutionFrom();
            			}
            		} else {
            			if(request.getParameter("institutionFrom") != null) {
            				institutionFrom = request.getParameter("institutionFrom");
            			}
            		}
            	%>
            	<select name="institutionFrom">
			<option value=""></option>
			<option value="4844" <%=("4844").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LAKERIDGE HEALTH-AJAX SITE</option>
			<option value="1870" <%=("1870").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GLENGARRY MEMORIAL HOSPITAL</option>
			<option value="1254" <%=("1254").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ALMONTE GENERAL HOSPITAL</option>
			<option value="1799" <%=("1799").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ARNPRIOR REGIONAL HEALTH</option>
			<option value="2147" <%=("2147").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ATIKOKAN GENERAL HOSPITAL</option>
			<option value="4702" <%=("4702").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WEENEEBAYKO AREA HLTH AUTH-ATTAWAPISKAT</option>
			<option value="3991" <%=("3991").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>QUINTE HEALTHCARE CORPORATION-BANCROFT</option>
			<option value="1825" <%=("1825").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ROYAL VICTORIA REGIONAL HEALTH CENTRE</option>
			<option value="1801" <%=("1801").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST FRANCIS MEMORIAL HOSPITAL</option>
			<option value="3988" <%=("3988").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>QUINTE HEALTHCARE CORPORATION-BELLEVILLE</option>
			<option value="2057" <%=("2057").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTH SHORE HLTH NTWRK-BLIND RIVER SITE</option>
			<option value="4619" <%=("4619").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>MUSKOKA ALGONQUIN HEALTHCARE-BRACEBRIDGE</option>
			<option value="4681" <%=("4681").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WILLIAM OSLER HEALTH SYSTEM-CIVIC SITE</option>
			<option value="4675" <%=("4675").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>BRANT COMMUNITY HEALTHCARE SYS-BRANTFORD</option>
			<option value="1273" <%=("1273").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>BROCKVILLE GENERAL HOSP-CHARLES ST SITE</option>
			<option value="1160" <%=("1160").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>JOSEPH BRANT HOSPITAL</option>
			<option value="1905" <%=("1905").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CAMBRIDGE MEMORIAL HOSPITAL</option>
			<option value="1829" <%=("1829").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CANADIAN ARMED FORCES BASE HOSPITAL</option>
			<option value="1597" <%=("1597").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CAMPBELLFORD MEMORIAL HOSPITAL</option>
			<option value="1256" <%=("1256").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CARLETON PLACE AND DISTRICT MEM HOSPITAL</option>
			<option value="2173" <%=("2173").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SERVICES DE SANTE DE CHAPLEAU HLTH SERV</option>
			<option value="1223" <%=("1223").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>PUBLIC GENERAL HOSP SOCIETY OF CHATHAM</option>
			<option value="4042" <%=("4042").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SOUTH BRUCE GREY HEALTH CENTRE-CHESLEY</option>
			<option value="4008" <%=("4008").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LAKERIDGE HEALTH -BOWMANVILLE</option>
			<option value="1199" <%=("1199").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CLINTON PUBLIC HOSPITAL</option>
			<option value="3860" <%=("3860").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTHUMBERLAND HILLS HOSPITAL</option>
			<option value="2078" <%=("2078").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LADY MINTO HOSPITAL (THE)</option>
			<option value="1833" <%=("1833").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>COLLINGWOOD GENERAL AND MARINE HOSPITAL</option>
			<option value="4451" <%=("4451").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CORNWALL COMMUNITY HOSPITAL</option>
			<option value="1803" <%=("1803").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>DEEP RIVER AND DISTRICT HOSPITAL</option>
			<option value="2103" <%=("2103").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>DRYDEN REGIONAL HEALTH CENTRE</option>
			<option value="1146" <%=("1146").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HALDIMAND WAR MEMORIAL HOSPITAL</option>
			<option value="4036" <%=("4036").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SOUTH BRUCE GREY HEALTH CENTRE-DURHAM</option>
			<option value="2058" <%=("2058").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST JOSEPH'S GENERAL HOSPITAL</option>
			<option value="2148" <%=("2148").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>RIVERSIDE HEALTH CARE FAC-EMO SITE</option>
			<option value="2204" <%=("2204").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ENGLEHART AND DISTRICT HOSPITAL</option>
			<option value="2174" <%=("2174").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ESPANOLA GENERAL HOSPITAL</option>
			<option value="1203" <%=("1203").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SOUTH HURON HOSPITAL</option>
			<option value="1936" <%=("1936").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GROVES MEMORIAL COMMUNITY HOSPITAL</option>
			<option value="4700" <%=("4700").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WEENEEBAYKO AREA HLTH AUTH-FORT ALBANY</option>
			<option value="4210" <%=("4210").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NIAGARA HEALTH SYSTEM-FORT ERIE DOUGLAS</option>
			<option value="2150" <%=("2150").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>RIVERSIDE HEALTH CARE FAC-LAVERENDRYE</option>
			<option value="2175" <%=("2175").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GERALDTON DISTRICT HOSPITAL</option>
			<option value="1206" <%=("1206").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ALEXANDRA MARINE AND GENERAL HOSPITAL</option>
			<option value="4788" <%=("4788").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HAMILTON HLTH SCIENCES CORP-WEST LINCOLN</option>
			<option value="1946" <%=("1946").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GUELPH GENERAL HOSPITAL</option>
			<option value="1149" <%=("1149").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WEST HALDIMAND GENERAL HOSPITAL</option>
			<option value="3737" <%=("3737").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HALIBURTON HIGHLANDS HLTH SERV CORP-HALI</option>
			<option value="4622" <%=("4622").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HALTON HEALTHCARE SERVICES CORP-GEORGETO</option>
			<option value="1982" <%=("1982").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HAMILTON HEALTH SCIENCES CORP-GENERAL</option>
			<option value="1983" <%=("1983").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HAMILTON HEALTH SCIENCES CORP-JURAVINSKI</option>
			<option value="1994" <%=("1994").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HAMILTON HEALTH SCIENCES CORP-MCMASTER</option>
			<option value="2003" <%=("2003").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST JOSEPH'S HEALTH CARE SYSTEM-HAMILTON</option>
			<option value="1124" <%=("1124").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HANOVER AND DISTRICT HOSPITAL</option>
			<option value="1777" <%=("1777").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HAWKESBURY AND DISTRICT GENERAL HOSPITAL</option>
			<option value="2082" <%=("2082").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HOPITAL NOTRE DAME HOSPITAL (HEARST)</option>
			<option value="2061" <%=("2061").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HORNEPAYNE COMMUNITY HOSPITAL</option>
			<option value="4616" <%=("4616").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>MUSKOKA ALGONQUIN HEALTHCARE-HUNTSVILLE</option>
			<option value="1696" <%=("1696").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ALEXANDRA HOSPITAL</option>
			<option value="2084" <%=("2084").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ANSON GENERAL HOSPITAL</option>
			<option value="2088" <%=("2088").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SENSENBRENNER HOSPITAL (THE)</option>
			<option value="1284" <%=("1284").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>KEMPTVILLE DISTRICT HOSPITAL</option>
			<option value="2110" <%=("2110").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LAKE-OF-THE-WOODS DISTRICT HOSPITAL</option>
			<option value="3907" <%=("3907").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SOUTH BRUCE GREY HEALTH CTR-KINCARDINE</option>
			<option value="4830" <%=("4830").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>KINGSTON HEALTH SCIENCES CENTRE -HOTEL DIEU</option>
			<option value="4831" <%=("4831").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>KINGSTON HEALTH SCIENCES CENTRE -KINGSTON GENERAL</option>
			<option value="2211" <%=("2211").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>KIRKLAND AND DISTRICT HOSPITAL</option>
			<option value="1921" <%=("1921").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST MARY'S GENERAL HOSPITAL</option>
			<option value="3734" <%=("3734").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GRAND RIVER HOSPITAL CORP-WATERLOO SITE</option>
			<option value="1067" <%=("1067").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ERIE SHORES HEALTHCARE</option>
			<option value="1893" <%=("1893").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ROSS MEMORIAL HOSPITAL</option>
			<option value="1030" <%=("1030").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GREY BRUCE HEALTH SERVICES-LIONS HEAD</option>
			<option value="1740" <%=("1740").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LISTOWEL MEMORIAL HOSPITAL</option>
			<option value="2121" <%=("2121").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>MANITOULIN HEALTH CENTRE-LITTLE CURRENT</option>
			<option value="1497" <%=("1497").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST.JOSEPH'S HEALTH CARE,LONDON</option>
			<option value="3850" <%=("3850").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LONDON HLTH SCIENCES CTR-UNIVERSITY HOSP</option>
			<option value="4359" <%=("4359").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LONDON HLTH SCIENCES CTR-VICTORIA HOSP</option>
			<option value="4602" <%=("4602").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST.JOSEPH'S HEALTH CARE,LONDON-PARKWOOD</option>
			<option value="2176" <%=("2176").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>MANITOUWADGE GENERAL HOSPITAL</option>
			<option value="4819" <%=("4819").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTH OF SUPERIOR HLTHCARE GR-WILSON MEM</option>
			<option value="4025" <%=("4025").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GREY BRUCE HEALTH SERVICES-MARKDALE SITE</option>
			<option value="2049" <%=("2049").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SHOULDICE HOSPITAL</option>
			<option value="3587" <%=("3587").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>MARKHAM STOUFFVILLE HOSPITAL</option>
			<option value="2090" <%=("2090").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>BINGHAM MEMORIAL HOSPITAL</option>
			<option value="2126" <%=("2126").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>MATTAWA GENERAL HOSPITAL</option>
			<option value="4027" <%=("4027").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GREY BRUCE HEALTH SERVICES-MEAFORD SITE</option>
			<option value="1844" <%=("1844").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GEORGIAN BAY GENERAL HOSP-MIDLAND SITE</option>
			<option value="4022" <%=("4022").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HALTON HEALTHCARE SERVICES CORP-MILTON</option>
			<option value="2123" <%=("2123").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>MANITOULIN HEALTH CENTRE-MINDEMOYA UNIT</option>
			<option value="4747" <%=("4747").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>TRILLIUM HEALTH PARTNERS-CREDIT VALLEY</option>
			<option value="4752" <%=("4752").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>TRILLIUM HEALTH PARTNERS-MISSISSAUGA S</option>
			<option value="4698" <%=("4698").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WEENEEBAYKO AREA HLTH AUTH-MOOSE FACTORY</option>
			<option value="4323" <%=("4323").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTH WELLINGTON HLTH CARE-MOUNT FOREST</option>
			<option value="1295" <%=("1295").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LENNOX AND ADDINGTON COUNTY GEN HOSPITAL</option>
			<option value="2207" <%=("2207").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>TEMISKAMING HOSPITAL</option>
			<option value="1817" <%=("1817").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>STEVENSON MEMORIAL HOSPITAL ALLISTON</option>
			<option value="1507" <%=("1507").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>FOUR COUNTIES HEALTH SERVICES CORP</option>
			<option value="2038" <%=("2038").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SOUTHLAKE REGIONAL HEALTH CENTRE</option>
			<option value="4213" <%=("4213").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NIAGARA HEALTH SYSTEM-GREATER NIAGARA</option>
			<option value="2178" <%=("2178").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NIPIGON DISTRICT MEMORIAL HOSPITAL</option>
			<option value="4730" <%=("4730").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTH BAY REGIONAL HEALTH CENTRE</option>
			<option value="3926" <%=("3926").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HALTON HEALTHCARE SERVICES CORP-OAKVILLE</option>
			<option value="3684" <%=("3684").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HEADWATERS HEALTH CARE CENTRE-DUFFERIN</option>
			<option value="1853" <%=("1853").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ORILLIA SOLDIERS' MEMORIAL HOSPITAL</option>
			<option value="3932" <%=("3932").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LAKERIDGE HEALTH -OSHAWA SITE</option>
			<option value="1657" <%=("1657").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CHILDREN'S HOSPITAL OF EASTERN ONTARIO</option>
			<option value="1661" <%=("1661").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HOPITAL MONTFORT</option>
			<option value="1681" <%=("1681").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>QUEENSWAY-CARLETON HOSPITAL</option>
			<option value="4046" <%=("4046").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>OTTAWA HOSPITAL ( THE )-CIVIC SITE</option>
			<option value="4048" <%=("4048").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>OTTAWA HOSPITAL ( THE )-GENERAL SITE</option>
			<option value="4164" <%=("4164").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>UNIVERSITY OF OTTAWA HEART INSTITUTE</option>
			<option value="4599" <%=("4599").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ROYAL OTTAWA HLTH CARE GROUP-MENTAL HLTH</option>
			<option value="3944" <%=("3944").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GREY BRUCE HEALTH SERVICES-OWEN SOUND</option>
			<option value="4326" <%=("4326").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTH WELLINGTON HLTH CARE-PALMERSTON</option>
			<option value="3729" <%=("3729").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WEST PARRY SOUND HEALTH CENTRE</option>
			<option value="1804" <%=("1804").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>PEMBROKE REGIONAL HOSPITAL INC.</option>
			<option value="3732" <%=("3732").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>PERTH & SMITHS FALLS DIST-PERTH SITE</option>
			<option value="1768" <%=("1768").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>PETERBOROUGH REGIONAL HEALTH CENTRE</option>
			<option value="4418" <%=("4418").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>BLUEWATER HEALTH-PETROLIA SITE</option>
			<option value="3992" <%=("3992").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>QUINTE HEALTHCARE CORPORATION-PICTON</option>
			<option value="2153" <%=("2153").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>RIVERSIDE HEALTH CARE FAC-RAINY RIVER</option>
			<option value="2115" <%=("2115").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>RED LAKE MARG COCHENOUR MEM HOSP (THE)</option>
			<option value="1813" <%=("1813").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>RENFREW VICTORIA HOSPITAL</option>
			<option value="2046" <%=("2046").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>MACKENZIE HEALTH</option>
			<option value="4415" <%=("4415").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>BLUEWATER HEALTH-SARNIA GENERAL SITE</option>
			<option value="4407" <%=("4407").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SAULT AREA HOSPITAL-SAULT STE MARIE</option>
			<option value="4836" <%=("4836").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SCARBOROUGH AND ROUGE HOSPITAL-CENTENARY</option>
			<option value="4840" <%=("4840").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SCARBOROUGH AND ROUGE HOSPITAL -SCAR.GEN.SITE</option>
			<option value="4842" <%=("4842").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SCARBOROUGH AND ROUGE HOSPITAL -BIRCHMOUNT </option>
			<option value="4005" <%=("4005").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LAKERIDGE HEALTH -PORT PERRY</option>
			<option value="1213" <%=("1213").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SEAFORTH COMMUNITY HOSPITAL</option>
			<option value="1591" <%=("1591").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORFOLK GENERAL HOSPITAL</option>
			<option value="4353" <%=("4353").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SIOUX LOOKOUT MENO-YA-WIN HLTH CTR-DISTR</option>
			<option value="1269" <%=("1269").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>PERTH & SMITHS FALLS DIST-SMITHS FALLS</option>
			<option value="2094" <%=("2094").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SMOOTH ROCK FALLS HOSPITAL</option>
			<option value="4030" <%=("4030").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GREY BRUCE HEALTH SERVICES-SOUTHAMPTON</option>
			<option value="4224" <%=("4224").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NIAGARA HEALTH SYSTEM-ST CATHARINES GEN</option>
			<option value="1748" <%=("1748").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST MARYS MEMORIAL HOSPITAL</option>
			<option value="1059" <%=("1059").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST THOMAS-ELGIN GENERAL HOSPITAL</option>
			<option value="1754" <%=("1754").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>STRATFORD GENERAL HOSPITAL</option>
			<option value="1515" <%=("1515").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>STRATHROY MIDDLESEX GENERAL HOSPITAL</option>
			<option value="2812" <%=("2812").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WEST NIPISSING GENERAL HOSPITAL</option>
			<option value="4059" <%=("4059").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HEALTH SCIENCES NORTH-LAURENTIAN</option>
			<option value="4736" <%=("4736").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTH BAY REGIONAL HEALTH CTR-KIRKWOOD P</option>
			<option value="4822" <%=("4822").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTH OF SUPERIOR HLTHCARE GR-MCCAUSLAND</option>
			<option value="4770" <%=("4770").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTH SHORE HLTH NTWRK-THESSALON SITE</option>
			<option value="3853" <%=("3853").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>THUNDER BAY REGIONAL HLTH SCIENCES CTR</option>
			<option value="1709" <%=("1709").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>TILLSONBURG DISTRICT MEMORIAL HOSPITAL</option>
			<option value="3414" <%=("3414").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>TIMMINS & DISTRICT GENERAL HOSPITAL</option>
			<option value="1302" <%=("1302").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>TOR. EAST HLTH NTWRK-MICHAEL GARRON HOSP</option>
			<option value="1330" <%=("1330").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NORTH YORK GENERAL HOSPITAL</option>
			<option value="1332" <%=("1332").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>DON MILLS SURGICAL UNIT</option>
			<option value="1406" <%=("1406").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HOSPITAL FOR SICK CHILDREN  (THE)</option>
			<option value="1443" <%=("1443").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST JOSEPH'S HEALTH CENTRE</option>
			<option value="1444" <%=("1444").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ST MICHAEL'S HOSPITAL</option>
			<option value="3240" <%=("3240").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SALVATION ARMY TORONTO GRACE HLTH CTR PL</option>
			<option value="3438" <%=("3438").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>BELLWOOD HEALTH SERVICES INC (SCARB)</option>
			<option value="3459" <%=("3459").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CASEY HOUSE HOSPICE</option>
			<option value="3910" <%=("3910").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>UNIVERSITY HEALTH NETWORK</option>
			<option value="3929" <%=("3929").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WILLIAM OSLER HEALTH SYSTEM-ETOBICOKE</option>
			<option value="3936" <%=("3936").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SUNNYBROOK HEALTH SCIENCES CENTRE</option>
			<option value="4761" <%=("4761").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CENTRE FOR ADDICTION & MENTAL HEALTH</option>
			<option value="4799" <%=("4799").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>HUMBER RIVER HOSPITAL - WILSON SITE</option>
			<option value="4804" <%=("4804").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SINAI HEALTH SYSTEM-MOUNT SINAI SITE</option>
			<option value="1193" <%=("1193").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>CANADIAN FORCES HOSPITAL</option>
			<option value="3994" <%=("3994").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>QUINTE HEALTHCARE CORPORATION-TRENTON</option>
			<option value="4465" <%=("4465").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>MARKHAM STOUFFVILLE HOSP-UXBRIDGE SITE</option>
			<option value="4039" <%=("4039").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SOUTH BRUCE GREY HEALTH CENTRE-WALKERTON</option>
			<option value="1239" <%=("1239").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>SYDENHAM DISTRICT HOSPITAL</option>
			<option value="2076" <%=("2076").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>LADY DUNN HEALTH CENTRE</option>
			<option value="4227" <%=("4227").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>NIAGARA HEALTH SYSTEM-WELLAND COUNTY</option>
			<option value="4796" <%=("4796").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>ONTARIO SHORES CTR FOR MENTAL HLTH SCIEN</option>
			<option value="4033" <%=("4033").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>GREY BRUCE HEALTH SERVICES-WIARTON SITE</option>
			<option value="1885" <%=("1885").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WINCHESTER DISTRICT MEMORIAL HOSPITAL</option>
			<option value="1079" <%=("1079").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WINDSOR REGIONAL HOSPITAL-METROPOLITAN</option>
			<option value="4607" <%=("4607").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WINDSOR REGIONAL HOSPITAL-MARYVALE SITE</option>
			<option value="4773" <%=("4773").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WINDSOR REGIONAL HOSP-OUELLETTE CAMPUS</option>
			<option value="1217" <%=("1217").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WINGHAM AND DISTRICT HOSPITAL</option>
			<option value="1716" <%=("1716").equals(institutionFrom) ? "selected=\"selected\" " : "" %>>WOODSTOCK GENERAL HOSPITAL TRUST</option>

		</select>
            </div>
            
            <div class="space">&nbsp;</div>
			<div class="label"></div>
            <div class="input">
            </div>
        </li>
        
        
        <li class="row weak">
			<div class="label">Visit Disposition: </div>
            <div class="input">
            <%
           			String visitDisposition=new String();
            		if(bFirstDisp) {
            			if(appt.getVisitDisposition() != null) {
            				visitDisposition = appt.getVisitDisposition();
            			}
            		} else {
            			if(request.getParameter("visitDisposition") != null) {
            				visitDisposition = request.getParameter("visitDisposition");
            			}
            		}
            	%>
            	<select name="visitDisposition" style="width:150px">
               		<option value=""></option>
               		<option value="16" <%="16".equals(visitDisposition)?" selected=\"selected\" " : "" %>>Home with support/referral</option>
               		<option value="17" <%="17".equals(visitDisposition)?" selected=\"selected\" " : "" %>>Private home</option>
               		<option value="06" <%="06".equals(visitDisposition)?" selected=\"selected\" " : "" %>>Admit to reporting facility as inpatient to special care unit or O.R. from ambulatory care visit</option>
               		<option value="07" <%="07".equals(visitDisposition)?" selected=\"selected\" " : "" %>>Admit to reporting facility as an inpatient to another unit of the reporting facility from ambulatory care visit</option>
               		<option value="08" <%="08".equals(visitDisposition)?" selected=\"selected\" " : "" %>>Transfer to another acute care facility directly from ambulatory care visit</option>
               		<option value="09" <%="09".equals(visitDisposition)?" selected=\"selected\" " : "" %>>Transfer to another non acute care facility directly from ambulatory care visit</option>
               		<option value="12" <%="12".equals(visitDisposition)?" selected=\"selected\" " : "" %>>Intra-facility transfer to day surgery</option>
               		<option value="13" <%="13".equals(visitDisposition)?" selected=\"selected\" " : "" %>>Intra-facility trasnfer to the ED</option>
               		<option value="14" <%="14".equals(visitDisposition)?" selected=\"selected\" " : "" %>>Intra-facility transfer to clinic</option>
               	</select>
            </div>
            
            <div class="space">&nbsp;</div>
			<div class="label">Reason for Discharge:</div>
            <div class="input">
            <%
           			String reasonForDischarge=new String();
            		if(bFirstDisp) {
            			if(appt.getReasonForDischarge() != null) {
            				reasonForDischarge = appt.getReasonForDischarge();
            			}
            		} else {
            			if(request.getParameter("reasonForDischarge") != null) {
            				reasonForDischarge = request.getParameter("reasonForDischarge");
            			}
            		}
            	%>            
            	<select name="reasonForDischarge" style="width:150px">
            		<option value=""></option>
            		<option value="1" <%="1".equals(reasonForDischarge)?" selected=\"selected\" " : "" %>>Treatment Completed (ie goals complete, plateau in rehab recovery, etc.)</option>
            		<option value="2" <%="2".equals(reasonForDischarge)?" selected=\"selected\" " : "" %>>Treatment Incomplete: Change in Medical Status (admission to inpatient or death)</option>
            		<option value="3" <%="3".equals(reasonForDischarge)?" selected=\"selected\" " : "" %>>Treatment Incomplete: Transferred to another OPR</option>
            		<option value="4" <%="4".equals(reasonForDischarge)?" selected=\"selected\" " : "" %>>Treatment Incomplete: Patient Choice</option>
            		<option value="5" <%="5".equals(reasonForDischarge)?" selected=\"selected\" " : "" %>>Treatment Incomplete: Unknown/Other (includes patients who do not attend after only attending one visit)</option>
            		<option value="9" <%="9".equals(reasonForDischarge)?" selected=\"selected\" " : "" %>>Unknown or Unavailable</option>
            	</select>
            </div>
        </li>
        
        <li class="row weak">
			<div class="label">Main Problem:</div>
            <div class="input">
            <%
           			String mainProblem=new String();
            		if(bFirstDisp) {
            			if(appt.getMainProblem() != null) {
            				mainProblem = appt.getMainProblem();
            			}
            		} else {
            			if(request.getParameter("mainProblem") != null) {
            				mainProblem = request.getParameter("mainProblem");
            			}
            		}
            	%>      
            	<select name="mainProblem">
               		<option value=""></option>
               		<option value="Z509" <%="Z509".equals(mainProblem)?" selected=\"selected\" " : "" %>>Rehabilitation</option>
               	</select>
            </div>
            <div class="space">&nbsp;</div>
			<div class="label">Other Problem:</div>
            <div class="input">
           <%
           			String otherProblem=new String();
            		if(bFirstDisp) {
            			if(appt.getOtherProblem() != null) {
            				otherProblem = appt.getOtherProblem();
            			}
            		} else {
            			if(request.getParameter("otherProblem") != null) {
            				otherProblem = request.getParameter("otherProblem");
            			}
            		}
            	%>                  
            	<select name="otherProblem">
	            	<option value=""></option>
	            	<option value="I64" <%="I64".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Stroke - Unable to determine</option>
					<option value="I619" <%="I619".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Stroke - Intracerebral hemorrhage</option>
					<option value="I634" <%="I634".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Stroke - Ischemic</option>
					<option value="G459" <%="G459".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Stroke - TIA</option>
					<option value="G939" <%="G939".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Acquired Brain Injury - Non-Traumatic</option>
					<option value="C719" <%="C719".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Acquired Brain Injury - Malignant brain tumor</option>
					<option value="D332" <%="D332".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Acquired Brain Injury - Benign brain tumor</option>
					<option value="S069" <%="S069".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Acquired Brain Injury - Traumatic</option>
					<option value="S060" <%="S060".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Acquired Brain Injury - Concussion</option>
					<option value="G35" <%="G35".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Multiple Sclerosis</option>
					<option value="G20" <%="G20".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Parkinson's</option>
					<option value="G629" <%="G629".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Polyneuropathies</option>
					<option value="G610" <%="G610".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Guillain-Barre</option>
					<option value="G809" <%="G809".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Cerebral Palsy</option>
					<option value="G589" <%="G589".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Mononeuropathies of Limb</option>
					<option value="G549" <%="G549".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Nerve Root and Plexus Disorders</option>
					<option value="G510" <%="G510".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Bell's Palsy</option>
					<option value="G1220" <%="G1220".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Amyotrophic Lateral Sclerosis (ALS)</option>
					<option value="G710" <%="G710".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Duchene Muscular Dystrophy</option>
					<option value="M7929" <%="M7929".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Neuralgia</option>
					<option value="G319" <%="G319".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Degenerative disease</option>
					<option value="G969" <%="G969".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neurological - Other</option>
					<option value="G82293" <%="G82293".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Spinal Cord Dysfunction - Paraplegia</option>
					<option value="G82591" <%="G82591".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Spinal Cord Dysfunction - Quadriplegia</option>
					<option value="D334" <%="D334".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Spinal Cord Dysfunction - Benign tumor</option>
					<option value="C720" <%="C720".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Spinal Cord Dysfunction - Malignant tumor</option>
					<option value="Z890" <%="Z890".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Fingers/Thumb</option>
					<option value="Z891" <%="Z891".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Hand/Wrist</option>
					<option value="Z892" <%="Z892".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Upper Limb Above Wrist</option>
					<option value="Z893" <%="Z893".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Bilateral Upper Limbs</option>
					<option value="Z894" <%="Z894".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Foot/Ankle</option>
					<option value="Z896" <%="Z896".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Lower Extremity Above the Knee</option>
					<option value="Z895" <%="Z895".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Lower Extremity Below the Knee</option>
					<option value="Z897" <%="Z897".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Bilateral Lower Limbs</option>
					<option value="Z898" <%="Z898".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Upper and Lower Limbs</option>
					<option value="Z899" <%="Z899".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Amputation - Other Amputation</option>
					<option value="M069" <%="M069".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Arthritis - Rheumatoid</option>
					<option value="M199" <%="M199".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Arthritis - Osteoarthritis</option>
					<option value="M1399" <%="M1399".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Arthritis - Other</option>
					<option value="M542" <%="M542".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Persistent Pain - Neck Pain</option>
					<option value="M549" <%="M549".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Persistent Pain - Back Pain</option>
					<option value="M7969" <%="M7969".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Persistent Pain - Extremity Pain</option>
					<option value="M8902" <%="M8902".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Persistent Pain - Complex Regional Pain Syndrome</option>
					<option value="R529" <%="R529".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Persistent Pain - Other</option>
					<option value="M797" <%="M797".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Persistent Pain - Fibromyalgia</option>
					<option value="S72090" <%="S72090".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho -Traumatic Hip Fracture</option>
					<option value="M8445" <%="M8445".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Pathological/Non-Traumatic Hip Fracture</option>
					<option value="S32800" <%="S32800".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Pelvis Fracture</option>
					<option value="T0290" <%="T0290".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Major Multiple Fracture</option>
					<option value="Z9660" <%="Z9660".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hip Replacement</option>
					<option value="Z9661" <%="Z9661".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Knee Replacement</option>
					<option value="M750" <%="M750".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Shoulder - Adhesive capsulitis of Shoulder</option>
					<option value="S43090" <%="S43090".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Shoulder - Dislocated Shoulder</option>
					<option value="S42390" <%="S42390".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Shoulder - Fracture of Humerus</option>
					<option value="S42090" <%="S42090".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Shoulder - Fracture of Clavicle</option>
					<option value="M754" <%="M754".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Shoulder - Impingement Syndrome</option>
					<option value="S43402" <%="S43402".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Shoulder - Other sprain/strain/tear</option>
					<option value="M751" <%="M751".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Shoulder - Rotator Cuff Syndrome/Tear</option>
					<option value="M758" <%="M758".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Shoulder - Tendinopathy</option>
					<option value="S5349" <%="S5349".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Elbow - Sprain/Strain/Tear</option>
					<option value="M770" <%="M770".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Elbow - Epicondylitis - medial</option>
					<option value="M771" <%="M771".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Elbow - Epicondylitis - lateral</option>
					<option value="S42090" <%="S42090".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Elbow - Fracture</option>
					<option value="G560" <%="G560".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Forearm/Wrist - Carpal Tunnel</option>
					<option value="S62800" <%="S62800".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Forearm/Wrist - Fracture</option>
					<option value="S6359" <%="S6359".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Forearm/Wrist - Strain/Sprain</option>
					<option value="S5498" <%="S5498".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Forearm - Injury of Nerves</option>
					<option value="S5688" <%="S5688".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Forearm - Injury of Muscle & Tendon</option>
					<option value="S599" <%="S599".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Forearm - Other Injuries</option>
					<option value="S62800" <%="S62800".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hand - Fracture - Finger/Hand</option>
					<option value="S6498" <%="S6498".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hand - Injury of Nerves</option>
					<option value="S699" <%="S699".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hand - Other Injuries</option>
					<option value="S678" <%="S678".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hand - Crushing Injuries</option>
					<option value="S6379" <%="S6379".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hand - Other Hand Sprain/Strain/Tear</option>
					<option value="M779" <%="M779".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hip - Hamstring Tendonitis</option>
					<option value="S799" <%="S799".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hip - Other Injuries</option>
					<option value="S7319" <%="S7319".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hip - Sprain/Strain/Tear</option>
					<option value="S336" <%="S336".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Hip - Sacro-Iliac Strain</option>
					<option value="S800" <%="S800".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Knee - Contusion</option>
					<option value="M229" <%="M229".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Knee - Disorders of Patella</option>
					<option value="S82400" <%="S82400".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Knee - Fracture of Fibula</option>
					<option value="S82200" <%="S82200".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Knee - Fracture of Tibia</option>
					<option value="M239" <%="M239".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Knee - Internal Derangement of Knee</option>
					<option value="S899" <%="S899".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Knee - Other Unspecified Injuries</option>
					<option value="S836" <%="S836".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Knee - Other Knee/Leg Sprain/Strain/Tear</option>
					<option value="S9349" <%="S9349".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Ankle - Sprain/Strain/Tear</option>
					<option value="S999" <%="S999".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Ankle - Other Unspecified Injuries</option>
					<option value="S82890" <%="S82890".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Ankle - Fracture</option>
					<option value="M773" <%="M773".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Foot - Calcaneal Spur</option>
					<option value="S92900" <%="S92900".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Foot - Fracture</option>
					<option value="S9498" <%="S9498".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Foot - Injury of Nerves</option>
					<option value="S9698" <%="S9698".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Foot - Injury of Muscle & Tendon</option>
					<option value="S999" <%="S999".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Foot - Unspecified Injuries</option>
					<option value="M722" <%="M722".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Foot - Plantar Fasciitis</option>
					<option value="S1428" <%="S1428".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Back - Cervical - Nerve Injury</option>
					<option value="M45" <%="M45".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Back - Mechanical (Arthritis & Mechanical)</option>
					<option value="S2428" <%="S2428".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Back - Thoracic - Nerve Injury</option>
					<option value="S3428" <%="S3428".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Back - Lumbar & Sacral - Nerve Injury</option>
					<option value="M6099" <%="M6099".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Myositis</option>
					<option value="M9999" <%="M9999".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Other - Musculoskeletal condition</option>
					<option value="M4199" <%="M4199".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Postural Dysfunction - Scoliosis</option>
					<option value="M4059" <%="M4059".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Postural Dysfunction - Lordosis</option>
					<option value="M6599" <%="M6599".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Synovitis and Tenosynovitis</option>
					<option value="S034" <%="S034".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Ortho - Temporomandibular Strain/Sprain</option>
					<option value="I509" <%="I509".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Cardiac - Heart Failure</option>
					<option value="I409" <%="I409".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Cardiac - Myocarditis</option>
					<option value="Z951" <%="Z951".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Cardiac - Surgical - CABG</option>
					<option value="Z9500" <%="Z9500".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Cardiac - Surgical - Pacemaker</option>
					<option value="Z952" <%="Z952".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Cardiac - Surgical - Valve Replacement</option>
					<option value="Z955" <%="Z955".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Cardiac - Surgical - PCI/Coronary angioplasty</option>
					<option value="Q249" <%="Q249".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Cardiac - Adult Congenital Heart Disease (ACHD)</option>
					<option value="J449" <%="J449".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Pulmonary - COPD</option>
					<option value="Z991" <%="Z991".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Pulmonary - Respiratory Ventilator</option>
					<option value="J4590" <%="J4590".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Pulmonary - Asthma</option>
					<option value="J989" <%="J989".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Pulmonary - Other</option>
					<option value="T20" <%="T20".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Head and Neck</option>
					<option value="T21" <%="T21".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Trunk</option>
					<option value="T22" <%="T22".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Arm and Shoulder</option>
					<option value="T23" <%="T23".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Wrist and Hand</option>
					<option value="T24" <%="T24".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Hip and Leg</option>
					<option value="T25" <%="T25".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Ankle and Foot</option>
					<option value="T27" <%="T27".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Respiratory Tract</option>
					<option value="T28" <%="T28".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Other Internal Organs</option>
					<option value="T29" <%="T29".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Multiple Body Regions</option>
					<option value="T35" <%="T35".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Burns - Frostbite</option>
					<option value="Q059" <%="Q059".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Congenital Deformities - Spina Bifida</option>
					<option value="Q898" <%="Q898".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Congenital Deformities - Other</option>
					<option value="H819" <%="H819".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Disorders of Vestibular Function</option>
					<option value="G259" <%="G259".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Movement Disorder - Other</option>
					<option value="R53" <%="R53".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Debility/Frailty - Deconditioned</option>
					<option value="C809" <%="C809".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Neoplasm(s)</option>
					<option value="I890" <%="I890".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Circulatory Disorder - Lymphedema</option>
					<option value="I739" <%="I739".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Circulatory Disorder - Peripheral Vascular Disease</option>
					<option value="T889" <%="T889".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Medical/Surgical Complications</option>
					<option value="Z941" <%="Z941".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Transplants - Heart</option>
					<option value="Z942" <%="Z942".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Transplants - Lung</option>
					<option value="Z944" <%="Z944".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Transplants - Liver</option>
					<option value="Z940" <%="Z940".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Transplants - Kidney</option>
					<option value="Z9480" <%="Z9480".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Transplants - Bone Marrow</option>
					<option value="Z943" <%="Z943".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Transplants - Heart and lung</option>
					<option value="R69" <%="R69".equals(otherProblem)?" selected=\"selected\" " : ""  %>>Other - Term not on the list</option>
            	</select>
            </div>
        </li>
        
        <li class="row weak">
			<div class="label">Other Problem Prefix:</div>
            <div class="input">
           <%
           			String otherProblemPrefix =new String();
            		if(bFirstDisp) {
            			if(appt.getOtherProblemPrefix() != null) {
            				otherProblemPrefix = appt.getOtherProblemPrefix();
            			}
            		} else {
            			if(request.getParameter("otherProblemPrefix") != null) {
            				otherProblemPrefix = request.getParameter("otherProblemPrefix");
            			}
            		}
            	%>                       
           		<select name="otherProblemPrefix" style="width:150px">
                	<option value=""></option>
                	<option value="D" <%="D".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Total/hemi-arthroplasty</option>
					<option value="K" <%="K".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Reverse arthroplasty</option>
					<option value="W" <%="W".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Left body impairment</option>
					<option value="X" <%="X".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Right body impairment</option>
					<option value="Y" <%="Y".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Left and Right body impairment</option>
					<option value="Z" <%="Z".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>No paresis</option>
					<option value="U" <%="U".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Unilateral</option>
					<option value="B" <%="B".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Bilateral</option>
					<option value="N" <%="N".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Cervical</option>
					<option value="T" <%="T".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Thoracic </option>
					<option value="L" <%="L".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Lumbar</option>
					<option value="S" <%="S".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Sacral </option>
					<option value="O" <%="O".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Cardiomyopathy</option>
					<option value="P" <%="P".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Pericarditis</option>
					<option value="F" <%="F".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>1st degree burn</option>
					<option value="G" <%="G".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>2nd degree burn</option>
					<option value="H" <%="H".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>3rd degree burn</option>
					<option value="A" <%="A".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Aortic </option>
					<option value="E" <%="E".equals(otherProblemPrefix)?" selected=\"selected\" " : ""  %>>Extremity</option>
                </select>
            </div>
            <div class="space">&nbsp;</div>
			<div class="label">Program Type:</div>
            <div class="input">
            <%
           			String programType=new String();
            		if(bFirstDisp) {
            			programType = appt.getProgramType();
            		} else {
            			programType = request.getParameter("programType");
            		}
            	%>
            	<input type="text" name="programType" id="programType" value="<%=programType%>"/>
            </div>
        </li>
        
        <li class="row weak">
			<div class="label"></div>
            <div class="input"></div>
            <div class="space">&nbsp;</div>
			<div class="label"></div>
            <div class="input"></div>
        </li>
    </ul>

<% if (isSiteSelected) { %>
<table class="buttonBar deep">
	<tr>
            <% if (!bMultipleSameDayGroupAppt) { %>
        <td align="left"><input type="submit" class="rightButton blueButton top" id="updateButton"
			onclick="document.forms['EDITAPPT'].displaymode.value='Update Appt'; onButUpdate();"
			value="<bean:message key="appointment.editappointment.btnUpdateAppointment"/>">
             <% if (!props.getProperty("allowMultipleSameDayGroupAppt", "").equalsIgnoreCase("no")) {%>
		<input type="submit" id="groupButton"
			onclick="document.forms['EDITAPPT'].displaymode.value='Group Action'; onButUpdate();"
			value="<bean:message key="appointment.editappointment.btnGroupAction"/>">
             <% }%>
        <input TYPE="submit" id="printReceiptButton"
            onclick="document.forms['EDITAPPT'].displaymode.value='Update Appt';document.forms['EDITAPPT'].printReceipt.value='1';"
            VALUE="<bean:message key='appointment.editappointment.btnPrintReceipt'/>">
        <input type="hidden" name="printReceipt" value="">             
		<input type="submit" class="redButton button" id="deleteButton"
			onclick="document.forms['EDITAPPT'].displaymode.value='Delete Appt'; onButDelete();"
			value="<bean:message key="appointment.editappointment.btnDeleteAppointment"/>">
		<input type="button" name="buttoncancel" id="cancelButton"
			value="<bean:message key="appointment.editappointment.btnCancelAppointment"/>"
			onClick="onButCancel();"> <input type="button"
			name="buttoncancel" id="noShowButton"
			value="<bean:message key="appointment.editappointment.btnNoShow"/>"
			onClick="window.location='appointmentcontrol.jsp?buttoncancel=No Show&displaymode=Update Appt&appointment_no=<%=appointment_no%>'">
			<input type="button"
			name="buttonprintcard" id="printCardButton"
			value="Print Card"
			onClick="window.location='appointmentcontrol.jsp?displaymode=PrintCard&appointment_no=<%=appointment_no%>'">
			
			 <a href="javascript:void(0);" title="Annotation" onclick="window.open('<%=request.getContextPath()%>/annotation/annotation.jsp?display=<%=annotation_display%>&amp;table_id=<%=appointment_no%>','anwin','width=400,height=500');">
            	<img src="<%=request.getContextPath() %>/images/notes.gif" alt="rxAnnotation" height="16" width="13" border="0"/>
            </a>
		</td>
		<td align="right" nowrap><input type="button" name="labelprint" id="labelButton"
			value="<bean:message key="appointment.editappointment.btnLabelPrint"/>"
			onClick="window.open('../demographic/demographiclabelprintsetting.jsp?demographic_no='+document.EDITAPPT.demographic_no.value, 'labelprint','height=550,width=700,location=no,scrollbars=yes,menubars=no,toolbars=no' )">
		<!--input type="button" name="Button" value="<bean:message key="global.btnExit"/>" onClick="self.close()"-->
		 <% if (!props.getProperty("allowMultipleSameDayGroupAppt", "").equalsIgnoreCase("no")) {%>
                    <input type="button" id="repeatButton"
			value="<bean:message key="appointment.addappointment.btnRepeat"/>"
			onclick="onButRepeat()"></td>
                 <% }
                }%>
	</tr>
</table>
<% } %>

</div>
<div id="bottomInfo">
<table width="95%" align="center">
	<tr>
		<td><bean:message key="Appointment.msgTelephone" />: <%= StringUtils.trimToEmpty(phone)%><br>
		<bean:message key="Appointment.msgRosterStatus" />: <%=StringUtils.trimToEmpty(rosterstatus)%>
		</td>
		<% if (alert!=null && !alert.equals("")) { %>
		<td bgcolor='yellow'><font color='red'><b><%=alert%></b></font></td>
		<% } %>
	</tr>
</table>
<hr />

<% if (isSiteSelected) { %>
<table width="95%" align="center" id="belowTbl">
	<tr>
		<td><input type="submit"
			onclick="document.forms['EDITAPPT'].displaymode.value='Cut';"
			value="Cut" /> | <input type="submit"
			onclick="document.forms['EDITAPPT'].displaymode.value='Copy';"
			value="Copy" />
                     <%
                     if(bFirstDisp && apptObj!=null) {

                            long numSameDayGroupApptsPaste = 0;

                            if (props.getProperty("allowMultipleSameDayGroupAppt", "").equalsIgnoreCase("no")) {
                                
                                List<Appointment> aa = appointmentDao.search_group_day_appt(myGroupNo,Integer.parseInt(demono),appt.getAppointmentDate());
                                
                                numSameDayGroupApptsPaste = aa.size() > 0 ? new Long(aa.size()): 0;
                            }
                  %><a href=#
			onclick="pasteAppt(<%=(numSameDayGroupApptsPaste > 0)%>);">Paste</a>
		<% } %>
		</td>
	</tr>
	<%if(cheader1s != null && cheader1s.size()>0){%>
		<tr>
		<th width="40%"><font color="red">Outstanding 3rd Invoices</font></th>
		<th width="20%"><font color="red">Invoice Date</font></th>
		<th><font color="red">Amount</font></th>
		<th><font color="red">Balance</font></th>
		</tr>
		<%
		java.text.SimpleDateFormat fm = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(int i=0;i<cheader1s.size();i++) {
			if(cheader1s.get(i).getPayProgram().matches(BillingDataHlp.BILLINGMATCHSTRING_3RDPARTY)){ 
				BigDecimal payment = billingOnExtDao.getAccountVal(cheader1s.get(i).getId(), billingOnExtDao.KEY_PAYMENT);
				BigDecimal discount = billingOnExtDao.getAccountVal(cheader1s.get(i).getId(), billingOnExtDao.KEY_DISCOUNT);
				BigDecimal credit =  billingOnExtDao.getAccountVal(cheader1s.get(i).getId(), billingOnExtDao.KEY_CREDIT);
				BigDecimal total = cheader1s.get(i).getTotal();
				BigDecimal balance = total.subtract(payment).subtract(discount).add(credit);
				
            	if(balance.compareTo(BigDecimal.ZERO) != 0) { %>
					<tr>
						<td align="center"><a href="javascript:void(0)" onclick="popupPage(600,800, '<%=request.getContextPath() %>/billing/CA/ON/billingONCorrection.jsp?billing_no=<%=cheader1s.get(i).getId()%>')"><font color="red">Inv #<%=cheader1s.get(i).getId() %></font></a></td>
						<td align="center"><font color="red"><%=fm.format(cheader1s.get(i).getTimestamp()) %></font></td>
						<td align="center"><font color="red">$<%=cheader1s.get(i).getTotal() %></font></td>
						<td align="center"><font color="red">$<%=balance %></font></td>
					</tr>
				<%}
			}
		}
	 } %>
</table>
<% } %>


</div>
</FORM>
</div> <!-- end of edit appointment screen -->

<% 
    String formTblProp = props.getProperty("appt_formTbl");
    String[] formTblNames = formTblProp.split(";");
               
    int numForms = 0;
    for (String formTblName : formTblNames){
        if ((formTblName != null) && !formTblName.equals("")) {
            //form table name defined
            for(EncounterForm ef:encounterFormDao.findByFormTable(formTblName)) {
            	String formName = ef.getFormName();
                pageContext.setAttribute("formName", formName);
                boolean formComplete = false;
                EctFormData.PatientForm[] ptForms = EctFormData.getPatientFormsFromLocalAndRemote(loggedInInfo,demono, formTblName);

                if (ptForms.length > 0) {
                    formComplete = true;
                }
                numForms++;
                if (numForms == 1) {
           
%>
         <table style="font-size: 9pt;" bgcolor="#c0c0c0" align="center" valign="top" cellpadding="3px">
            <tr bgcolor="#ccccff">
                <th colspan="2">
                    <bean:message key="appointment.addappointment.msgFormsSaved"/>
                </th>
            </tr>              
<%              } %>
             
            <tr bgcolor="#c0c0c0" align="left">
                <th style="padding-right: 20px"><c:out value="${formName}:"/></th>
<%                 if (formComplete){  %>
                <td><bean:message key="appointment.addappointment.msgFormCompleted"/></td>
<%                 } else {            %>
                <td><bean:message key="appointment.addappointment.msgFormNotCompleted"/></td>
<%                 } %>               
            </tr>
<%                         
            }
        }
    }
               
    if (numForms > 0) {        %>
         </table>
<%  }   %>
         
         
<!-- View Appointment: Screen that summarizes appointment data.
Currently this is only used in the mobile version -->
<div id="viewAppointment" style="display:<%=(bFirstDisp && isMobileOptimized) ? "block":"none"%>;">
    <%
        // Format date to be more readable
        java.text.SimpleDateFormat inform = new java.text.SimpleDateFormat ("yyyy-MM-dd");
        String strDate = bFirstDisp ? ConversionUtils.toDateString(appt.getAppointmentDate()) : request.getParameter("appointment_date");
        java.util.Date d = inform.parse(strDate);
        String formatDate = "";
        try { // attempt to change string format
        java.util.ResourceBundle prop = ResourceBundle.getBundle("oscarResources", request.getLocale());
        formatDate = oscar.util.UtilDateUtilities.DateToString(d, prop.getString("date.EEEyyyyMMdd"));
        } catch (Exception e) {
            org.oscarehr.util.MiscUtils.getLogger().error("Error", e);
            formatDate = oscar.util.UtilDateUtilities.DateToString(inform.parse(strDate), "EEE, yyyy-MM-dd");
        }
    %>
    <div class="header">
        <div class="title" id="appointmentTitle">
            <bean:message key="appointment.editappointment.btnView" />
        </div>
        <a href=# onclick="window.close();" id="backButton" class="leftButton top"><%= strDate%></a>
        <a href="javascript:toggleView();" id="editButton" class="rightButton top">Edit</a>
    </div>
    <div id="info" class="panel">
        <ul>
            <li class="mainInfo"><a href="#" onclick="demographicdetail(550,700)">
                <%
                    String apptName = (bFirstDisp ? appt.getName() : request.getParameter("name")).toString();
                    //If a comma exists, need to split name into first and last to prevent overflow
                    int comma = apptName.indexOf(",");
                    if (comma != -1)
                        apptName = apptName.substring(0, comma) + ", " + apptName.substring(comma+1);
                %>
                <%=apptName%>
            </a></li>
            <li><div class="label"><bean:message key="Appointment.formDate" />: </div>
                <div class="info"><%=formatDate%></div>
            </li>
            <% // Determine appointment status from code so we can access
   // the description, colour, image, etc.
      AppointmentStatus apptStatus = (AppointmentStatus)allStatus.get(0);
      for (int i = 0; i < allStatus.size(); i++) {
            AppointmentStatus st = (AppointmentStatus) allStatus.get(i);
            if (st.getStatus().equals(statusCode)) {
                apptStatus = st;
                break;
            }
      }
%>
            <li><div class="label"><bean:message key="Appointment.formStatus" />: </div>
                <div class="info">
                <font style="background-color:<%=apptStatus.getColor()%>; font-weight:bold;">
                    <img src="../images/<%=apptStatus.getIcon()%>" />
                    <%=apptStatus.getDescription()%>
                </font>
                </div>
            </li>
            <li><div class="label"><bean:message key="appointment.editappointment.msgTime" />: </div>
                <div class="info">From <%=bFirstDisp ? ConversionUtils.toTimeStringNoSeconds(appt.getStartTime()) : request.getParameter("start_time")%>
                to <%=bFirstDisp ? ConversionUtils.toTimeStringNoSeconds(appt.getEndTime()) : request.getParameter("end_time")%></div>
            </li>
            <li><div class="label"><bean:message key="Appointment.formType" />: </div>
                <div class="info"><%=bFirstDisp ? appt.getType() : request.getParameter("type")%></div>
            </li>
            <li><div class="label"><bean:message key="Appointment.formReason" />: </div>
                <div class="info"><%=bFirstDisp ? appt.getReason() : request.getParameter("reason")%></div>
            </li>
            <li><div class="label"><bean:message key="Appointment.formLocation" />: </div>
                <div class="info"><%=bFirstDisp ? appt.getLocation() : request.getParameter("location")%></div>
            </li>
            <li><div class="label"><bean:message key="Appointment.formResources" />: </div>
                <div class="info"><%=bFirstDisp ? appt.getResources() : request.getParameter("resources")%></div>
            </li>
            <li>&nbsp;</li>
            <li class="notes">
                <div class="label"><bean:message key="Appointment.formNotes" />: </div>
                <div class="info"><%=bFirstDisp ? appt.getNotes() : request.getParameter("notes")%></div>
            </li>
        </ul>
    </div>
</div> <!-- end of screen to view appointment -->
</body>
<script type="text/javascript">
var loc = document.forms['EDITAPPT'].location;
if(loc.nodeName.toUpperCase() == 'SELECT') loc.style.backgroundColor=loc.options[loc.selectedIndex].style.backgroundColor;

jQuery(document).ready(function(){
	var belowTbl = jQuery("#belowTbl");
	if (belowTbl != null && belowTbl.length > 0 && belowTbl.find("tr").length == 2) {
		jQuery(belowTbl.find("tr")[1]).remove();
	} 
});

</script>

</html:html>
