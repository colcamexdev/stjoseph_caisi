	<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="net.sf.json.JSONArray"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="org.oscarehr.integration.ocean.OceanUtils"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_appointment" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_appointment");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>

<%@page import="org.oscarehr.util.SessionConstants"%>
<%@page import="org.oscarehr.common.model.ProviderPreference"%>
<%@page import="oscar.oscarBilling.ca.bc.decisionSupport.BillingGuidelines"%>
<%@page import="org.oscarehr.decisionSupport.model.DSConsequence"%>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="java.util.Set,java.util.HashSet"%>
<%@page import="org.oscarehr.managers.ProgramManager2"%>
<%
 
  String DONOTBOOK = "Do_Not_Book";
  String curProvider_no = request.getParameter("provider_no");
  String curDoctor_no = request.getParameter("doctor_no") != null ? request.getParameter("doctor_no") : "";
  String curUser_no = (String) session.getAttribute("user");
  String userfirstname = (String) session.getAttribute("userfirstname");
  String userlastname = (String) session.getAttribute("userlastname");
  
  LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);

  ProviderPreference providerPreference=(ProviderPreference)session.getAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE);
  int everyMin=providerPreference.getEveryMin();

  boolean bFirstDisp=true; //this is the first time to display the window
  boolean bFromWL=false; //this is from waiting list page

  if (request.getParameter("bFirstDisp")!=null) bFirstDisp= (request.getParameter("bFirstDisp")).equals("true");
  if (request.getParameter("demographic_no")!=null) bFromWL=true;

  String duration = request.getParameter("duration")!=null?(request.getParameter("duration").equals(" ")||request.getParameter("duration").equals("")||request.getParameter("duration").equals("null")?(""+everyMin) :request.getParameter("duration")):(""+everyMin) ;

  //check for management fee code eligibility
  Set<String> billingRecommendations = new HashSet<String>();
  try{
    List<DSConsequence> list = BillingGuidelines.getInstance().evaluateAndGetConsequences(loggedInInfo, request.getParameter("demographic_no"), curProvider_no);

    for (DSConsequence dscon : list){
        if (dscon.getConsequenceStrength().equals(DSConsequence.ConsequenceStrength.recommendation)) {
            String recommendation = new String(dscon.getText());
            billingRecommendations.add(recommendation);
        }
    }
  }catch(Exception e){
    MiscUtils.getLogger().error("Error", e);
  }
%>
<%@ page import="java.util.*, java.sql.*, oscar.*, java.text.*, java.lang.*, oscar.appt.*" errorPage="errorpage.jsp"%>
<%@ page import="oscar.appt.status.service.AppointmentStatusMgr"%>
<%@ page import="oscar.appt.status.service.impl.AppointmentStatusMgrImpl"%>
<%@ page import="org.oscarehr.common.model.AppointmentStatus"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.oscarEncounter.data.EctFormData"%>
<%@ page import="org.oscarehr.common.model.DemographicCust" %>
<%@ page import="org.oscarehr.common.dao.DemographicCustDao" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.oscarehr.common.model.Demographic" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.common.model.EncounterForm" %>
<%@ page import="org.oscarehr.common.dao.EncounterFormDao" %>
<%@ page import="org.oscarehr.common.model.Appointment" %>
<%@ page import="org.oscarehr.common.dao.OscarAppointmentDao" %>
<%@ page import="oscar.util.ConversionUtils" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.PMmodule.model.Program" %>
<%@ page import="org.oscarehr.PMmodule.model.ProgramProvider" %>
<%@ page import="org.oscarehr.common.model.Facility" %>
<%@ page import="org.oscarehr.PMmodule.service.ProviderManager" %>
<%@ page import="org.oscarehr.PMmodule.service.ProgramManager" %>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="org.oscarehr.managers.LookupListManager"%>
<%@ page import="org.oscarehr.common.model.LookupList"%>
<%@ page import="org.oscarehr.common.model.LookupListItem"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>

<jsp:useBean id="providerBean" class="java.util.Properties" scope="session" />

<%
	DemographicCustDao demographicCustDao = (DemographicCustDao)SpringUtils.getBean("demographicCustDao");
	ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
	DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	EncounterFormDao encounterFormDao = SpringUtils.getBean(EncounterFormDao.class);
	OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
	
	ProviderManager providerManager = SpringUtils.getBean(ProviderManager.class);
	ProgramManager programManager = SpringUtils.getBean(ProgramManager.class);
	
	String providerNo = loggedInInfo.getLoggedInProviderNo();
	Facility facility = loggedInInfo.getCurrentFacility();
	
    List<Program> programs = programManager.getActiveProgramByFacility(providerNo, facility.getId());

	LookupListManager lookupListManager = SpringUtils.getBean(LookupListManager.class);
	LookupList reasonCodes = lookupListManager.findLookupListByName(loggedInInfo, "reasonCode");
	pageContext.setAttribute("reasonCodes", reasonCodes);
	
    int iPageSize=5;

    ApptData apptObj = ApptUtil.getAppointmentFromSession(request);

    oscar.OscarProperties pros = oscar.OscarProperties.getInstance();
    String strEditable = pros.getProperty("ENABLE_EDIT_APPT_STATUS");
    Boolean isMobileOptimized = session.getAttribute("mobileOptimized") != null;

    AppointmentStatusMgr apptStatusMgr = new AppointmentStatusMgrImpl();
    List<AppointmentStatus> allStatus = apptStatusMgr.getAllActiveStatus();
    
    String useProgramLocation = OscarProperties.getInstance().getProperty("useProgramLocation");
    String moduleNames = OscarProperties.getInstance().getProperty("ModuleNames");
    boolean caisiEnabled = moduleNames != null && org.apache.commons.lang.StringUtils.containsIgnoreCase(moduleNames, "Caisi");
    boolean locationEnabled = caisiEnabled && (useProgramLocation != null && useProgramLocation.equals("true"));
    
    ProgramManager2 programManager2 = SpringUtils.getBean(ProgramManager2.class);
    
    Integer oceanNumReferrals = null;
    List<JSONObject> oceanReferrals = null;
    if (!bFirstDisp && request.getParameter("demographic_no") != null && !request.getParameter("demographic_no").equals("")) {
    	//check with ocean
    	String oceanId = OceanUtils.getPatientRefFromDemographicNo(request.getParameter("demographic_no"));

    	if(!StringUtils.isEmpty(oceanId)) {
    		//check for any active referrals
    		oceanNumReferrals = OceanUtils.getNumberOfActiveReferrals(oceanId,request.getParameter("demographic_no"));
    		if(oceanNumReferrals != null && oceanNumReferrals > 0) {
    			oceanReferrals = OceanUtils.getReferrals(request.getParameter("demographic_no"));
    		}
    	}
    }
%>
<%@page import="org.oscarehr.common.dao.SiteDao"%>
<%@page import="org.oscarehr.common.model.Site"%>
<html:html locale="true">
<head>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="<%=request.getContextPath()%>/js/fg.menu.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/cupertino/jquery-ui-1.8.18.custom.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/fg.menu.css">

<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/checkDate.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
<% if (isMobileOptimized) { %>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, width=device-width" />
    <link rel="stylesheet" href="../mobile/appointmentstyle.css" type="text/css">
<% } else { %>
    <link rel="stylesheet" href="appointmentstyle.css" type="text/css">
<% }%>
<title><bean:message key="appointment.addappointment.title" /></title>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>
   <script>
     jQuery.noConflict();
   </script>
<oscar:customInterface section="addappt"/>
<script type="text/javascript">

function onAdd() {
    if (document.ADDAPPT.notes.value.length > 255) {
      window.alert("<bean:message key="appointment.editappointment.msgNotesTooBig"/>");
      return false;
    }
    return calculateEndTime() ;
}


<!--
function setfocus() {
	this.focus();
  document.ADDAPPT.keyword.focus();
  document.ADDAPPT.keyword.select();
}

function upCaseCtrl(ctrl) {
	ctrl.value = ctrl.value.toUpperCase();
}

function onBlockFieldFocus(obj) {
  obj.blur();
  document.ADDAPPT.keyword.focus();
  document.ADDAPPT.keyword.select();
  window.alert("<bean:message key="Appointment.msgFillNameField"/>");
}

function checkTypeNum(typeIn) {
	var typeInOK = true;
	var i = 0;
	var length = typeIn.length;
	var ch;

	// walk through a string and find a number
	if (length>=1) {
	  while (i <  length) {
		  ch = typeIn.substring(i, i+1);
		  if (ch == ":") { i++; continue; }
		  if ((ch < "0") || (ch > "9") ) {
			  typeInOK = false;
			  break;
		  }
	    i++;
      }
	} else typeInOK = false;
	return typeInOK;
}

function checkTimeTypeIn(obj) {
  var colonIdx;
  if(!checkTypeNum(obj.value) ) {
	  alert ("<bean:message key="Appointment.msgFillTimeField"/>");
  } else {
      colonIdx = obj.value.indexOf(':');
      if(colonIdx==-1) {
        if(obj.value.length < 3) alert("<bean:message key="Appointment.msgFillValidTimeField"/>");
        obj.value = obj.value.substring(0, obj.value.length-2 )+":"+obj.value.substring( obj.value.length-2 );
  }
}
          
  var hours = "";
  var minutes = "";  

  colonIdx = obj.value.indexOf(':');  
  if (colonIdx < 1)
      hours = "00";     
  else if (colonIdx == 1)
      hours = "0" + obj.value.substring(0,1);
  else
      hours = obj.value.substring(0,2);
  
  minutes = obj.value.substring(colonIdx+1,colonIdx+3);
  if (minutes.length == 0)
	    minutes = "00";
  else if (minutes.length == 1)
    minutes = "0" + minutes;
  else if (minutes > 59)
    minutes = "00";

  obj.value = hours + ":" + minutes;    
}

var readOnly=false;
function checkDateTypeIn(obj) {
    if (obj.value == '') {
        alert("Date cannot be empty");
        return false;
    } else { 
        obj.value = obj.value.replace(/\//g,"-");
        if (!check_date(obj.name))
          return false;
    } 
}

function calculateEndTime() {
  var stime = document.ADDAPPT.start_time.value;
  var vlen = stime.indexOf(':')==-1?1:2;
  var shour = stime.substring(0,2) ;
  var smin = stime.substring(stime.length-vlen) ;
  var duration = document.ADDAPPT.duration.value ;
  
  if(isNaN(duration)) {
	  alert("<bean:message key="Appointment.msgFillTimeField"/>");
	  return false;
  }

  if(eval(duration) == 0) { duration =1; }
  if(eval(duration) < 0) { duration = Math.abs(duration) ; }

  var lmin = eval(smin)+eval(duration)-1 ;
  var lhour = parseInt(lmin/60);

  if((lmin) > 59) {
    shour = eval(shour) + eval(lhour);
    shour = shour<10?("0"+shour):shour;
    smin = lmin - 60*lhour;
  } else {
    smin = lmin;
  }

  smin = smin<10?("0"+ smin):smin;
  document.ADDAPPT.end_time.value = shour +":"+ smin;

  if(shour > 23) {
    alert("<bean:message key="Appointment.msgCheckDuration"/>");
    return false;
  }

  //no show
  if(document.ADDAPPT.keyword.value.substring(0,1)=="." && document.ADDAPPT.demographic_no.value=="" ) {
    document.ADDAPPT.status.value = 'N' ;
  }

  return true;
}

function onNotBook() {
	document.forms[0].keyword.value = "<%=DONOTBOOK%>" ;
}

function onButRepeat() {
	document.forms[0].action = "appointmentrepeatbooking.jsp" ;
	if (calculateEndTime()) { document.forms[0].submit(); }
}
<% if(apptObj!=null) { %>
function pasteAppt(multipleSameDayGroupAppt) {

        var warnMsgId = document.getElementById("tooManySameDayGroupApptWarning");

        if (multipleSameDayGroupAppt) {
           warnMsgId.style.display = "block";
           if (document.forms[0].groupButton) {
              document.forms[0].groupButton.style.display = "none";
           }
           if (document.forms[0].addPrintPreviewButton){
              document.forms[0].addPrintPreviewButton.style.display = "none";
           }
           document.forms[0].addButton.style.display = "none";
           document.forms[0].printButton.style.display = "none";

           if (document.forms[0].pasteButton) {
                document.forms[0].pasteButton.style.display = "none";
           }

           if (document.forms[0].apptRepeatButton) {
                document.forms[0].apptRepeatButton.style.display = "none";
           }
        }
        else {
           warnMsgId.style.display = "none";
        }
        //document.forms[0].status.value = "<%=apptObj.getStatus()%>";
        document.forms[0].duration.value = "<%=apptObj.getDuration()%>";
        //document.forms[0].chart_no.value = "<%=apptObj.getChart_no()%>";
        document.forms[0].keyword.value = "<%=apptObj.getName()%>";
        document.forms[0].demographic_no.value = "<%=apptObj.getDemographic_no()%>";
        document.forms[0].reason.value = "<%= StringEscapeUtils.escapeJavaScript(apptObj.getReason()) %>";
        document.forms[0].notes.value = "<%= StringEscapeUtils.escapeJavaScript(apptObj.getNotes()) %>";
        //document.forms[0].location.value = "<%=apptObj.getLocation()%>";
        document.forms[0].resources.value = "<%=apptObj.getResources()%>";
        document.forms[0].type.value = "<%=apptObj.getType()%>";
        if('<%=apptObj.getUrgency()%>' == 'critical') {
                document.forms[0].urgency.checked = "checked";
        }

}
<% } %>


	function openTypePopup () {
		windowprops = "height=170,width=500,location=no,scrollbars=no,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=100,left=100";
		var popup=window.open("appointmentType.jsp?type="+document.forms['ADDAPPT'].type.value, "Appointment Type", windowprops);
		if (popup != null) {
			if (popup.opener == null) {
				popup.opener = self;
			}
			popup.focus();
		}
	}

	function setType(typeSel,reasonSel,locSel,durSel,notesSel,resSel) {
		  document.forms['ADDAPPT'].type.value = typeSel;
		  document.forms['ADDAPPT'].reason.value = reasonSel;
		  document.forms['ADDAPPT'].duration.value = durSel;
		  document.forms['ADDAPPT'].notes.value = notesSel;
		  document.forms['ADDAPPT'].duration.value = durSel;
		  document.forms['ADDAPPT'].resources.value = resSel;
		  var loc = document.forms['ADDAPPT'].location;
		  if(loc.nodeName == 'SELECT') {
		          for(c = 0;c < loc.length;c++) {
		                  if(loc.options[c].innerHTML == locSel) {
		                          loc.selectedIndex = c;
		                          loc.style.backgroundColor=loc.options[loc.selectedIndex].style.backgroundColor;
		                          break;
		                  }
		          }
		  } else if (loc.nodeName == "INPUT") {
			  document.forms['ADDAPPT'].location.value = locSel;
		  }
	}

	$.fn.labselect = function(str) {
		/*
	    $('option', this).filter(function() {
	       return $(this).text() == str;
	    })[0].selected = true;
*/
	    var items = $('option', this).filter(function() {
		       return $(this).text() == str;
	    });
	
		if(items != null && items.length > 0) {
			items[0].selected = true;
		}
	    return this;
	};
	
	function updateFieldsUsingOCEAN() {
		//console.log('TODO');
		var selectedOption = $("#oceanReferral").children("option:selected");
		var refSource = selectedOption.attr("refsource");
		$('#referralSourcePriorToAmbulatoryCareVisit').labselect(refSource);
		
		var srcSiteName = selectedOption.attr("srcSiteName");
		$("#institutionFrom").labselect(srcSiteName);
		
		var referralDate = selectedOption.attr("sentDate");
		referralDate = referralDate.substring(0,10);
		$("#referralDate").val(referralDate);
		
		var case1 = selectedOption.attr("case1");
		$("#programType").val(case1);
	}

</script>

<%

  SimpleDateFormat fullform = new SimpleDateFormat ("yyyy-MM-dd HH:mm");
  SimpleDateFormat inform = new SimpleDateFormat ("yyyy-MM-dd");
  SimpleDateFormat outform = new SimpleDateFormat ("EEE");

  java.util.Date apptDate;

  if(request.getParameter("year")==null || request.getParameter("month")==null || request.getParameter("day")==null){
    Calendar cal = Calendar.getInstance();
    String sDay = String.valueOf(cal.get(Calendar.DATE));
    String sMonth = String.valueOf(cal.get(Calendar.MONTH) + 1);
    String sYear = String.valueOf(cal.get(Calendar.YEAR));
    String sTime=(request.getParameter("start_time")==null)?"00:00AM":request.getParameter("start_time");
    apptDate = fullform.parse(bFirstDisp?(sYear + "-" + sMonth + "-" + sDay + " "+ sTime):
        (request.getParameter("appointment_date") + " " + sTime)) ;
  }else if(request.getParameter("start_time")==null){
    apptDate = fullform.parse(bFirstDisp?(request.getParameter("year") + "-" + request.getParameter("month") + "-" + request.getParameter("day")+" "+ "00:00 AM"):
        (request.getParameter("appointment_date") + " " + "00:00AM")) ;
  }else
  {
    apptDate = fullform.parse(bFirstDisp?(request.getParameter("year") + "-" + request.getParameter("month") + "-" + request.getParameter("day")+" "+ request.getParameter("start_time")):
        (request.getParameter("appointment_date") + " " + request.getParameter("start_time"))) ;
  }

  String dateString1 = outform.format(apptDate );
  String dateString2 = inform.format(apptDate );

  GregorianCalendar caltime =new GregorianCalendar( );
  caltime.setTime(apptDate);
  caltime.add(GregorianCalendar.MINUTE, Integer.parseInt(duration)-1 );

  java.util.Date startTime = ConversionUtils.fromDateString(request.getParameter("start_time"),"HH:mm");
  java.util.Date endTime = ConversionUtils.fromDateString(caltime.get(Calendar.HOUR_OF_DAY) +":"+ caltime.get(Calendar.MINUTE),"HH:mm");
  
  List<Appointment> appts = appointmentDao.search_appt(apptDate, curProvider_no, startTime, endTime, startTime, endTime, startTime, endTime, Integer.parseInt((String)request.getSession().getAttribute("programId_oscarView")));
  
  long apptnum = appts.size() > 0 ? new Long(appts.size()) : 0;
  
  OscarProperties props = OscarProperties.getInstance();
  
  String timeoutSeconds = props.getProperty("appointment_locking_timeout","0");
  int timeoutSecs = 0; 
  try { 
    timeoutSecs = Integer.parseInt(timeoutSeconds);
  }catch (NumberFormatException e) {/*empty*/}
  
  int hourInt = caltime.get(Calendar.HOUR_OF_DAY); 
  String hour = String.valueOf(hourInt);
  if (hour.length() == 0)
      hour = "00";
   else if (hour.length() == 1)
      hour = "0" + hour;
  
  int minuteInt = caltime.get(Calendar.MINUTE); 
  String minute = String.valueOf(minuteInt);
   if (minute.length() == 0)
      minute = "00";
   else if (minute.length() == 1) 
      minute = "0" + minute;  
  
   if (timeoutSecs > 0) {
%>

<script>
        var timers = new Array();

	$(document).ready(function(){
           $(window).bind('beforeunload',function(){cancelPageLock();
           });
           //cancel any page view/locks held by provider on clicking 'X'
           $("form#addappt").submit(function() {$(window).unbind('beforeunload');
           });

           calculateEndTime();
           var endTime = document.forms[0].end_time.value;
           var startTime = document.forms[0].start_time.value;
           var apptDate = document.forms[0].appointment_date.value; 
           updatePageLock(100,apptDate,startTime,endTime);	   
	});
        
        function checkPageLock() {
           $("#searchBtn").attr("disabled","disabled");
           calculateEndTime();
           var endTime = document.forms[0].end_time.value;
           var startTime = document.forms[0].start_time.value;
           var apptDate = document.forms[0].appointment_date.value;
           updatePageLock(100,apptDate,startTime,endTime);
           
        }
        
        function updatePageLock(timeout, apptDate, startTime, endTime) {

           for (var i = 0; i < timers.length; i++) {
                clearTimeout(timers[i]);               
           }
                      
	   haveLock=false;
           $.ajax({
               type: "POST",
               url: "<%=request.getContextPath()%>/PageMonitoringService.do",
               data: { method: "update", page: "addappointment", pageId: "<%=curProvider_no%>|" + apptDate + "|" + startTime + "|" + endTime, lock: true, timeout: <%=timeoutSeconds%>, cleanupExisting: true},
               dataType: 'json',
               async: false,
               success: function(data,textStatus) {
                       lockData=data;
                            var locked=false;
                            var lockedProviderName='';
                            var providerNames='';
                            haveLock=false;
                       $.each(data, function(key, val) {				
                         if(val.locked) {
                             locked=true;
                             lockedProviderName=val.providerName;
                         }
                         if(val.locked==true && val.self==true) {
                                       haveLock=true;
                               }
                         if(providerNames.length > 0)
                             providerNames += ",";
                         providerNames += val.providerName;

                       });

                       var lockedMsg = locked?'<span style="color:red" title="'+lockedProviderName+'">&nbsp(locked)</span>':'';
                       $("#lock_notification").html(
                            '<span title="'+providerNames+'">Viewers:'+data.length+lockedMsg+'</span>'	   
                       );


                       if(haveLock==true) { //i have the lock
                            $("#addButton").show(); 
                            $("#printButton").show();
                            $("#addPrintPreviewButton").show(); 
                            $("#pasteButton").show(); 
                            $("#apptRepeatButton").show(); 
                       } else if(locked && !haveLock) { //someone else has lock.
                            $("#addButton").hide(); 
                            $("#printButton").hide();
                            $("#addPrintPreviewButton").hide(); 
                            $("#pasteButton").hide(); 
                            $("#apptRepeatButton").hide();                          
                       } else { //no lock
                            $("#addButton").show(); 
                            $("#printButton").show();
                            $("#addPrintPreviewButton").show(); 
                            $("#pasteButton").show(); 
                            $("#apptRepeatButton").show();                                 
                       }
                       $("#searchBtn").removeAttr("disabled");
               }
             }
            );
            
            timers.push(setTimeout(function(){updatePageLock(5000, apptDate, startTime, endTime)},timeout));      
        }
        
        function cancelPageLock() {
           calculateEndTime();
           var endTime = document.forms[0].end_time.value;
           var startTime = document.forms[0].start_time.value;
           var apptDate = document.forms[0].appointment_date.value;
           
           for (var i = 0; i < timers.length; i++) {
               clearTimeout(timers[i]);               
           }
           
           $.ajax({
               type: "POST",
               url: "<%=request.getContextPath()%>/PageMonitoringService.do",
               data: { method: "cancel", page: "addappointment", pageId: "<%=curProvider_no%>|" + apptDate + "|" + startTime + "|" + endTime},
               dataType: 'json',
               async: false,
               success: function(data,textStatus) {}
           });
        }

</script>

<%
 } else {        
%>
<script>
    function checkPageLock() { //don't do anything unless timeout/locking is enabled.
    }
    function cancelPageLock() { //don't do anything unless timeout/locking is enabled.
    }
</script>
<%
  }
  String deepcolor = apptnum==0?"#CCCCFF":"gold", weakcolor = apptnum==0?"#EEEEFF":"ivory";
  if (!isMobileOptimized) {
%>
      <!-- Change the background color of deep/weak sections -->
      <style type="text/css">
          .deep { background-color: <%= deepcolor %>; }
          .weak { background-color: <%= weakcolor %>; }
      </style>
  
<%
  }
  boolean bDnb = false;
  for(Appointment a : appts) {
	  String apptName = a.getName();
	  if (apptName.equalsIgnoreCase(DONOTBOOK)) bDnb = true;
  }
 

  // select provider lastname & firstname
  String pLastname = "";
  String pFirstname = "";
  Provider p = providerDao.getProvider(curProvider_no);
  if(p != null) {
	  pLastname = p.getLastName();
      pFirstname = p.getFirstName();
  }
%>
</head>
<body bgproperties="fixed"
	onLoad="setfocus()" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
 <% if (timeoutSecs >0) { %>
    <div id="lock_notification">
        <span title="">Viewers: N/A</span>
    </div>
 <% } %>
<%
  String patientStatus = "";
  String disabled="";
  String address ="";
  String province = "";
  String city ="";
  String postal ="";
  String phone = "";
  String phone2 = "";
  String email  = "";
  String hin = "";
  String dob = "";
  String sex = "";

  //to show Alert msg

  boolean bMultipleSameDayGroupAppt = false;
  String displayStyle = "display:none";
  String myGroupNo = providerPreference.getMyGroupNo();

  if (props.getProperty("allowMultipleSameDayGroupAppt", "").equalsIgnoreCase("no")) {

        String demographicNo = request.getParameter("demographic_no");

        if (!bFirstDisp && (demographicNo != null) && (!demographicNo.equals(""))) {

        	
        	appts = appointmentDao.search_group_day_appt(myGroupNo, Integer.parseInt(demographicNo), apptDate);
            
            long numSameDayGroupAppts = appts.size() > 0 ? new Long(appts.size()) : 0;
            bMultipleSameDayGroupAppt = (numSameDayGroupAppts > 0);
        }

        if (bMultipleSameDayGroupAppt){
            displayStyle="display:block";
        }
  }
  %>
  <div id="tooManySameDayGroupApptWarning" style="<%=displayStyle%>">
    <table width="98%" BGCOLOR="red" border=1 align='center'>
        <tr>
            <th>
                <font color='white'>
                    <bean:message key='appointment.addappointment.titleMultipleGroupDayBooking'/><br/>
                    <bean:message key='appointment.addappointment.MultipleGroupDayBooking'/>
                </font>
            </th>
        </tr>
    </table>
</div>
<%
  if (!bFirstDisp && request.getParameter("demographic_no") != null && !request.getParameter("demographic_no").equals("")) {
	  Demographic d = demographicDao.getDemographic(request.getParameter("demographic_no"));
	  if(d != null) {
		  patientStatus = d.getPatientStatus();
		  address = d.getAddress();
		  city = d.getCity();
		  province = d.getProvince();
		  postal = d.getPostal();
		  phone = d.getPhone();
		  phone2 = d.getPhone2();
		  email = d.getEmail();
		  String year_of_birth   = d.getYearOfBirth();
	      String month_of_birth  = d.getMonthOfBirth();
	      String date_of_birth   = d.getDateOfBirth();
	      dob = "("+year_of_birth+"-"+month_of_birth+"-"+date_of_birth+")";
	      sex = d.getSex();
	      hin = d.getHin();
	      String ver = d.getVer();
	      hin = hin +" "+ ver;
	        
	      if (patientStatus == null || patientStatus.equalsIgnoreCase("AC")) {
	        patientStatus = "";
	      } else if (patientStatus.equalsIgnoreCase("FI")||patientStatus.equalsIgnoreCase("DE")||patientStatus.equalsIgnoreCase("IN")) {
	      	disabled = "disabled";
	      }

	      String rosterStatus = d.getRosterStatus();
	      if (rosterStatus == null || rosterStatus.equalsIgnoreCase("RO")) {
	      	rosterStatus = "";
	      }

	      if(!patientStatus.equals("") || !rosterStatus.equals("") ) {
	      	String rsbgcolor = "BGCOLOR=\"orange\"" ;
	        String exp = " null-undefined\n IN-inactive ID-deceased OP-out patient\n NR-not signed\n FS-fee for service\n TE-terminated\n SP-self pay\n TP-third party";

%>
<table width="98%" <%=rsbgcolor%> border=0 align='center'>
	<tr>
		<td><font color='blue' title='<%=exp%>'> <b><bean:message key="Appointment.msgPatientStatus" />:&nbsp;
                    <font color='yellow'><%=patientStatus%></font>&nbsp;<bean:message key="Appointment.msgRosterStatus" />:&nbsp;
                    <font color='yellow'><%=rosterStatus%></font></b></font>
                </td>
	</tr>
</table>
<%

        }
	}
	if( request.getParameter("demographic_no")!=null && !"".equals(request.getParameter("demographic_no")) ) {
	DemographicCust demographicCust = demographicCustDao.find(Integer.parseInt(request.getParameter("demographic_no")));

		if (demographicCust != null && demographicCust.getAlert() != null && !demographicCust.getAlert().equals("") ) {

%>
<p>
<table width="98%" BGCOLOR="yellow" border=1 align='center'>
	<tr>
		<td><font color='red'><bean:message key="Appointment.formAlert" />: <b><%=demographicCust.getAlert()%></b></font></td>
	</tr>
</table>
<%

		}
	}
  }


  if(apptnum!=0) {

%>
<table width="98%" class="deep" border=1 align='center'>
	<tr>
		<%--    <TH><font color='red'><%=apptnum>1?"Double ++ ":"Double"%> Booking</font></TH>--%>
		<TH><font color='red'> <% if(apptnum>1) {

         %> <bean:message key='appointment.addappointment.msgBooking' />
		<%

       } else {

          %> <bean:message
			key='appointment.addappointment.msgDoubleBooking' /> <%
			if(bDnb) out.println("<br/>You can NOT book an appointment on this time slot.");
       }

     %> </font></TH>
	</tr>
</table>
<% } %>

<% if (billingRecommendations.size() > 0) { %>
        <table width="98%" align="center" style="border:solid 3px red;padding-left:10px;line-height:150%;font-family:Arial;color: red; background-color: #FFFFFF; font-size: 18px; font-weight: bold; text-align:left;">
            <% for (String recommendation : billingRecommendations) { %>
                <tr>
                    <th><%=recommendation%></th>
                </tr>
            <% } %>
        </table>
<% } %>

<FORM NAME="ADDAPPT" id="addappt" METHOD="post" ACTION="<%=request.getContextPath()%>/appointment/appointmentcontrol.jsp"
	onsubmit="return(onAdd())"><INPUT TYPE="hidden"
	NAME="displaymode" value="">
	<input type="hidden" name="year" value="<%=request.getParameter("year") %>" >
    <input type="hidden" name="month" value="<%=request.getParameter("month") %>" >
    <input type="hidden" name="day" value="<%=request.getParameter("day") %>" >
    <input type="hidden" name="fromAppt" value="1" >
	
<div class="header deep">
    <div class="title">
        <!-- We display a shortened title for the mobile version -->
        <% if (isMobileOptimized) { %><bean:message key="appointment.addappointment.msgMainLabelMobile" />
        <% } else { %><bean:message key="appointment.addappointment.msgMainLabel" />
        <%          out.println("("+pFirstname+" "+pLastname+")"); %>
        <% } %>
    </div>
</div>
<div class="panel">
    <ul>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formDate" /><font size='-1' color='brown'>(<%=dateString1%>)</font>:</div>
            <div class="input">
                <INPUT TYPE="TEXT" NAME="appointment_date"
                    VALUE="<%=dateString2%>" WIDTH="25" HEIGHT="20" border="0"
                    hspace="2" onChange="checkDateTypeIn(this);checkPageLock()">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formStatus" />:</div>
            <div class="input">
				<%
            if (strEditable!=null&&strEditable.equalsIgnoreCase("yes")){
            %> <select name="status" STYLE="width: 154px" HEIGHT="20"
                    border="0">
                    <% for (int i = 0; i < allStatus.size(); i++) { %>
                    <option
                            value="<%=(allStatus.get(i)).getStatus()%>"
                            <%=(allStatus.get(i)).getStatus().equals(request.getParameter("status"))?"SELECTED":""%>><%=(allStatus.get(i)).getDescription()%></option>
                    <% } %>
            </select> <%
            }
            if (strEditable==null || !strEditable.equalsIgnoreCase("yes")){
            %> <INPUT TYPE="TEXT" NAME="status"
					VALUE='<%=bFirstDisp?"t":request.getParameter("status")==null?"":request.getParameter("status").equals("")?"":request.getParameter("status")%>'
					WIDTH="25" HEIGHT="20" border="0" hspace="2"> <%}%>
            </div>
        </li>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formStartTime" />:</div>
            <div class="input">
                <INPUT TYPE="TEXT" NAME="start_time"
                    VALUE='<%=request.getParameter("start_time")%>' WIDTH="25"
                    HEIGHT="20" border="0" onChange="checkTimeTypeIn(this);checkPageLock()">
            </div>
            <div class="space">&nbsp;</div>

            <%
				    // multisites start ==================
				    boolean bMultisites = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable();
				    SiteDao siteDao = (SiteDao)SpringUtils.getBean("siteDao");
				    List<Site> sites = siteDao.getActiveSitesByProviderNo((String) session.getAttribute("user"));
				    // multisites end ==================

				    boolean bMoreAddr = bMultisites? true : props.getProperty("scheduleSiteID", "").equals("") ? false : true;
				    String tempLoc = "";
				    if(bFirstDisp && bMoreAddr) {
				            tempLoc = (new JdbcApptImpl()).getLocationFromSchedule(dateString2, curProvider_no);
				    }
				    String loc = bFirstDisp?tempLoc:request.getParameter("location");
				    String colo = bMultisites
				                                        ? ApptUtil.getColorFromLocation(sites, loc)
				                                        : bMoreAddr? ApptUtil.getColorFromLocation(props.getProperty("scheduleSiteID", ""), props.getProperty("scheduleSiteColor", ""),loc) : "white";
			%>                                    
					<div class="input" style="text-align: right;"> <INPUT TYPE="button" NAME="typeButton" VALUE="<bean:message key="Appointment.formType"/>" onClick="openTypePopup()"> </div>

            <div class="input">
                <INPUT TYPE="TEXT" NAME="type"
                    VALUE='<%=bFirstDisp?"":request.getParameter("type").equals("")?"":request.getParameter("type")%>'
                    WIDTH="25" HEIGHT="20" border="0" hspace="2">
            </div>
        </li>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formDuration" />:</div> <!--font face="arial"> End Time :</font-->
            <div class="input">
                <INPUT TYPE="TEXT" NAME="duration"
                        VALUE="<%=duration%>" WIDTH="25" HEIGHT="20" border="0" hspace="2" onChange="checkPageLock()">
                <INPUT TYPE="hidden" NAME="end_time"
                        VALUE='<%=request.getParameter("end_time")%>' WIDTH="25"
                        HEIGHT="20" border="0" hspace="2" onChange="checkTimeTypeIn(this)">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formDoctor" />:</div>
            <div class="input">
                <INPUT type="TEXT" readonly
                       value="<%=bFirstDisp ? "" : StringEscapeUtils.escapeHtml(providerBean.getProperty(curDoctor_no,""))%>">
            </div>
        </li>
        <li class="row deep">
            <div class="label"><bean:message key="appointment.addappointment.formSurName" />:</div>
            <div class="input">
            	<% 
            		String name="";
            		name = String.valueOf((bFirstDisp && !bFromWL)?"":request.getParameter("name")==null?session.getAttribute("appointmentname")==null?"":session.getAttribute("appointmentname"):request.getParameter("name"));
            	%>
                <INPUT TYPE="TEXT" NAME="keyword"
                        VALUE="<%=name%>"
                        HEIGHT="20" border="0" hspace="2" width="25" tabindex="1">
            </div>
            <div class="space">
                <a href=# onclick="onNotBook();"><font size='-1' color='brown'>Not book</font></a>
            </div>
            <INPUT TYPE="hidden" NAME="orderby" VALUE="last_name, first_name">
<%
    String searchMode = request.getParameter("search_mode");
    if (searchMode == null || searchMode.isEmpty()) {
        searchMode = OscarProperties.getInstance().getProperty("default_search_mode","search_name");
    }
%> 
            <INPUT TYPE="hidden" NAME="search_mode" VALUE="<%=searchMode%>"> 
            <INPUT TYPE="hidden" NAME="originalpage" VALUE="../appointment/addappointment.jsp"> 
            <INPUT TYPE="hidden" NAME="limit1" VALUE="0"> 
            <INPUT TYPE="hidden" NAME="limit2" VALUE="5"> 
            <INPUT TYPE="hidden" NAME="ptstatus" VALUE="active"> 
			<input type="hidden" name="outofdomain" value="<%=OscarProperties.getInstance().getProperty("pmm.client.search.outside.of.domain.enabled","true")%>"/> 
            <!--input type="hidden" name="displaymode" value="Search " -->
            <div class="label">
                <INPUT TYPE="submit" name="searchBtn" id="searchBtn" style="width:auto;"
                    onclick="document.forms['ADDAPPT'].displaymode.value='Search '"
                    VALUE="<bean:message key="appointment.addappointment.btnSearch"/>">
            </div>
            <div class="input">
                <input type="TEXT" name="demographic_no"
                    ONFOCUS="onBlockFieldFocus(this)" readonly
                    value='<%=(bFirstDisp && !bFromWL)?"":request.getParameter("demographic_no").equals("")?"":request.getParameter("demographic_no")%>'
                    width="25" height="20" border="0" hspace="2">
            </div>
        </li>
        <li class="row deep">
            <div class="label">Coded Reason:</div>
            <div class="input">
                <select name="reasonCode">                
	                <c:choose>
	                	<c:when test="${ not empty reasonCodes  }">
	                		<c:forEach items="${ reasonCodes.items }" var="reason" >
	                		<c:if test="${ reason.active }">
	                			<option value="${ reason.id }" id="${ reason.value }" >
	                				<c:out value="${ reason.label }" />
	                			</option>
	                		</c:if>
	                		</c:forEach>     
	                	</c:when>
	                	<c:otherwise>
	                		<option value="-1">Other</option>
	                	</c:otherwise>
	                </c:choose>
				</select>
			</div>
            <div class="space">&nbsp;</div>
            
            <%if(oceanNumReferrals != null && oceanNumReferrals>0) {
            	SimpleDateFormat formatFrom = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            	SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            %>
	            <div class="label">eReferrals</div>
	            <div class="input">
	            	<select name="oceanReferral" id="oceanReferral" onChange="updateFieldsUsingOCEAN()">
	            		<option></option>
	            		<%for(JSONObject refData : oceanReferrals) { 
	            			JSONObject ref = refData.getJSONObject("referral");
	           
	            			JSONObject data = refData.getJSONObject("data");
	            		
	            		%>
	            			<option case1="<%=data.getString("case") %>" sentDate="<%=ref.getString("sentDate") %>" srcSiteName="<%=ref.getString("srcSiteName") %>" value="<%=ref.getString("referralRef")%>" reason="<%=data.getString("reasonforreferral")%>" refsource="<%=data.getString("refSource")%>"><%=ref.getString("description")%> (Created : <%=formatTo.format(formatFrom.parse(ref.getString("creationDate")))%>)</option>
	            		<% } %>
	            	</select>
	            </div>
            <% } else { %>
            		<div class="label"><span id="oceanExtLabel">&nbsp;</span></div>
            		<div class="input"><span id="oceanExtInput">&nbsp;</span></div>
            <% } %>
        </li>
        <li class="row deep">
            <div class="label"><bean:message key="Appointment.formReason" />:</div>
            <div class="input">
             
				<textarea id="reason" name="reason" tabindex="2" rows="2" wrap="virtual" cols="18"><%=bFirstDisp?"":request.getParameter("reason").equals("")?"":request.getParameter("reason")%></textarea>
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formNotes" />:</div>
            <div class="input">
                <textarea name="notes" tabindex="3" rows="2" wrap="virtual" cols="18"><%=bFirstDisp?"":request.getParameter("notes").equals("")?"":request.getParameter("notes")%></textarea>
            </div>
        </li>
        <% if (pros.isPropertyActive("mc_number")) { %>
        <li class="row deep">
            <div class="label">M/C number:</div>
            <div class="input">
                <input type="text" name="appt_mc_number" tabindex="4" />
            </div>
            <div class="space">&nbsp;</div>
            <div class="label">&nbsp;</div>
            <div class="input">&nbsp;</div>
        </li>
        <% } %>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formLocation" />:</div>

            <div class="input">
		<% // multisites start ==================
		if (bMultisites) { %>
	        <select tabindex="4" name="location" style="background-color: <%=colo%>" onchange='this.style.backgroundColor=this.options[this.selectedIndex].style.backgroundColor'>
	        <% for (Site s:sites) { %>
	                <option value="<%=s.getName()%>" style="background-color: <%=s.getBgColor()%>" <%=s.getName().equals(loc)?"selected":"" %>><%=s.getName()%></option>
	        <% } %>
	        </select>
		<% } else {
			// multisites end ==================
			if (locationEnabled) {
		%>
			<select name="location" >
                <%
                String sessionLocation = "";
                ProgramProvider programProvider = programManager2.getCurrentProgramInDomain(loggedInInfo, loggedInInfo.getLoggedInProviderNo());
                if(programProvider!=null && programProvider.getProgram() != null) {
                	sessionLocation = programProvider.getProgram().getId().toString();
                }
                if (programs != null && !programs.isEmpty()) {
			       	for (Program program : programs) {
			       	    String description = StringUtils.isBlank(program.getLocation()) ? program.getName() : program.getLocation();
			   	%>
			        <option value="<%=program.getId()%>" <%=program.getId().toString().equals(sessionLocation) ? "selected='selected'" : ""%>><%=StringEscapeUtils.escapeHtml(description)%></option>
			    <%	}
                }
			  	%>
            </select>
        	<% } else { %>
        	<input type="TEXT" name="location" tabindex="4" value="<%=loc%>" width="25" height="20" border="0" hspace="2">	
        	<% } %>
		<% } %>
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formResources" />:</div>
            <div class="input">
                <input type="TEXT" name="resources"
                    tabindex="5"
                    value='<%=bFirstDisp?"":request.getParameter("resources").equals("")?"":request.getParameter("resources")%>'
                    width="25" height="20" border="0" hspace="2">
            </div>
        </li>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formCreator" />:</div>
            <div class="input">
                <INPUT TYPE="TEXT" NAME="user_id" readonly
                    VALUE='<%=bFirstDisp?(StringEscapeUtils.escapeHtml(userlastname)+", "+StringEscapeUtils.escapeHtml(userfirstname)):request.getParameter("user_id").equals("")?"Unknown":request.getParameter("user_id")%>'
                    WIDTH="25" HEIGHT="20" border="0" hspace="2">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formDateTime" />:</div>
            <div class="input">
<%
            GregorianCalendar now=new GregorianCalendar();
            GregorianCalendar cal = (GregorianCalendar) now.clone();
            cal.add(GregorianCalendar.DATE, 1);
            String strDateTime=now.get(Calendar.YEAR)+"-"+(now.get(Calendar.MONTH)+1)+"-"+now.get(Calendar.DAY_OF_MONTH)+" "
                + now.get(Calendar.HOUR_OF_DAY)+":"+now.get(Calendar.MINUTE)+":"+now.get(Calendar.SECOND);
%>
                <INPUT TYPE="TEXT" NAME="createdatetime" readonly VALUE="<%=strDateTime%>" WIDTH="25" HEIGHT="20" border="0" hspace="2">
                <INPUT TYPE="hidden" NAME="provider_no" VALUE="<%=curProvider_no%>">
                <INPUT TYPE="hidden" NAME="dboperation" VALUE="search_titlename">
                <INPUT TYPE="hidden" NAME="creator" VALUE='<%=StringEscapeUtils.escapeHtml(userlastname)+", "+StringEscapeUtils.escapeHtml(userfirstname)%>'>
                <INPUT TYPE="hidden" NAME="remarks" VALUE="">
            </div>
        </li>
        <li class="row weak">
            <% String emailReminder = pros.getProperty("emailApptReminder");
               if ((emailReminder != null) && emailReminder.equalsIgnoreCase("yes")) { %>
                    <div class="label"><bean:message key="Appointment.formEmailReminder" />:</div>
                    <div class="input"><input type="checkbox" name="emailPt" value="email reminder"></div>
             <%  }else { %>
                    <div class="label"></div>
                    <div class="input"></div>
	     <%  }%>

            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formCritical" />:</div>
            <div class="input">
            	<input type="checkbox" name="urgency" value="critical"/>
            </div>
        </li>
        
         <li class="row weak">
           
            <div class="label">Referral Source Prior to Ambulatory Care Visit:</div>
            <div class="input">
            	<select name="referralSourcePriorToAmbulatoryCareVisit" id="referralSourcePriorToAmbulatoryCareVisit">
						<option value=""></option>
						<option value="Self/family member, caretaker, guardian">Self/family member, caretaker, guardian</option>
						<option value="Inpatient service (reporting or other facility)">Inpatient service (reporting or other facility)</option>
						<option value="Ambulatory care service (reporting or other facility)">Ambulatory care service (reporting or other facility)</option>
						<option value="Private practice">Private practice</option>
						<option value="Drug dependency program">Drug dependency program</option>
						<option value="Community health service">Community health service</option>
						<option value="Residential care facility">Residential care facility</option>
						<option value="Legal service">Legal service</option>
						<option value="Educational agency">Educational agency</option>
						<option value="Home care">Home care</option>
						<option value="Mental health facility">Mental health facility</option>
						<option value="Other">Other</option>
						<option value="Unknown/unavailable">Unknown/unavailable</option>
				</select>
            </div>
            <div class="space">&nbsp;</div>
            <div class="label">Date ready for rehab/Referral date (yyyy-mm-dd):</div>
            <div class="input">
				<input type="text" name="referralDate" id="referralDate" value=""/>
            </div>
        </li>
        
         <li class="row weak">
            
                    <div class="label">Institution From:</div>
                    <div class="input">
            	<select name="institutionFrom" id="institutionFrom">
						<option value=""></option>
						<option value="4844">LAKERIDGE HEALTH-AJAX SITE</option>
						<option value="1870">GLENGARRY MEMORIAL HOSPITAL</option>
						<option value="1254">ALMONTE GENERAL HOSPITAL</option>
						<option value="1799">ARNPRIOR REGIONAL HEALTH</option>
						<option value="2147">ATIKOKAN GENERAL HOSPITAL</option>
						<option value="4702">WEENEEBAYKO AREA HLTH AUTH-ATTAWAPISKAT</option>
						<option value="3991">QUINTE HEALTHCARE CORPORATION-BANCROFT</option>
						<option value="1825">ROYAL VICTORIA REGIONAL HEALTH CENTRE</option>
						<option value="1801">ST FRANCIS MEMORIAL HOSPITAL</option>
						<option value="3988">QUINTE HEALTHCARE CORPORATION-BELLEVILLE</option>
						<option value="2057">NORTH SHORE HLTH NTWRK-BLIND RIVER SITE</option>
						<option value="4619">MUSKOKA ALGONQUIN HEALTHCARE-BRACEBRIDGE</option>
						<option value="4681">WILLIAM OSLER HEALTH SYSTEM-CIVIC SITE</option>
						<option value="4675">BRANT COMMUNITY HEALTHCARE SYS-BRANTFORD</option>
						<option value="1273">BROCKVILLE GENERAL HOSP-CHARLES ST SITE</option>
						<option value="1160">JOSEPH BRANT HOSPITAL</option>
						<option value="1905">CAMBRIDGE MEMORIAL HOSPITAL</option>
						<option value="1829">CANADIAN ARMED FORCES BASE HOSPITAL</option>
						<option value="1597">CAMPBELLFORD MEMORIAL HOSPITAL</option>
						<option value="1256">CARLETON PLACE AND DISTRICT MEM HOSPITAL</option>
						<option value="2173">SERVICES DE SANTE DE CHAPLEAU HLTH SERV</option>
						<option value="1223">PUBLIC GENERAL HOSP SOCIETY OF CHATHAM</option>
						<option value="4042">SOUTH BRUCE GREY HEALTH CENTRE-CHESLEY</option>
						<option value="4008">LAKERIDGE HEALTH -BOWMANVILLE</option>
						<option value="1199">CLINTON PUBLIC HOSPITAL</option>
						<option value="3860">NORTHUMBERLAND HILLS HOSPITAL</option>
						<option value="2078">LADY MINTO HOSPITAL (THE)</option>
						<option value="1833">COLLINGWOOD GENERAL AND MARINE HOSPITAL</option>
						<option value="4451">CORNWALL COMMUNITY HOSPITAL</option>
						<option value="1803">DEEP RIVER AND DISTRICT HOSPITAL</option>
						<option value="2103">DRYDEN REGIONAL HEALTH CENTRE</option>
						<option value="1146">HALDIMAND WAR MEMORIAL HOSPITAL</option>
						<option value="4036">SOUTH BRUCE GREY HEALTH CENTRE-DURHAM</option>
						<option value="2058">ST JOSEPH'S GENERAL HOSPITAL</option>
						<option value="2148">RIVERSIDE HEALTH CARE FAC-EMO SITE</option>
						<option value="2204">ENGLEHART AND DISTRICT HOSPITAL</option>
						<option value="2174">ESPANOLA GENERAL HOSPITAL</option>
						<option value="1203">SOUTH HURON HOSPITAL</option>
						<option value="1936">GROVES MEMORIAL COMMUNITY HOSPITAL</option>
						<option value="4700">WEENEEBAYKO AREA HLTH AUTH-FORT ALBANY</option>
						<option value="4210">NIAGARA HEALTH SYSTEM-FORT ERIE DOUGLAS</option>
						<option value="2150">RIVERSIDE HEALTH CARE FAC-LAVERENDRYE</option>
						<option value="2175">GERALDTON DISTRICT HOSPITAL</option>
						<option value="1206">ALEXANDRA MARINE AND GENERAL HOSPITAL</option>
						<option value="4788">HAMILTON HLTH SCIENCES CORP-WEST LINCOLN</option>
						<option value="1946">GUELPH GENERAL HOSPITAL</option>
						<option value="1149">WEST HALDIMAND GENERAL HOSPITAL</option>
						<option value="3737">HALIBURTON HIGHLANDS HLTH SERV CORP-HALI</option>
						<option value="4622">HALTON HEALTHCARE SERVICES CORP-GEORGETO</option>
						<option value="1982">HAMILTON HEALTH SCIENCES CORP-GENERAL</option>
						<option value="1983">HAMILTON HEALTH SCIENCES CORP-JURAVINSKI</option>
						<option value="1994">HAMILTON HEALTH SCIENCES CORP-MCMASTER</option>
						<option value="2003">ST JOSEPH'S HEALTH CARE SYSTEM-HAMILTON</option>
						<option value="1124">HANOVER AND DISTRICT HOSPITAL</option>
						<option value="1777">HAWKESBURY AND DISTRICT GENERAL HOSPITAL</option>
						<option value="2082">HOPITAL NOTRE DAME HOSPITAL (HEARST)</option>
						<option value="2061">HORNEPAYNE COMMUNITY HOSPITAL</option>
						<option value="4616">MUSKOKA ALGONQUIN HEALTHCARE-HUNTSVILLE</option>
						<option value="1696">ALEXANDRA HOSPITAL</option>
						<option value="2084">ANSON GENERAL HOSPITAL</option>
						<option value="2088">SENSENBRENNER HOSPITAL (THE)</option>
						<option value="1284">KEMPTVILLE DISTRICT HOSPITAL</option>
						<option value="2110">LAKE-OF-THE-WOODS DISTRICT HOSPITAL</option>
						<option value="3907">SOUTH BRUCE GREY HEALTH CTR-KINCARDINE</option>
						<option value="4830">KINGSTON HEALTH SCIENCES CENTRE -HOTEL DIEU</option>
						<option value="4831">KINGSTON HEALTH SCIENCES CENTRE -KINGSTON GENERAL</option>
						<option value="2211">KIRKLAND AND DISTRICT HOSPITAL</option>
						<option value="1921">ST MARY'S GENERAL HOSPITAL</option>
						<option value="3734">GRAND RIVER HOSPITAL CORP-WATERLOO SITE</option>
						<option value="1067">ERIE SHORES HEALTHCARE</option>
						<option value="1893">ROSS MEMORIAL HOSPITAL</option>
						<option value="1030">GREY BRUCE HEALTH SERVICES-LIONS HEAD</option>
						<option value="1740">LISTOWEL MEMORIAL HOSPITAL</option>
						<option value="2121">MANITOULIN HEALTH CENTRE-LITTLE CURRENT</option>
						<option value="1497">ST.JOSEPH'S HEALTH CARE,LONDON</option>
						<option value="3850">LONDON HLTH SCIENCES CTR-UNIVERSITY HOSP</option>
						<option value="4359">LONDON HLTH SCIENCES CTR-VICTORIA HOSP</option>
						<option value="4602">ST.JOSEPH'S HEALTH CARE,LONDON-PARKWOOD</option>
						<option value="2176">MANITOUWADGE GENERAL HOSPITAL</option>
						<option value="4819">NORTH OF SUPERIOR HLTHCARE GR-WILSON MEM</option>
						<option value="4025">GREY BRUCE HEALTH SERVICES-MARKDALE SITE</option>
						<option value="2049">SHOULDICE HOSPITAL</option>
						<option value="3587">MARKHAM STOUFFVILLE HOSPITAL</option>
						<option value="2090">BINGHAM MEMORIAL HOSPITAL</option>
						<option value="2126">MATTAWA GENERAL HOSPITAL</option>
						<option value="4027">GREY BRUCE HEALTH SERVICES-MEAFORD SITE</option>
						<option value="1844">GEORGIAN BAY GENERAL HOSP-MIDLAND SITE</option>
						<option value="4022">HALTON HEALTHCARE SERVICES CORP-MILTON</option>
						<option value="2123">MANITOULIN HEALTH CENTRE-MINDEMOYA UNIT</option>
						<option value="4747">TRILLIUM HEALTH PARTNERS-CREDIT VALLEY</option>
						<option value="4752">TRILLIUM HEALTH PARTNERS-MISSISSAUGA S</option>
						<option value="4698">WEENEEBAYKO AREA HLTH AUTH-MOOSE FACTORY</option>
						<option value="4323">NORTH WELLINGTON HLTH CARE-MOUNT FOREST</option>
						<option value="1295">LENNOX AND ADDINGTON COUNTY GEN HOSPITAL</option>
						<option value="2207">TEMISKAMING HOSPITAL</option>
						<option value="1817">STEVENSON MEMORIAL HOSPITAL ALLISTON</option>
						<option value="1507">FOUR COUNTIES HEALTH SERVICES CORP</option>
						<option value="2038">SOUTHLAKE REGIONAL HEALTH CENTRE</option>
						<option value="4213">NIAGARA HEALTH SYSTEM-GREATER NIAGARA</option>
						<option value="2178">NIPIGON DISTRICT MEMORIAL HOSPITAL</option>
						<option value="4730">NORTH BAY REGIONAL HEALTH CENTRE</option>
						<option value="3926">HALTON HEALTHCARE SERVICES CORP-OAKVILLE</option>
						<option value="3684">HEADWATERS HEALTH CARE CENTRE-DUFFERIN</option>
						<option value="1853">ORILLIA SOLDIERS' MEMORIAL HOSPITAL</option>
						<option value="3932">LAKERIDGE HEALTH -OSHAWA SITE</option>
						<option value="1657">CHILDREN'S HOSPITAL OF EASTERN ONTARIO</option>
						<option value="1661">HOPITAL MONTFORT</option>
						<option value="1681">QUEENSWAY-CARLETON HOSPITAL</option>
						<option value="4046">OTTAWA HOSPITAL ( THE )-CIVIC SITE</option>
						<option value="4048">OTTAWA HOSPITAL ( THE )-GENERAL SITE</option>
						<option value="4164">UNIVERSITY OF OTTAWA HEART INSTITUTE</option>
						<option value="4599">ROYAL OTTAWA HLTH CARE GROUP-MENTAL HLTH</option>
						<option value="3944">GREY BRUCE HEALTH SERVICES-OWEN SOUND</option>
						<option value="4326">NORTH WELLINGTON HLTH CARE-PALMERSTON</option>
						<option value="3729">WEST PARRY SOUND HEALTH CENTRE</option>
						<option value="1804">PEMBROKE REGIONAL HOSPITAL INC.</option>
						<option value="3732">PERTH & SMITHS FALLS DIST-PERTH SITE</option>
						<option value="1768">PETERBOROUGH REGIONAL HEALTH CENTRE</option>
						<option value="4418">BLUEWATER HEALTH-PETROLIA SITE</option>
						<option value="3992">QUINTE HEALTHCARE CORPORATION-PICTON</option>
						<option value="2153">RIVERSIDE HEALTH CARE FAC-RAINY RIVER</option>
						<option value="2115">RED LAKE MARG COCHENOUR MEM HOSP (THE)</option>
						<option value="1813">RENFREW VICTORIA HOSPITAL</option>
						<option value="2046">MACKENZIE HEALTH</option>
						<option value="4415">BLUEWATER HEALTH-SARNIA GENERAL SITE</option>
						<option value="4407">SAULT AREA HOSPITAL-SAULT STE MARIE</option>
						<option value="4836">SCARBOROUGH AND ROUGE HOSPITAL-CENTENARY</option>
						<option value="4840">SCARBOROUGH AND ROUGE HOSPITAL -SCAR.GEN.SITE</option>
						<option value="4842">SCARBOROUGH AND ROUGE HOSPITAL -BIRCHMOUNT </option>
						<option value="4005">LAKERIDGE HEALTH -PORT PERRY</option>
						<option value="1213">SEAFORTH COMMUNITY HOSPITAL</option>
						<option value="1591">NORFOLK GENERAL HOSPITAL</option>
						<option value="4353">SIOUX LOOKOUT MENO-YA-WIN HLTH CTR-DISTR</option>
						<option value="1269">PERTH & SMITHS FALLS DIST-SMITHS FALLS</option>
						<option value="2094">SMOOTH ROCK FALLS HOSPITAL</option>
						<option value="4030">GREY BRUCE HEALTH SERVICES-SOUTHAMPTON</option>
						<option value="4224">NIAGARA HEALTH SYSTEM-ST CATHARINES GEN</option>
						<option value="1748">ST MARYS MEMORIAL HOSPITAL</option>
						<option value="1059">ST THOMAS-ELGIN GENERAL HOSPITAL</option>
						<option value="1754">STRATFORD GENERAL HOSPITAL</option>
						<option value="1515">STRATHROY MIDDLESEX GENERAL HOSPITAL</option>
						<option value="2812">WEST NIPISSING GENERAL HOSPITAL</option>
						<option value="4059">HEALTH SCIENCES NORTH-LAURENTIAN</option>
						<option value="4736">NORTH BAY REGIONAL HEALTH CTR-KIRKWOOD P</option>
						<option value="4822">NORTH OF SUPERIOR HLTHCARE GR-MCCAUSLAND</option>
						<option value="4770">NORTH SHORE HLTH NTWRK-THESSALON SITE</option>
						<option value="3853">THUNDER BAY REGIONAL HLTH SCIENCES CTR</option>
						<option value="1709">TILLSONBURG DISTRICT MEMORIAL HOSPITAL</option>
						<option value="3414">TIMMINS & DISTRICT GENERAL HOSPITAL</option>
						<option value="1302">TOR. EAST HLTH NTWRK-MICHAEL GARRON HOSP</option>
						<option value="1330">NORTH YORK GENERAL HOSPITAL</option>
						<option value="1332">DON MILLS SURGICAL UNIT</option>
						<option value="1406">HOSPITAL FOR SICK CHILDREN  (THE)</option>
						<option value="1443">ST JOSEPH'S HEALTH CENTRE</option>
						<option value="1444">ST MICHAEL'S HOSPITAL</option>
						<option value="3240">SALVATION ARMY TORONTO GRACE HLTH CTR PL</option>
						<option value="3438">BELLWOOD HEALTH SERVICES INC (SCARB)</option>
						<option value="3459">CASEY HOUSE HOSPICE</option>
						<option value="3910">UNIVERSITY HEALTH NETWORK</option>
						<option value="3929">WILLIAM OSLER HEALTH SYSTEM-ETOBICOKE</option>
						<option value="3936">SUNNYBROOK HEALTH SCIENCES CENTRE</option>
						<option value="4761">CENTRE FOR ADDICTION & MENTAL HEALTH</option>
						<option value="4799">HUMBER RIVER HOSPITAL - WILSON SITE</option>
						<option value="4804">SINAI HEALTH SYSTEM-MOUNT SINAI SITE</option>
						<option value="1193">CANADIAN FORCES HOSPITAL</option>
						<option value="3994">QUINTE HEALTHCARE CORPORATION-TRENTON</option>
						<option value="4465">MARKHAM STOUFFVILLE HOSP-UXBRIDGE SITE</option>
						<option value="4039">SOUTH BRUCE GREY HEALTH CENTRE-WALKERTON</option>
						<option value="1239">SYDENHAM DISTRICT HOSPITAL</option>
						<option value="2076">LADY DUNN HEALTH CENTRE</option>
						<option value="4227">NIAGARA HEALTH SYSTEM-WELLAND COUNTY</option>
						<option value="4796">ONTARIO SHORES CTR FOR MENTAL HLTH SCIEN</option>
						<option value="4033">GREY BRUCE HEALTH SERVICES-WIARTON SITE</option>
						<option value="1885">WINCHESTER DISTRICT MEMORIAL HOSPITAL</option>
						<option value="1079">WINDSOR REGIONAL HOSPITAL-METROPOLITAN</option>
						<option value="4607">WINDSOR REGIONAL HOSPITAL-MARYVALE SITE</option>
						<option value="4773">WINDSOR REGIONAL HOSP-OUELLETTE CAMPUS</option>
						<option value="1217">WINGHAM AND DISTRICT HOSPITAL</option>
						<option value="1716">WOODSTOCK GENERAL HOSPITAL TRUST</option>
					</select>                    
                    
                    </div>
	  
            <div class="space">&nbsp;</div>
            <div class="label"></div>
            <div class="input">
            	
            </div>
        </li>
        
        
         <li class="row weak">
           
                    <div class="label">Visit Disposition:</div>
                    <div class="input">
                    	<select name="visitDisposition" style="width:150px">
                    		<option value=""></option>
                    		<option value="16">Home with support/referral</option>
                    		<option value="17">Private home</option>
                    		<option value="06">Admit to reporting facility as inpatient to special care unit or O.R. from ambulatory care visit</option>
               				<option value="07">Admit to reporting facility as an inpatient to another unit of the reporting facility from ambulatory care visit</option>
               				<option value="08">Transfer to another acute care facility directly from ambulatory care visit</option>
               				<option value="09">Transfer to another non acute care facility directly from ambulatory care visit</option>
                    		<option value="12">Intra-facility transfer to day surgery</option>
                    		<option value="13">Intra-facility trasnfer to the ED</option>
                    		<option value="14">Intra-facility transfer to clinic</option>
                    	</select>
                    </div>
	   
            <div class="space">&nbsp;</div>
            <div class="label">Reason for Discharge:</div>
            <div class="input">
            	<select name="reasonForDischarge" style="width:150px">
            		<option value=""></option>
            		<option value="1">Treatment Completed (ie goals complete, plateau in rehab recovery, etc.)</option>
            		<option value="2">Treatment Incomplete: Change in Medical Status (admission to inpatient or death)</option>
            		<option value="3">Treatment Incomplete: Transferred to another OPR</option>
            		<option value="4">Treatment Incomplete: Patient Choice</option>
            		<option value="5">Treatment Incomplete: Unknown/Other (includes patients who do not attend after only attending one visit)</option>
            		<option value="9">Unknown or Unavailable</option>
            	</select>
            </div>
        </li>
        
        
        <li class="row weak">
           
                    <div class="label">Main Problem:</div>
                    <div class="input">
                    	<select name="mainProblem">
                    		<option value=""></option>
                    		<option value="Z509">Rehabilitation</option>
                    	</select>
                    </div>
	   
            <div class="space">&nbsp;</div>
            <div class="label">Other Problem:</div>
            <div class="input">
            	<select name="otherProblem">
            		<option value=""></option>
            		<option value="I64">Stroke - Unable to determine</option>
					<option value="I619">Stroke - Intracerebral hemorrhage</option>
					<option value="I634">Stroke - Ischemic</option>
					<option value="G459">Stroke - TIA</option>
					<option value="G939">Acquired Brain Injury - Non-Traumatic</option>
					<option value="C719">Acquired Brain Injury - Malignant brain tumor</option>
					<option value="D332">Acquired Brain Injury - Benign brain tumor</option>
					<option value="S069">Acquired Brain Injury - Traumatic</option>
					<option value="S060">Acquired Brain Injury - Concussion</option>
					<option value="G35">Neurological - Multiple Sclerosis</option>
					<option value="G20">Neurological - Parkinson's</option>
					<option value="G629">Neurological - Polyneuropathies</option>
					<option value="G610">Neurological - Guillain-Barre</option>
					<option value="G809">Neurological - Cerebral Palsy</option>
					<option value="G589">Neurological - Mononeuropathies of Limb</option>
					<option value="G549">Neurological - Nerve Root and Plexus Disorders</option>
					<option value="G510">Neurological - Bell's Palsy</option>
					<option value="G1220">Neurological - Amyotrophic Lateral Sclerosis (ALS)</option>
					<option value="G710">Neurological - Duchene Muscular Dystrophy</option>
					<option value="M7929">Neurological - Neuralgia</option>
					<option value="G319">Neurological - Degenerative disease</option>
					<option value="G969">Neurological - Other</option>
					<option value="G82293">Spinal Cord Dysfunction - Paraplegia</option>
					<option value="G82591">Spinal Cord Dysfunction - Quadriplegia</option>
					<option value="D334">Spinal Cord Dysfunction - Benign tumor</option>
					<option value="C720">Spinal Cord Dysfunction - Malignant tumor</option>
					<option value="Z890">Amputation - Fingers/Thumb</option>
					<option value="Z891">Amputation - Hand/Wrist</option>
					<option value="Z892">Amputation - Upper Limb Above Wrist</option>
					<option value="Z893">Amputation - Bilateral Upper Limbs</option>
					<option value="Z894">Amputation - Foot/Ankle</option>
					<option value="Z896">Amputation - Lower Extremity Above the Knee</option>
					<option value="Z895">Amputation - Lower Extremity Below the Knee</option>
					<option value="Z897">Amputation - Bilateral Lower Limbs</option>
					<option value="Z898">Amputation - Upper and Lower Limbs</option>
					<option value="Z899">Amputation - Other Amputation</option>
					<option value="M069">Arthritis - Rheumatoid</option>
					<option value="M199">Arthritis - Osteoarthritis</option>
					<option value="M1399">Arthritis - Other</option>
					<option value="M542">Persistent Pain - Neck Pain</option>
					<option value="M549">Persistent Pain - Back Pain</option>
					<option value="M7969">Persistent Pain - Extremity Pain</option>
					<option value="M8902">Persistent Pain - Complex Regional Pain Syndrome</option>
					<option value="R529">Persistent Pain - Other</option>
					<option value="M797">Persistent Pain - Fibromyalgia</option>
					<option value="S72090">Ortho -Traumatic Hip Fracture</option>
					<option value="M8445">Ortho - Pathological/Non-Traumatic Hip Fracture</option>
					<option value="S32800">Ortho - Pelvis Fracture</option>
					<option value="T0290">Ortho - Major Multiple Fracture</option>
					<option value="Z9660">Ortho - Hip Replacement</option>
					<option value="Z9661">Ortho - Knee Replacement</option>
					<option value="M750">Ortho - Shoulder - Adhesive capsulitis of Shoulder</option>
					<option value="S43090">Ortho - Shoulder - Dislocated Shoulder</option>
					<option value="S42390">Ortho - Shoulder - Fracture of Humerus</option>
					<option value="S42090">Ortho - Shoulder - Fracture of Clavicle</option>
					<option value="M754">Ortho - Shoulder - Impingement Syndrome</option>
					<option value="S43402">Ortho - Shoulder - Other sprain/strain/tear</option>
					<option value="M751">Ortho - Shoulder - Rotator Cuff Syndrome/Tear</option>
					<option value="M758">Ortho - Shoulder - Tendinopathy</option>
					<option value="S5349">Ortho - Elbow - Sprain/Strain/Tear</option>
					<option value="M770">Ortho - Elbow - Epicondylitis - medial</option>
					<option value="M771">Ortho - Elbow - Epicondylitis - lateral</option>
					<option value="S42090">Ortho - Elbow - Fracture</option>
					<option value="G560">Ortho - Forearm/Wrist - Carpal Tunnel</option>
					<option value="S62800">Ortho - Forearm/Wrist - Fracture</option>
					<option value="S6359">Ortho - Forearm/Wrist - Strain/Sprain</option>
					<option value="S5498">Ortho - Forearm - Injury of Nerves</option>
					<option value="S5688">Ortho - Forearm - Injury of Muscle & Tendon</option>
					<option value="S599">Ortho - Forearm - Other Injuries</option>
					<option value="S62800">Ortho - Hand - Fracture - Finger/Hand</option>
					<option value="S6498">Ortho - Hand - Injury of Nerves</option>
					<option value="S699">Ortho - Hand - Other Injuries</option>
					<option value="S678">Ortho - Hand - Crushing Injuries</option>
					<option value="S6379">Ortho - Hand - Other Hand Sprain/Strain/Tear</option>
					<option value="M779">Ortho - Hip - Hamstring Tendonitis</option>
					<option value="S799">Ortho - Hip - Other Injuries</option>
					<option value="S7319">Ortho - Hip - Sprain/Strain/Tear</option>
					<option value="S336">Ortho - Hip - Sacro-Iliac Strain</option>
					<option value="S800">Ortho - Knee - Contusion</option>
					<option value="M229">Ortho - Knee - Disorders of Patella</option>
					<option value="S82400">Ortho - Knee - Fracture of Fibula</option>
					<option value="S82200">Ortho - Knee - Fracture of Tibia</option>
					<option value="M239">Ortho - Knee - Internal Derangement of Knee</option>
					<option value="S899">Ortho - Knee - Other Unspecified Injuries</option>
					<option value="S836">Ortho - Knee - Other Knee/Leg Sprain/Strain/Tear</option>
					<option value="S9349">Ortho - Ankle - Sprain/Strain/Tear</option>
					<option value="S999">Ortho - Ankle - Other Unspecified Injuries</option>
					<option value="S82890">Ortho - Ankle - Fracture</option>
					<option value="M773">Ortho - Foot - Calcaneal Spur</option>
					<option value="S92900">Ortho - Foot - Fracture</option>
					<option value="S9498">Ortho - Foot - Injury of Nerves</option>
					<option value="S9698">Ortho - Foot - Injury of Muscle & Tendon</option>
					<option value="S999">Ortho - Foot - Unspecified Injuries</option>
					<option value="M722">Ortho - Foot - Plantar Fasciitis</option>
					<option value="S1428">Ortho - Back - Cervical - Nerve Injury</option>
					<option value="M45">Ortho - Back - Mechanical (Arthritis & Mechanical)</option>
					<option value="S2428">Ortho - Back - Thoracic - Nerve Injury</option>
					<option value="S3428">Ortho - Back - Lumbar & Sacral - Nerve Injury</option>
					<option value="M6099">Ortho - Myositis</option>
					<option value="M9999">Ortho - Other - Musculoskeletal condition</option>
					<option value="M4199">Ortho - Postural Dysfunction - Scoliosis</option>
					<option value="M4059">Ortho - Postural Dysfunction - Lordosis</option>
					<option value="M6599">Ortho - Synovitis and Tenosynovitis</option>
					<option value="S034">Ortho - Temporomandibular Strain/Sprain</option>
					<option value="I509">Cardiac - Heart Failure</option>
					<option value="I409">Cardiac - Myocarditis</option>
					<option value="Z951">Cardiac - Surgical - CABG</option>
					<option value="Z9500">Cardiac - Surgical - Pacemaker</option>
					<option value="Z952">Cardiac - Surgical - Valve Replacement</option>
					<option value="Z955">Cardiac - Surgical - PCI/Coronary angioplasty</option>
					<option value="Q249">Cardiac - Adult Congenital Heart Disease (ACHD)</option>
					<option value="J449">Pulmonary - COPD</option>
					<option value="Z991">Pulmonary - Respiratory Ventilator</option>
					<option value="J4590">Pulmonary - Asthma</option>
					<option value="J989">Pulmonary - Other</option>
					<option value="T20">Burns - Head and Neck</option>
					<option value="T21">Burns - Trunk</option>
					<option value="T22">Burns - Arm and Shoulder</option>
					<option value="T23">Burns - Wrist and Hand</option>
					<option value="T24">Burns - Hip and Leg</option>
					<option value="T25">Burns - Ankle and Foot</option>
					<option value="T27">Burns - Respiratory Tract</option>
					<option value="T28">Burns - Other Internal Organs</option>
					<option value="T29">Burns - Multiple Body Regions</option>
					<option value="T35">Burns - Frostbite</option>
					<option value="Q059">Congenital Deformities - Spina Bifida</option>
					<option value="Q898">Congenital Deformities - Other</option>
					<option value="H819">Disorders of Vestibular Function</option>
					<option value="G259">Movement Disorder - Other</option>
					<option value="R53">Debility/Frailty - Deconditioned</option>
					<option value="C809">Neoplasm(s)</option>
					<option value="I890">Circulatory Disorder - Lymphedema</option>
					<option value="I739">Circulatory Disorder - Peripheral Vascular Disease</option>
					<option value="T889">Medical/Surgical Complications</option>
					<option value="Z941">Transplants - Heart</option>
					<option value="Z942">Transplants - Lung</option>
					<option value="Z944">Transplants - Liver</option>
					<option value="Z940">Transplants - Kidney</option>
					<option value="Z9480">Transplants - Bone Marrow</option>
					<option value="Z943">Transplants - Heart and lung</option>
					<option value="R69">Other - Term not on the list</option>
            		
            	</select>
            </div>
        </li>
        
             <li class="row weak">
           
                    <div class="label">Other Problem Prefix:</div>
                    <div class="input">
                    	<select name="otherProblemPrefix" style="width:150px">
                    		<option value=""></option>
                    		<option value="D">Total/hemi-arthroplasty</option>
							<option value="K">Reverse arthroplasty</option>
							<option value="W">Left body impairment</option>
							<option value="X">Right body impairment</option>
							<option value="Y">Left and Right body impairment</option>
							<option value="Z">No paresis</option>
							<option value="U">Unilateral</option>
							<option value="B">Bilateral</option>
							<option value="N">Cervical</option>
							<option value="T">Thoracic </option>
							<option value="L">Lumbar</option>
							<option value="S">Sacral </option>
							<option value="O">Cardiomyopathy</option>
							<option value="P">Pericarditis</option>
							<option value="F">1st degree burn</option>
							<option value="G">2nd degree burn</option>
							<option value="H">3rd degree burn</option>
							<option value="A">Aortic </option>
							<option value="E">Extremity</option>						
                    	</select>
                    </div>
	   
            <div class="space">&nbsp;</div>
            <div class="label">Program Type:</div>
            <div class="input">
            	<input type="text" name="programType" id="programType" /> 
            </div>
        </li>
    </ul>
</div>
<%String demoNo = request.getParameter("demographic_no");%>
<table class="buttonBar deep">
    <tr>
        <% if(!(bDnb || bMultipleSameDayGroupAppt)) { %>
        <TD nowrap>
        <%    if (!props.getProperty("allowMultipleSameDayGroupAppt", "").equalsIgnoreCase("no")) {%>
      <INPUT TYPE="submit" id="groupButton"
            onclick="document.forms['ADDAPPT'].displaymode.value='Group Appt'"
            VALUE="<bean:message key="appointment.addappointment.btnGroupAppt"/>"
            <%=disabled%>>
        <% }

  if(dateString2.equals( inform.format(inform.parse(now.get(Calendar.YEAR)+"-"+(now.get(Calendar.MONTH)+1)+"-"+now.get(Calendar.DAY_OF_MONTH))) )

    || dateString2.equals( inform.format(inform.parse(cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH))) ) ) {

        org.apache.struts.util.MessageResources resources = org.apache.struts.util.MessageResources.getMessageResources("oscarResources");

        %> <INPUT TYPE="submit" id="addPrintPreviewButton"
            onclick="document.forms['ADDAPPT'].displaymode.value='Add Appt & PrintPreview'"
            VALUE="<bean:message key='appointment.addappointment.btnAddApptPrintPreview'/>"
            <%=disabled%>>


<%
  }

%>
        <INPUT TYPE="submit" id="addButton" class="rightButton blueButton top"
            onclick="document.forms['ADDAPPT'].displaymode.value='Add Appointment'"
            tabindex="6"
            VALUE="<% if (isMobileOptimized) { %><bean:message key="appointment.addappointment.btnAddAppointmentMobile" />
                   <% } else { %><bean:message key="appointment.addappointment.btnAddAppointment"/><% } %>"
            <%=disabled%>>
        <INPUT TYPE="submit" id="printButton"
            onclick="document.forms['ADDAPPT'].displaymode.value='Add Appt & PrintCard'"
            VALUE="<bean:message key='global.btnPrint'/>"
            <%=disabled%>>
        <input TYPE="submit" id="printReceiptButton"
            onclick="document.forms['ADDAPPT'].displaymode.value='Add Appointment';document.forms['ADDAPPT'].printReceipt.value='1';"
            VALUE="<bean:message key='appointment.addappointment.btnPrintReceipt'/>"
            <%=disabled%>>
        <input type="hidden" name="printReceipt" value="">
                </TD>
		<TD></TD>
        <% } %>
    <TD align="right">
        <%
           if(bFirstDisp && apptObj!=null) {

               long numSameDayGroupApptsPaste = 0;

               if (props.getProperty("allowMultipleSameDayGroupAppt", "").equalsIgnoreCase("no")) {
                    String [] sqlParam = new String[3] ;
                    sqlParam[0] = myGroupNo; //schedule group
                    //convert empty string to placeholder demographic number "0" to prevent NumberFormatException when cutting/copying an empty appointmnet.
                    if(apptObj.getDemographic_no().trim().equals(""))
                    {
                        apptObj.setDemographic_no("0");//demographic numbers start at 1
                    }
                    sqlParam[1] = apptObj.getDemographic_no();
                    sqlParam[2] = dateString2;
		    appts = appointmentDao.search_group_day_appt(myGroupNo, Integer.parseInt(apptObj.getDemographic_no()), apptDate);
                    numSameDayGroupApptsPaste = appts.size() > 0 ? new Long(appts.size()) : 0;
                }
          %>
          <input type="button" id="pasteButton" value="Paste" onclick="pasteAppt(<%=(numSameDayGroupApptsPaste > 0)%>);">
        <% }%>
          <INPUT TYPE="RESET" id="backButton" class="leftButton top" VALUE="<bean:message key="appointment.addappointment.btnCancel"/>" onClick="cancelPageLock();window.close();">
       <% if (!props.getProperty("allowMultipleSameDayGroupAppt", "").equalsIgnoreCase("no")) {%>
          <input type="button" id="apptRepeatButton" value="<bean:message key="appointment.addappointment.btnRepeat"/>" onclick="onButRepeat()" <%=disabled%>>
      <%  } %>
   </TD>
	</tr>
</table>
</FORM>


<table align="center">
<tr>
    <td valign="top">
        <%if( bFromWL && demoNo != null && demoNo.length() > 0 ) {%>
        <table style="font-size: 9pt;" bgcolor="#c0c0c0" align="center" valign="top" cellpadding="3px">
            <tr bgcolor="#ccccff">
                <th colspan="2">
                    <bean:message key="appointment.addappointment.msgDemgraphics"/>
                    <a title="Master File" onclick="popup(700,1000,'../demographic/demographiccontrol.jsp?demographic_no=<%=demoNo%>&amp;displaymode=edit&amp;dboperation=search_detail','master')" href="javascript: function myFunction() {return false; }"><bean:message key="appointment.addappointment.btnEdit"/></a>

                    <bean:message key="appointment.addappointment.msgSex"/>: <%=sex%> &nbsp; <bean:message key="appointment.addappointment.msgDOB"/>: <%=dob%>
                </th>
            </tr>
             <tr bgcolor="#ccccff">
                <th style="padding-right: 20px" align="left"><bean:message key="appointment.addappointment.msgHin"/>:</th>
                <td><%=hin.replace("null", "")%> </td>
            </tr>
            <tr bgcolor="#ccccff">
                <th style="padding-right: 20px"align="left"><bean:message key="appointment.addappointment.msgAddress"/>:</th>
                <td><%=StringUtils.trimToEmpty(address)%>, <%=StringUtils.trimToEmpty(city)%>, <%=StringUtils.trimToEmpty(province)%>, <%=StringUtils.trimToEmpty(postal)%></td>
            </tr>
            <tr bgcolor="#ccccff">
                <th style="padding-right: 20px" align="left"><bean:message key="appointment.addappointment.msgPhone"/>:</th>
                <td><b><bean:message key="appointment.addappointment.msgH"/></b>:<%=StringUtils.trimToEmpty(phone)%> <b><bean:message key="appointment.addappointment.msgW"/></b>:<%=StringUtils.trimToEmpty(phone2)%> </td>
            </tr>
            <tr bgcolor="#ccccff" align="left">
                <th style="padding-right: 20px"><bean:message key="appointment.addappointment.msgEmail"/>:</th>
                <td><%=StringUtils.trimToEmpty(email)%></td>
            </tr>

        </table>
        <%}%>
    </td>
    <td valign="top">
    <%
        String formTblProp = props.getProperty("appt_formTbl","");
        String[] formTblNames = formTblProp.split(";");

        int numForms = 0;
        for (String formTblName : formTblNames){
            if ((formTblName != null) && !formTblName.equals("")) {
                //form table name defined
                for(EncounterForm ef:encounterFormDao.findByFormTable(formTblName)) {
                    String formName = ef.getFormName();
                    pageContext.setAttribute("formName", formName);
                    boolean formComplete = false;
                    EctFormData.PatientForm[] ptForms = EctFormData.getPatientFormsFromLocalAndRemote(loggedInInfo, demoNo, formTblName);

                    if (ptForms.length > 0) {
                        formComplete = true;
                    }
                    numForms++;
                    if (numForms == 1) {
               
    %>
            <table style="font-size: 9pt;" bgcolor="#c0c0c0" align="center" valign="top" cellpadding="3px">
                <tr bgcolor="#ccccff">
                    <th colspan="2">
                        <bean:message key="appointment.addappointment.msgFormsSaved"/>
                    </th>
                </tr>
    <%              }%>

                <tr bgcolor="#c0c0c0" align="left">
                    <th style="padding-right: 20px"><c:out value="${formName}:"/></th>
    <%              if (formComplete){  %>
                        <td><bean:message key="appointment.addappointment.msgFormCompleted"/></td>
    <%              } else {            %>
                        <td><bean:message key="appointment.addappointment.msgFormNotCompleted"/></td>
    <%              } %>
                </tr>
    <%
                }
            }
        }

        if (numForms > 0) {
    %>
         </table>
    <%  }   %>
    </td>
    <td valign="top">
<table style="font-size: 8pt;" bgcolor="#c0c0c0" align="center" valign="top">
	<tr bgcolor="#ccccff">
		<th colspan="4"><bean:message key="appointment.addappointment.msgOverview" /></th>
	</tr>
	<tr bgcolor="#ccccff">
		<th style="padding-right: 25px"><bean:message key="Appointment.formDate" /></th>
 		<th style="padding-right: 25px"><bean:message key="Appointment.formStartTime" /></th>
		<th style="padding-right: 25px"><bean:message key="appointment.addappointment.msgProvider" /></th>
		<th><bean:message key="appointment.addappointment.msgComments" /></th>
	</tr>
	<%

        int iRow=0;
        if( bFromWL && demoNo != null && demoNo.length() > 0 ) {

            Object [] param2 = new Object[3];
            param2[0] = demoNo;
            Calendar cal2 = Calendar.getInstance();
            param2[1] = new java.sql.Date(cal2.getTime().getTime());
            java.util.Date start = cal2.getTime();
            cal2.add(Calendar.YEAR, 1);
            java.util.Date end = cal2.getTime();
            param2[2] = new java.sql.Date(cal2.getTime().getTime());
            
            for(Object[] result : appointmentDao.search_appt_future(Integer.parseInt(demoNo), start, end)) {
            	Appointment a = (Appointment)result[0];
            	p = (Provider)result[1];
           
                iRow ++;
                if (iRow > iPageSize) break;
    %>
	<tr bgcolor="#eeeeff">
		<td style="background-color: #CCFFCC; padding-right: 25px"><%=ConversionUtils.toDateString(a.getAppointmentDate())%></td>
		<td style="background-color: #CCFFCC; padding-right: 25px"><%=ConversionUtils.toTimeString(a.getStartTime())%></td>
		<td style="background-color: #CCFFCC; padding-right: 25px"><%=p.getFormattedName()%></td>
		<td style="background-color: #CCFFCC;"><%=a.getStatus()==null?"":(a.getStatus().equals("N")?"No Show":(a.getStatus().equals("C")?"Cancelled":"") )%></td>
	</tr>
	<%
            }

            iRow=0;
            cal2 = Calendar.getInstance();
            cal2.add(Calendar.YEAR, -1);
            
            for(Object[] result : appointmentDao.search_appt_past(Integer.parseInt(demoNo), start, cal2.getTime())) {
            	Appointment a = (Appointment)result[0];
            	p = (Provider)result[1];
                iRow ++;
                if (iRow > iPageSize) break;
    %>
	<tr bgcolor="#eeeeff">
		<td style="background-color: #CCFFCC; padding-right: 25px"><%=ConversionUtils.toDateString(a.getAppointmentDate())%></td>
		<td style="background-color: #CCFFCC; padding-right: 25px"><%=ConversionUtils.toTimeString(a.getStartTime())%></td>
		<td style="background-color: #CCFFCC; padding-right: 25px"><%=p.getFormattedName()%></td>
		<td style="background-color: #CCFFCC;"><%=a.getStatus()==null?"":(a.getStatus().equals("N")?"No Show":(a.getStatus().equals("C")?"Cancelled":"") )%></td>
	</tr>
	<%
            }
        }
    %>
</table>
</td>
</tr>
</table>

</body>
<script type="text/javascript">
var loc = document.forms['ADDAPPT'].location;
if(loc.nodeName.toUpperCase() == 'SELECT') loc.style.backgroundColor=loc.options[loc.selectedIndex].style.backgroundColor;
</script>
</html:html>
