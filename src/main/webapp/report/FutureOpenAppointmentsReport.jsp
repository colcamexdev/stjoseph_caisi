<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.sql.ResultSet"%>
<%@page import="org.oscarehr.common.dao.MyGroupDao"%>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="oscar.oscarDemographic.data.*,java.util.*,oscar.oscarPrevention.*,oscar.oscarProvider.data.*,oscar.util.*,oscar.oscarReport.data.*,oscar.oscarPrevention.pageUtil.*,java.net.*,oscar.eform.*"%>
<%@page import="oscar.OscarProperties, org.oscarehr.util.SpringUtils, org.oscarehr.common.dao.BillingONCHeader1Dao" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="providerBean" class="java.util.Properties" scope="session" />
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	  boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_report" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_report");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>

<html locale="true">

<head>
<title>OSCAR Report</title><!-- i18n -->

<script type="text/javascript" src="../share/javascript/Oscar.js"></script>
<link rel="stylesheet" type="text/css" href="../share/css/OscarStandardLayout.css">
<link rel="stylesheet" type="text/css" media="all" href="../share/calendar/calendar.css" title="win2k-cold-1" >

<script type="text/javascript" src="../share/calendar/calendar.js" ></script>
<script type="text/javascript" src="../share/calendar/lang/<bean:message key="global.javascript.calendar"/>" ></script>
<script type="text/javascript" src="../share/calendar/calendar-setup.js" ></script>
<script type="text/javascript" src="../share/javascript/prototype.js"></script>
<script type="text/javascript" src="../share/javascript/sortable.js"></script>

<script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
<style type="text/css">
  div.ImmSet { background-color: #ffffff; }
  div.ImmSet h2 {  }
  div.ImmSet ul {  }
  div.ImmSet li {  }
  div.ImmSet li a { text-decoration:none; color:blue;}
  div.ImmSet li a:hover { text-decoration:none; color:red; }
  div.ImmSet li a:visited { text-decoration:none; color:blue;}
</style>


<style type="text/css">
	table.outline{
	   margin-top:50px;
	   border-bottom: 1pt solid #888888;
	   border-left: 1pt solid #888888;
	   border-top: 1pt solid #888888;
	   border-right: 1pt solid #888888;
	}
	table.grid{
	   border-bottom: 1pt solid #888888;
	   border-left: 1pt solid #888888;
	   border-top: 1pt solid #888888;
	   border-right: 1pt solid #888888;
	}
	td.gridTitles{
		border-bottom: 2pt solid #888888;
		font-weight: bold;
		text-align: center;
	}
        td.gridTitlesWOBottom{
                font-weight: bold;
                text-align: center;
        }
	td.middleGrid{
	   border-left: 1pt solid #888888;
	   border-right: 1pt solid #888888;
           text-align: center;
	}


label{
float: left;
width: 120px;
font-weight: bold;
}

span.labelLook{
font-weight:bold;

}

input, textarea,select{

margin-bottom: 5px;
}

textarea{
width: 250px;
height: 150px;
}

.boxes{
width: 1em;
}

#submitbutton{
margin-left: 120px;
margin-top: 5px;
width: 90px;
}

br{
clear: left;
}

table.ele {

   border-collapse:collapse;
}

table.ele td{
    border:1px solid grey;
    padding:2px;
}

/* Sortable tables */
table.ele thead {
    background-color:#eee;
    color:#666666;
    font-size: x-small;
    cursor: default;
}
</style>

<style type="text/css" media="print">
.MainTable {
    display:none;
}
.hiddenInPrint{
    display:none;
}
.shownInPrint{
    display:block;
}
</style>

<script>

function openAppointment(apptNo,demographicNo) {
	window.open('../appointment/appointmentcontrol.jsp?appointment_no='+apptNo+'&displaymode=edit&dboperation=search&demographic_no=' + demographicNo);
}

function highlightOn(apptNo) {
	$('#row_' + apptNo).css('background-color','salmon');
	$('#row_' + apptNo).css('cursor','pointer');
}

function highlightOff(apptNo) {
	$('#row_' + apptNo).css('background-color','white');
	$('#row_' + apptNo).css('cursor','none');
}

</script>
</head>

<%
	String groupSelected = request.getParameter("group");
	String daysSelected = request.getParameter("days");
	String action = request.getParameter("action");
	
	if(groupSelected == null) {
		groupSelected = "";
	}
	if(daysSelected == null) {
		daysSelected="60";
	}
%>
<body class="BodyStyle" vlink="#0000FF">
<!--  -->
    <table  class="MainTable" id="scrollNumber1" >
        <tr class="MainTableTopRow">
            <td class="MainTableTopRowLeftColumn" width="100" >
               OSCAR Report
            </td>
            <td class="MainTableTopRowRightColumn">
                <table class="TopStatusBar">
                    <tr>
                        <td >
                            Future Open Appointments
                        </td>
                        <td  >
                        
                        </td>
                        <td style="text-align:right">
                                <oscar:help keywords="report" key="app.top1"/> | <a href="javascript:popupStart(300,400,'About.jsp')" ><bean:message key="global.about" /></a> | <a href="javascript:popupStart(300,400,'License.jsp')" ><bean:message key="global.license" /></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="MainTableLeftColumn" valign="top">
               &nbsp;
            </td>
            <td valign="top" class="MainTableRightColumn">
               <form action="FutureOpenAppointmentsReport.jsp" method="POST">
               <input type="hidden" name="action" value="submit"/>
               <div>
                  Schedule Group:
                  <select name="group">
                      <option value="" <%=("".equals(groupSelected))?" selected=\"selected\" ":"" %> >--Select Group--</option>
                      <%
                      	MyGroupDao groupDao = SpringUtils.getBean(MyGroupDao.class);
                      	List<String> groupNames = groupDao.getGroups();
                      	for(String group:groupNames) {
                      %>
                        <option value="<%=group%>" <%=(group.equals(groupSelected))?" selected=\"selected\" ":"" %>><%=group%></option>
                      <%}%>
                  </select>
               </div>
               <div>
                  Days into Future to look:
                  <select name="days">
                      <option value="7" <%=("7".equals(daysSelected))?" selected=\"selected\" ":"" %> >1 week</option>
                      <option value="14" <%=("14".equals(daysSelected))?" selected=\"selected\" ":"" %>>2 weeks</option>
                      <option value="30" <%=("30".equals(daysSelected))?" selected=\"selected\" ":"" %>>30 days</option>
                      <option value="60" <%=("60".equals(daysSelected))?" selected=\"selected\" ":"" %>>60 days</option>
                      <option value="90" <%=("90".equals(daysSelected))?" selected=\"selected\" ":"" %>>90 days</option>
                      <option value="180" <%=("180".equals(daysSelected))?" selected=\"selected\" ":"" %>>6 months</option>
                  </select>
               </div>
               <input type="submit" />
               </form>

				<br/>
				
				<%
				if(action != null && "submit".equals(action)) {
				%>
					<table style="width:40%">
						<thead>
							<tr>
								<th style="text-align:left">Appointment Date</th>
								<th style="text-align:left">Appointment Time</th>
								<th style="text-align:left">Provider Name</th>
								<th style="text-align:left">Reason</th>
								
							</tr>	
						</thead>
						<tbody>
						<%
						String sql = "select a.appointment_no,CONCAT(p.last_name,\",\",p.first_name),a.appointment_date,a.start_time,a.demographic_no,a.reason from appointment a, provider p where a.provider_no = p.provider_no and a.status = 'd' and a.appointment_date > (CURDATE() + INTERVAL 1 DAY) and a.appointment_date <= (CURDATE() + INTERVAL " + daysSelected + " DAY) and a.demographic_no = 0 and a.provider_no in (select x.provider_no from mygroup x where x.mygroup_no = \""+ groupSelected+ "\") order by a.appointment_date,a.start_time";
						 ResultSet rsdemo = oscar.oscarDB.DBHandler.GetSQL(sql);
						while(rsdemo.next()) {
							int appointmentNo = rsdemo.getInt("appointment_no");
							String name = rsdemo.getString(2);
							String dt = rsdemo.getString(3);
							String time = rsdemo.getString(4);
							String demographicNo = rsdemo.getString(5);
							String reason = rsdemo.getString(6);
							%>
							
								<tr id="row_<%=appointmentNo%>" onClick="openAppointment(<%=appointmentNo%>,<%=demographicNo %>)" onMouseOver="highlightOn(<%=appointmentNo%>)" onMouseOut="highlightOff(<%=appointmentNo%>)">
									<td><%=dt %></td>
									<td><%=time %></td>
									<td><%=name %></td>
									<td><%=reason %></td>
								</tr>
							<%
						}
						rsdemo.close();
						%>
						</tbody>
					</table>
				<%	
				}
				%>
            </td>
        </tr>
        <tr>
            <td class="MainTableBottomRowLeftColumn">
            &nbsp;
            </td>
            <td class="MainTableBottomRowRightColumn" valign="top">
            &nbsp;
            </td>
        </tr>
    </table>

</body>
</html>
