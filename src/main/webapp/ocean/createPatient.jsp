	<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@page import="org.oscarehr.common.dao.DemographicExtDao"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.oscarehr.common.model.Demographic"%>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@page import="org.oscarehr.util.SpringUtils"%>
<%@page import="org.oscarehr.managers.DemographicManager"%>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.oscarehr.integration.ocean.OceanUtils"%>
<%
Logger logger = MiscUtils.getLogger();
DemographicExtDao dExtDao = SpringUtils.getBean(DemographicExtDao.class);
SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

JSONObject root = new JSONObject();

try {
	String refId = request.getParameter("refId");

	if(!StringUtils.isEmpty(refId)) {
		String ptData = OceanUtils.getPatientJSON(refId);
		if(ptData != null) {
	
			JSONObject data = JSONObject.fromObject(ptData);
			JSONObject demographics = data.getJSONObject("demographics");
			if(demographics != null) {
				String firstName = demographics.getString("firstName");
				String surname = demographics.getString("surname");
				
				String hin = demographics.getString("hn");
				String hinProv = demographics.getString("hnProv");
				String hinVersion = demographics.getString("hnVC");
				
				String birthDate = demographics.getString("birthDate"); //1960-01-01
				String sex = demographics.getString("sex"); //M
				
				//create the patient, admit to program, store refId somewhere , return demographicNo
				DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
				
				Demographic d = new Demographic();
				d.setLastName(surname);
				d.setFirstName(firstName);
				d.setHin(hin);
				d.setVer(hinVersion);
				d.setSex(sex);
				
				d.setAddress("");
				d.setCity("");
				d.setProvince("ON");
				d.setPostal("");
				d.setChartNo("");
				
				d.setPhone("");
				d.setPhone2("");
				
				
				try {
					Date date = formatter.parse(birthDate);
					Calendar cal = Calendar.getInstance();
					cal.setTime(date);
					d.setYearOfBirth(String.valueOf(cal.get(Calendar.YEAR)));
					d.setMonthOfBirth(("0"+String.valueOf(cal.get(Calendar.MONTH)+1)).substring(0,2));
					d.setDateOfBirth(("0"+String.valueOf(cal.get(Calendar.DAY_OF_MONTH))).substring(0,2));
				}catch(ParseException e) {
					logger.error("Error",e);					
				}
				d.setTitle("");
				d.setRosterStatus("");
				d.setPatientStatusDate(new Date());
				d.setProviderNo("");
				d.setDateJoined(new Date());
				
				demographicManager.createDemographic(LoggedInInfo.getLoggedInInfoFromSession(request), d, 10039);
				
				dExtDao.addKey("-1", d.getDemographicNo(), "echart_pending", "ocean|" + refId);
				
				logger.info("Created " + d.getDemographicNo() + " " + d.getFirstName() + " " + d.getLastName());
				
				root.put("demographicNo", d.getDemographicNo());
			}
		}
	}
}catch(Exception e) {
	MiscUtils.getLogger().error("Error",e);
	root.put("error","An error occured on the server");
}

root.write(response.getWriter());
%>
