	<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@page import="java.nio.charset.Charset"%>
<%@page import="net.sf.json.JSONArray"%>
<%@page import="java.security.spec.RSAPrivateCrtKeySpec"%>
<%@page import="java.security.spec.RSAPrivateKeySpec"%>
<%@page import="java.math.BigInteger"%>
<%@page import="java.security.spec.RSAPublicKeySpec"%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="net.sf.json.JSONString"%>
<%@page import="javax.crypto.spec.IvParameterSpec"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>
<%@page import="javax.crypto.Cipher"%>
<%@page import="java.security.spec.InvalidKeySpecException"%>
<%@page import="java.security.NoSuchAlgorithmException"%>
<%@page import="java.security.KeyFactory"%>
<%@page import="java.security.spec.PKCS8EncodedKeySpec"%>
<%@page import="java.security.PrivateKey"%>
<%@page import="java.util.Base64"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="org.apache.http.client.utils.URIBuilder"%>
<%@page import="org.apache.http.params.HttpParams"%>
<%@page import="org.apache.http.util.EntityUtils"%>
<%@page import="org.apache.http.client.methods.HttpGet"%>
<%@page import="org.apache.http.HttpResponse"%>
<%@page import="org.apache.http.impl.client.HttpClients"%>
<%@page import="org.apache.http.impl.client.CloseableHttpClient"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<%
	//URIBuilder builder = new URIBuilder("https://ocean.cognisantmd.com/svc/v1/referrals/" + "f83a90e4-2344-422e-9c42-1cbe40bba457");

	URIBuilder builder = new URIBuilder("https://ocean.cognisantmd.com/svc/v1/referrals");
	builder.setParameter("startDate", "2018-01-01").setParameter("endDate", "2019-10-29").setParameter("projection","none");
	
	HttpGet theGet = new HttpGet(builder.build());
	theGet.addHeader("siteCredential", "88340e79-73c1-437d-a95a-609616840b29");
	theGet.addHeader("siteNum", "5145");
	theGet.addHeader("siteKey", "OCEAN_API_SITEKEY");
	
	CloseableHttpClient client = HttpClients.custom().build();
	HttpResponse httpResponse = client.execute(theGet);
	String entity = EntityUtils.toString(httpResponse.getEntity());

	JSONArray res = JSONArray.fromObject(entity);
	
	for(int x=0;x<res.size();x++) {
	
		getReferral(res.getString(x));
	}
	
	//{referralInitialInfoId}
	
	//23447
	/*
	String m = getPatientJSON("43799");
	JSONObject ref = JSONObject.fromObject(m);
	JSONObject demographics = ref.getJSONObject("demographics");
	
	String firstName = demographics.getString("firstName");
	String surname = demographics.getString("surname");
	String title = demographics.getString("title"); //Mr.
	
	String hin = demographics.getString("hn");
	String hinProv = demographics.getString("hnProv");
	String hinVersion = demographics.getString("hnVC");
	
	String birthDate = demographics.getString("birthDate"); //1960-01-01
	String sex = demographics.getString("sex"); //M
	
	JSONObject address = demographics.getJSONObject("address");
	
	String addressLine = address.getString("streetAddress");
	String city = address.getString("city");
	String province = address.getString("province"); //ON
	String postalCode = address.getString("postalCode");
	String homePhone = address.getString("homePhone");
	String workPhone = address.getString("businessPhone");
	String workPhoneExt = address.getString("businessPhoneExt");
	String cellPhone = address.getString("mobilePhone");
	String email = address.getString("email");
	String country = address.getString("country");
	
	//store identifiers in order to be able to get updates?
	
	
	
	

	*/
	
	
	///////////////////////////////////////////////////
	
	
	/*
	URIBuilder builder = new URIBuilder("https://ocean.cognisantmd.com/svc/v1/svc/v1/patients/43799");
	
	HttpGet theGet = new HttpGet(builder.build());
	theGet.addHeader("siteCredential", "88340e79-73c1-437d-a95a-609616840b29");
	theGet.addHeader("siteNum", "5145");
	theGet.addHeader("siteKey", "OCEAN_API_SITEKEY");
	
	CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
	HttpResponse httpResponse = client.execute(theGet);
	

	
	String iv = httpResponse.getFirstHeader("iv").getValue();
	String oneTimeKey = httpResponse.getFirstHeader("oneTimeKey").getValue();
	byte[] encryptedData = EntityUtils.toByteArray(httpResponse.getEntity());
	
	String data = new String(encryptedData,"ISO-8859-1");

	JSONArray arr = JSONArray.fromObject(data);
	
	byte[] b = new byte[arr.size()];
	for(int x=0;x<b.length;x++) {
		b[x] = (byte)arr.getInt(x);
	}
	
	byte[] oneTimeKeyBytes = Base64.getDecoder().decode(oneTimeKey);
	
	Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    SecretKeySpec skeySpec = new SecretKeySpec("StJoesGuelph20!9".getBytes(), "AES");
    IvParameterSpec iv2 = new IvParameterSpec(Base64.getDecoder().decode(iv));
    cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv2);
    byte[] decryptedOneTimeKey = cipher.doFinal(oneTimeKeyBytes);
	
    
    //byte[] oneTimeKeyBytes2 = Base64.getDecoder().decode(decryptedOneTimeKey);
	
	//byte[] ptdata = Base64.getDecoder().decode(encryptedData);
	
	
	Cipher cipher2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
	//IvParameterSpec iv2 = new IvParameterSpec(Base64.getDecoder().decode(iv));
	SecretKeySpec skeySpec2 = new SecretKeySpec(decryptedOneTimeKey, "AES");    
	cipher2.init(Cipher.DECRYPT_MODE, skeySpec2, iv2);
    byte[] decrypted = cipher2.doFinal(b);
    
    String ptData = new String(decrypted);
    

	
	*/
	
	
	
	//String data2 = decrypt(b, oneTimeKey, iv);
	

	//JSONObject res = JSONObject.fromObject(entity);
	

	
	/*
	JSONObject ref = JSONObject.fromObject(entity);
	JSONObject enc = ref.getJSONObject("encryptedPtData");
	String encryptedData = enc.getString("data");	
	String iv =  enc.getString("iv");	
	
	String sharedEncryptionKey = "StJoesGuelph20!9";
	String oneTimeKeyEncryptedWithTargetPublicKey = ref.getString("oneTimeKeyEncryptedWithTargetPublicKey");
	
	
	byte[] privateKey = grabPrivateKey();
	String s = new String(privateKey, StandardCharsets.US_ASCII);
		
	String[] pkParts = s.split("\\|");
	
	String decryptedOneTimeKey = null;
	
	for(String part:pkParts) {

		JSONObject k = JSONObject.fromObject(part);
		String modulus = k.getString("n");
		String publicExp = k.getString("e");
		String privateExp = k.getString("d");
		String prime1 = k.getString("p");
		String prime2 = k.getString("q");
		String exponent1 = k.getString("dmp1");
		String exponent2 = k.getString("dmq1");
		String coefficient = k.getString("coeff");
		 
		 RSAPrivateCrtKeySpec keySpec = new RSAPrivateCrtKeySpec(
				 new BigInteger(modulus,16),
				 new BigInteger(publicExp,16),
				 new BigInteger(privateExp,16),
				 new BigInteger(prime1,16),
				 new BigInteger(prime2,16),
				 new BigInteger(exponent1,16),
				 new BigInteger(exponent2,16),
				 new BigInteger(coefficient,16)
				 
		);
		 
		
		PrivateKey privateKey2 = null;
		 KeyFactory keyFactory = null;
		    try {
		        keyFactory = KeyFactory.getInstance("RSA");
		    } catch (NoSuchAlgorithmException e) {
		    
		    }
		    try {
		        privateKey2 = keyFactory.generatePrivate(keySpec);
		    } catch (InvalidKeySpecException e) {
		     
		    }
	
		    try {
		    byte[] oneTime = Base64.getDecoder().decode(oneTimeKeyEncryptedWithTargetPublicKey.getBytes("ISO-8859-1"));
		    
		    Cipher decrypt=Cipher.getInstance("RSA");
		    decrypt.init(Cipher.DECRYPT_MODE, privateKey2);
		    String decryptedMessage = new String(decrypt.doFinal(oneTime), StandardCharsets.UTF_8);

		    if(decryptedMessage != null) {
		    	decryptedOneTimeKey = decryptedMessage;
		    	break;
		    }
		    }
		    
		    }
		  
	}
	
	if(decryptedOneTimeKey != null) {
		byte[] oneTimeKeyBytes = Base64.getDecoder().decode(decryptedOneTimeKey);
		
		byte[] ptdata = Base64.getDecoder().decode(encryptedData);
		
		
		Cipher cipher2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
		IvParameterSpec iv2 = new IvParameterSpec(Base64.getDecoder().decode(iv));
		SecretKeySpec skeySpec = new SecretKeySpec(oneTimeKeyBytes, "AES");    
		cipher2.init(Cipher.DECRYPT_MODE, skeySpec, iv2);
	    byte[] decrypted = cipher2.doFinal(ptdata);
	    
	    String ptData = new String(decrypted);

	}
	
*/
%>
<%!
	byte[] grabPrivateKey() throws Exception {
		URIBuilder builder = new URIBuilder("https://ocean.cognisantmd.com/svc/v1/site/privateKey");
		HttpGet theGet = new HttpGet(builder.build());
		theGet.addHeader("siteCredential", "88340e79-73c1-437d-a95a-609616840b29");
		theGet.addHeader("siteNum", "5145");
		theGet.addHeader("siteKey", "OCEAN_API_SITEKEY");
		
		CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
		HttpResponse httpResponse = client.execute(theGet);
		
		String entity = EntityUtils.toString(httpResponse.getEntity(), "ISO-8859-1");
		JSONObject key = JSONObject.fromObject("{\"test\": " + entity + "}");
		
		String keyData = key.getString("test");
		
		byte[] keyData2 = Base64.getDecoder().decode(keyData);
		
		String iv = httpResponse.getFirstHeader("iv").getValue();
		
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	    SecretKeySpec skeySpec = new SecretKeySpec("StJoesGuelph20!9".getBytes(), "AES");
	    IvParameterSpec iv2 = new IvParameterSpec(Base64.getDecoder().decode(iv));
	    cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv2);
	    byte[] decrypted = cipher.doFinal(keyData2);
	    
	   
		return decrypted;
		
	}

	void getReferral(String refId) throws Exception {
		URIBuilder builder = new URIBuilder("https://ocean.cognisantmd.com/svc/v1/referrals/" + refId);

		HttpGet theGet = new HttpGet(builder.build());
		theGet.addHeader("siteCredential", "88340e79-73c1-437d-a95a-609616840b29");
		theGet.addHeader("siteNum", "5145");
		theGet.addHeader("siteKey", "OCEAN_API_SITEKEY");
		
		CloseableHttpClient client = HttpClients.custom().build();
		HttpResponse httpResponse = client.execute(theGet);
		String entity = EntityUtils.toString(httpResponse.getEntity());
		
		JSONObject ref = JSONObject.fromObject(entity);

	}
	
	
	String getPatientJSON(String patientRef) throws Exception {
		URIBuilder builder = new URIBuilder("https://ocean.cognisantmd.com/svc/v1/svc/v1/patients/" + patientRef);
		
		HttpGet theGet = new HttpGet(builder.build());
		theGet.addHeader("siteCredential", "88340e79-73c1-437d-a95a-609616840b29");
		theGet.addHeader("siteNum", "5145");
		theGet.addHeader("siteKey", "OCEAN_API_SITEKEY");
		
		CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
		HttpResponse httpResponse = client.execute(theGet);
				
		String iv = httpResponse.getFirstHeader("iv").getValue();
		String oneTimeKey = httpResponse.getFirstHeader("oneTimeKey").getValue();
		byte[] encryptedData = EntityUtils.toByteArray(httpResponse.getEntity());
		
		String data = new String(encryptedData,"ISO-8859-1");

		JSONArray arr = JSONArray.fromObject(data);
		
		byte[] b = new byte[arr.size()];
		for(int x=0;x<b.length;x++) {
			b[x] = (byte)arr.getInt(x);
		}
		
		byte[] oneTimeKeyBytes = Base64.getDecoder().decode(oneTimeKey);
		
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	    SecretKeySpec skeySpec = new SecretKeySpec("StJoesGuelph20!9".getBytes(), "AES");
	    IvParameterSpec iv2 = new IvParameterSpec(Base64.getDecoder().decode(iv));
	    cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv2);
	    byte[] decryptedOneTimeKey = cipher.doFinal(oneTimeKeyBytes);
		
		
		Cipher cipher2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
		SecretKeySpec skeySpec2 = new SecretKeySpec(decryptedOneTimeKey, "AES");    
		cipher2.init(Cipher.DECRYPT_MODE, skeySpec2, iv2);
	    byte[] decrypted = cipher2.doFinal(b);
	    
	    String ptData = new String(decrypted);
	    
		return ptData;
			
	}
%>
<script>
	
</script>
</body>
</html>