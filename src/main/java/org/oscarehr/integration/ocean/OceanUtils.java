/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.integration.ocean;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.oscarehr.util.MiscUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import oscar.OscarProperties;

public class OceanUtils {

	private static void addAuthHeaders(HttpRequestBase request) {
		String siteCredential = OscarProperties.getInstance().getProperty("ocean.siteCredential");
		String siteNum = OscarProperties.getInstance().getProperty("ocean.siteNum");
		String siteKey = OscarProperties.getInstance().getProperty("ocean.siteKey");

		request.addHeader("siteCredential", siteCredential);
		request.addHeader("siteNum", siteNum);
		request.addHeader("siteKey", siteKey);
	}

	public static boolean cancelAppointment(String referralRef) throws Exception {
		String baseUrl = OscarProperties.getInstance().getProperty("ocean.url", "https://ocean.cognisantmd.com");

		URIBuilder builder = new URIBuilder(baseUrl + "/svc/v1/referrals/" + referralRef + "/appointments/0");

		HttpDelete theDelete = new HttpDelete(builder.build());
		addAuthHeaders(theDelete);

		CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
		HttpResponse httpResponse = client.execute(theDelete);

		if (httpResponse.getStatusLine().getStatusCode() == 200) {
			return true;
		} else {
			MiscUtils.getLogger().warn("HTTP response trying to delete appt on OCEAN is " + httpResponse.getStatusLine().getStatusCode());
			return false;
		}
	}

	public static boolean addAppointmentToReferral(String referralRef, Date apptDate) throws Exception {

		String baseUrl = OscarProperties.getInstance().getProperty("ocean.url", "https://ocean.cognisantmd.com");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		String text = sdf.format(apptDate);

		URIBuilder builder = new URIBuilder(baseUrl + "/svc/v1/referrals/" + referralRef + "/appointments");
		builder.setParameter("apptTime", text);

		HttpPost thePost = new HttpPost(builder.build());
		addAuthHeaders(thePost);

		CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
		HttpResponse httpResponse = client.execute(thePost);

		if (httpResponse.getStatusLine().getStatusCode() == 204) {
			return true;
		} else if (httpResponse.getStatusLine().getStatusCode() == 400) {
			URIBuilder builder2 = new URIBuilder(baseUrl + "/svc/v1/referrals/" + referralRef + "/appointments/0");
			builder2.setParameter("apptTime", text);
			HttpPost thePost2 = new HttpPost(builder2.build());
			addAuthHeaders(thePost2);

			CloseableHttpClient client2 = HttpClients.custom().disableContentCompression().build();
			HttpResponse httpResponse2 = client2.execute(thePost2);

			if (httpResponse2.getStatusLine().getStatusCode() == 204) {
				return true;
			}
			MiscUtils.getLogger().warn("Problem updating appointment with OCEAN : response code = " + httpResponse2.getStatusLine().getStatusCode());
			return false;
		} else {
			MiscUtils.getLogger().warn("Problem updating appointment with OCEAN : response code = " + httpResponse.getStatusLine().getStatusCode());
			return false;
		}
	}

	// /svc/v1/referrals
	public static List<JSONObject> getReferrals(String demographicNo) throws Exception {
		List<JSONObject> result = new ArrayList<JSONObject>();

		String baseUrl = OscarProperties.getInstance().getProperty("ocean.url", "https://ocean.cognisantmd.com");
		//String encryptionKey =  OscarProperties.getInstance().getProperty("ocean.encryptionKey");

		URIBuilder builder = new URIBuilder(baseUrl + "/svc/v1/referrals");
		//TODO: fix these hard dates
		builder.setParameter("startDate", "2019-01-01").setParameter("endDate", "2019-12-31");

		HttpGet theGet = new HttpGet(builder.build());
		addAuthHeaders(theGet);

		CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
		HttpResponse httpResponse = client.execute(theGet);

		if (httpResponse.getStatusLine().getStatusCode() == 200) {
			String entity = EntityUtils.toString(httpResponse.getEntity());
			JSONArray res = JSONArray.fromObject(entity);

			for (int x = 0; x < res.size(); x++) {
				JSONObject ref = res.getJSONObject(x);
				String er = ref.getString("externalRef");

				//only interested in the referrals that belong to this patient
				if (er != null && demographicNo.equals(er)) {
					//result.add(ref);

					//decrypt patient deata
					String oneTimeKeyEncryptedWithTargetPublicKey = ref.getString("oneTimeKeyEncryptedWithTargetPublicKey");
					JSONObject ptData = decryptPatientData(ref.getJSONObject("encryptedPtData"), oneTimeKeyEncryptedWithTargetPublicKey);

					JSONObject refData = new JSONObject();
					refData.put("referral",ref);
					
					//JSONObject info = new JSONObject();
					//info.put("referralSource","");
					
					if(ptData != null) {
			
						refData.put("data",ptData);
					}
					
					result.add(refData);
				}
			}
		}

		return result;
	}

	public static Integer getNumberOfActiveReferrals(String oceanPatientRef, String demographicNo) throws Exception {
		Integer numReferrals = null;

		String baseUrl = OscarProperties.getInstance().getProperty("ocean.url", "https://ocean.cognisantmd.com");
		String siteCredential = OscarProperties.getInstance().getProperty("ocean.siteCredential");
		String siteNum = OscarProperties.getInstance().getProperty("ocean.siteNum");
		String siteKey = OscarProperties.getInstance().getProperty("ocean.siteKey");

		URIBuilder builder = new URIBuilder(baseUrl + "/api/getPatientStatus?siteNum=" + siteNum + "&v=1&client=oscar&patientRef=" + oceanPatientRef + "&extPatientRef=" + demographicNo + "&decoded=false&checkForReferrals=true");
		HttpPost theGet = new HttpPost(builder.build());
		theGet.addHeader("sitePassword", siteCredential);
		theGet.addHeader("siteKey", siteKey);
		CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
		HttpResponse httpResponse = client.execute(theGet);

		if (httpResponse.getStatusLine().getStatusCode() >= 200 && httpResponse.getStatusLine().getStatusCode() < 300) {
			//String data = EntityUtils.toString(httpResponse.getEntity());
			//return data;
			for (Header h : httpResponse.getHeaders("ACTIVE_REFERRALS")) {
				try {
					numReferrals = Integer.parseInt(h.getValue());
				} catch (NumberFormatException e) {

				}
			}
		}

		return numReferrals;
	}

	public static String getPatientRefFromDemographicNo(String demographicNo) throws Exception {
		String baseUrl = OscarProperties.getInstance().getProperty("ocean.url", "https://ocean.cognisantmd.com");
		String siteCredential = OscarProperties.getInstance().getProperty("ocean.siteCredential");
		String siteNum = OscarProperties.getInstance().getProperty("ocean.siteNum");
		String siteKey = OscarProperties.getInstance().getProperty("ocean.siteKey");

		URIBuilder builder = new URIBuilder(baseUrl + "/api/getPatientRef?siteNum=" + siteNum + "&v=1&client=oscar&extPatientRef=" + demographicNo);
		HttpGet theGet = new HttpGet(builder.build());
		theGet.addHeader("sitePassword", siteCredential);
		theGet.addHeader("siteKey", siteKey);
		CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
		HttpResponse httpResponse = client.execute(theGet);

		if (httpResponse.getStatusLine().getStatusCode() == 200) {
			String data = EntityUtils.toString(httpResponse.getEntity());
			return data;
		}

		return null;
	}

	public static String getPatientJSON(String patientRef) throws Exception {

		String baseUrl = OscarProperties.getInstance().getProperty("ocean.url", "https://ocean.cognisantmd.com");
		String siteCredential = OscarProperties.getInstance().getProperty("ocean.siteCredential");
		String siteNum = OscarProperties.getInstance().getProperty("ocean.siteNum");
		String siteKey = OscarProperties.getInstance().getProperty("ocean.siteKey");
		String encryptionKey = OscarProperties.getInstance().getProperty("ocean.encryptionKey");

		URIBuilder builder = new URIBuilder(baseUrl + "/svc/v1/patients/" + patientRef);

		HttpGet theGet = new HttpGet(builder.build());
		theGet.addHeader("siteCredential", siteCredential);
		theGet.addHeader("siteNum", siteNum);
		theGet.addHeader("siteKey", siteKey);

		CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
		HttpResponse httpResponse = client.execute(theGet);

		if (httpResponse.getStatusLine().getStatusCode() == 200) {

			String iv = httpResponse.getFirstHeader("iv").getValue();
			String oneTimeKey = httpResponse.getFirstHeader("oneTimeKey").getValue();
			byte[] encryptedData = EntityUtils.toByteArray(httpResponse.getEntity());

			String data = new String(encryptedData, "ISO-8859-1");

			JSONArray arr = JSONArray.fromObject(data);

			byte[] b = new byte[arr.size()];
			for (int x = 0; x < b.length; x++) {
				b[x] = (byte) arr.getInt(x);
			}

			byte[] oneTimeKeyBytes = Base64.getDecoder().decode(oneTimeKey);

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			SecretKeySpec skeySpec = new SecretKeySpec(encryptionKey.getBytes(), "AES");
			IvParameterSpec iv2 = new IvParameterSpec(Base64.getDecoder().decode(iv));
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv2);
			byte[] decryptedOneTimeKey = cipher.doFinal(oneTimeKeyBytes);

			Cipher cipher2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
			SecretKeySpec skeySpec2 = new SecretKeySpec(decryptedOneTimeKey, "AES");
			cipher2.init(Cipher.DECRYPT_MODE, skeySpec2, iv2);
			byte[] decrypted = cipher2.doFinal(b);

			String ptData = new String(decrypted);

			return ptData;
		} else if (httpResponse.getStatusLine().getStatusCode() == 400) { //invalid/missing params

		} else if (httpResponse.getStatusLine().getStatusCode() == 401) { //unauthorized

		} else if (httpResponse.getStatusLine().getStatusCode() == 404) { //not found

		}

		return null;

	}

	static byte[] grabPrivateKey() throws Exception {
		URIBuilder builder = new URIBuilder("https://ocean.cognisantmd.com/svc/v1/site/privateKey");
		HttpGet theGet = new HttpGet(builder.build());
		theGet.addHeader("siteCredential", "88340e79-73c1-437d-a95a-609616840b29");
		theGet.addHeader("siteNum", "5145");
		theGet.addHeader("siteKey", "OCEAN_API_SITEKEY");

		CloseableHttpClient client = HttpClients.custom().disableContentCompression().build();
		HttpResponse httpResponse = client.execute(theGet);

		String entity = EntityUtils.toString(httpResponse.getEntity(), "ISO-8859-1");
		JSONObject key = JSONObject.fromObject("{\"test\": " + entity + "}");

		String keyData = key.getString("test");

		byte[] keyData2 = Base64.getDecoder().decode(keyData);

		String iv = httpResponse.getFirstHeader("iv").getValue();

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		SecretKeySpec skeySpec = new SecretKeySpec("StJoesGuelph20!9".getBytes(), "AES");
		IvParameterSpec iv2 = new IvParameterSpec(Base64.getDecoder().decode(iv));
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv2);
		byte[] decrypted = cipher.doFinal(keyData2);

		return decrypted;

	}

	private static JSONObject decryptPatientData(JSONObject encryptedBlock, String oneTimeKeyEncryptedWithTargetPublicKey) throws Exception {
		JSONObject result = null;

		String decryptedOneTimeKey = getDecryptedOneTimeKey(oneTimeKeyEncryptedWithTargetPublicKey);

		if (decryptedOneTimeKey != null) {
			byte[] oneTimeKeyBytes = Base64.getDecoder().decode(decryptedOneTimeKey);
			byte[] ptdata = Base64.getDecoder().decode(encryptedBlock.getString("data"));

			//decrypt it
			Cipher cipher2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec iv2 = new IvParameterSpec(Base64.getDecoder().decode(encryptedBlock.getString("iv")));
			SecretKeySpec skeySpec = new SecretKeySpec(oneTimeKeyBytes, "AES");
			cipher2.init(Cipher.DECRYPT_MODE, skeySpec, iv2);
			byte[] decrypted = cipher2.doFinal(ptdata);
			String ptData = new String(decrypted);

			MiscUtils.getLogger().info("ptData=" + ptData);
		
			JSONObject ptDataObj = JSONObject.fromObject(ptData);
			String ref1 = ptDataObj.getString("ref");
			String extRef1 = ptDataObj.getString("externalPatientRef");

			JSONObject ptUpdate = ptDataObj.getJSONObject("ptUpdate");

			if (ptUpdate != null) {
				JSONObject completedForms = ptUpdate.getJSONObject("completedForms");

				if (completedForms != null) {
					JSONObject rehabForm = completedForms.getJSONObject("outpatient_rehabilit9000");

					if (rehabForm != null) {
						result = new JSONObject();
						result.put("reasonforreferral",rehabForm.getString("reasonforreferral"));
						result.put("refSource",rehabForm.getString("refSource"));
						result.put("referralService",rehabForm.getString("referralService"));
						result.put("case",rehabForm.getString("Case"));

					}
				}
			}

		}

		return result;
	}

	private static String getDecryptedOneTimeKey(String oneTimeKeyEncryptedWithTargetPublicKey) throws Exception {
		byte[] privateKey = grabPrivateKey();
		String s = new String(privateKey, StandardCharsets.US_ASCII);
		String[] pkParts = s.split("\\|");
		for (String part : pkParts) {
			JSONObject k = JSONObject.fromObject(part);
			String modulus = k.getString("n");
			String publicExp = k.getString("e");
			String privateExp = k.getString("d");
			String prime1 = k.getString("p");
			String prime2 = k.getString("q");
			String exponent1 = k.getString("dmp1");
			String exponent2 = k.getString("dmq1");
			String coefficient = k.getString("coeff");

			RSAPrivateCrtKeySpec keySpec = new RSAPrivateCrtKeySpec(new BigInteger(modulus, 16), new BigInteger(publicExp, 16), new BigInteger(privateExp, 16), new BigInteger(prime1, 16), new BigInteger(prime2, 16), new BigInteger(exponent1, 16), new BigInteger(exponent2, 16), new BigInteger(coefficient, 16));

			PrivateKey privateKey2 = null;
			KeyFactory keyFactory = null;
			try {
				keyFactory = KeyFactory.getInstance("RSA");
			} catch (NoSuchAlgorithmException e) {
				MiscUtils.getLogger().error("Error",e);
			}
			try {
				privateKey2 = keyFactory.generatePrivate(keySpec);
			} catch (InvalidKeySpecException e) {
				MiscUtils.getLogger().error("Error",e);
			}

			try {
				byte[] oneTime = Base64.getDecoder().decode(oneTimeKeyEncryptedWithTargetPublicKey.getBytes("ISO-8859-1"));

				Cipher decrypt = Cipher.getInstance("RSA");
				decrypt.init(Cipher.DECRYPT_MODE, privateKey2);
				String decryptedMessage = new String(decrypt.doFinal(oneTime), StandardCharsets.UTF_8);

				if (decryptedMessage != null) {
					return decryptedMessage;
					
				}
			} catch (Exception e) {
				//suppress this..it happens as we loop through them
				//	MiscUtils.getLogger().error(e.getMessage());
			}

		}
		return null;
	}
	
	
}
