/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.report.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.casemgmt.dao.CaseManagementNoteDAO;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.EFormDao;
import org.oscarehr.common.dao.EFormDataDao;
import org.oscarehr.common.dao.EFormValueDao;
import org.oscarehr.common.dao.LookupListDao;
import org.oscarehr.common.dao.LookupListItemDao;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.dao.OscarAppointmentDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.EForm;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.LookupList;
import org.oscarehr.common.model.LookupListItem;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

/*
 * Physiotherapists
Janice Centurione
Kathleen Penner
Laurie Rowbotham
Lesley Burton
Steve Bruner


Physiotherapy Assistants
Irene Pica
Chelsea Balsom
Leanne Simkin

Occupational Therapists
Anne Wylie


 */
public class NACRSExportReportAction extends DispatchAction {

	Logger logger = MiscUtils.getLogger();
	
	OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
	LookupListDao lookupListDao = SpringUtils.getBean(LookupListDao.class);
	LookupListItemDao lookupListItemDao = SpringUtils.getBean(LookupListItemDao.class);
	DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
	CaseManagementNoteDAO noteDao = SpringUtils.getBean(CaseManagementNoteDAO.class);
	ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
	EFormDao eformDao = SpringUtils.getBean(EFormDao.class);
	EFormDataDao eformDataDao = SpringUtils.getBean(EFormDataDao.class);
	EFormValueDao eformValueDao = SpringUtils.getBean(EFormValueDao.class);
	MeasurementDao measurementDao = SpringUtils.getBean(MeasurementDao.class);
	
	@Override
	protected ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		//find all discharges between these 2 dates
		Date startDate = null;
		Date endDate = null;
		
		String type = request.getParameter("type");
		try {
			startDate = formatter.parse(request.getParameter("startDate"));
			endDate = formatter.parse(request.getParameter("endDate"));
			Calendar cal = Calendar.getInstance();
			cal.setTime(endDate);
			cal.add(Calendar.DAY_OF_YEAR,1);
			endDate = cal.getTime();
		}catch(NumberFormatException e) {
			logger.warn("Error parsing date",e);
			return null;
		}
		
		if(startDate == null || endDate == null) {
			return null;
		}
		
		Integer reasonCodeId = null;
		Integer initialVisitReasonCodeId = null;
		
		LookupList ll = lookupListDao.findByName("reasonCode");
		if(ll != null) {
			LookupListItem lli = lookupListItemDao.findByLookupListIdAndValue(ll.getId(), "discharge_visit");
			if(lli != null) {
				reasonCodeId = lli.getId();
			}
			LookupListItem lli2 = lookupListItemDao.findByLookupListIdAndValue(ll.getId(), "initial_visit");
			if(lli2 != null) {
				initialVisitReasonCodeId = lli2.getId();
			}
		}
		if(reasonCodeId == null) {
			logger.warn("must setup reasonCode lookup list with discharge_visit");
			return null;
		}
		
		
		
		HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("Patients");
        
        int rowNum = 0;
		
        HSSFRow headerRow = sheet.createRow(rowNum++);
        int columnNum = 0;
        
        headerRow.createCell(columnNum++).setCellValue("Chart No");
        headerRow.createCell(columnNum++).setCellValue("HCN");
        headerRow.createCell(columnNum++).setCellValue("HCN Issuer");
        headerRow.createCell(columnNum++).setCellValue("Postal Code");
        headerRow.createCell(columnNum++).setCellValue("Gender");
        headerRow.createCell(columnNum++).setCellValue("Birth Date");
        headerRow.createCell(columnNum++).setCellValue("Birth Date Estimated");
        headerRow.createCell(columnNum++).setCellValue("MIS FC Account Code");
        headerRow.createCell(columnNum++).setCellValue("Referral Source");
        headerRow.createCell(columnNum++).setCellValue("Institution From");
        headerRow.createCell(columnNum++).setCellValue("Referral Date");
        headerRow.createCell(columnNum++).setCellValue("Mode of Visit");
        headerRow.createCell(columnNum++).setCellValue("Discharge Date");
        headerRow.createCell(columnNum++).setCellValue("Visit Disposition");
        headerRow.createCell(columnNum++).setCellValue("Main Problem");
        headerRow.createCell(columnNum++).setCellValue("Other Problem");
        headerRow.createCell(columnNum++).setCellValue("Other Problem Prefix");
        headerRow.createCell(columnNum++).setCellValue("Total Attendances - Occupational Therapist");
        headerRow.createCell(columnNum++).setCellValue("Service Duration - Occupational Therapist");
        headerRow.createCell(columnNum++).setCellValue("Total Attendances - Occupational Therapist Assistant");
        headerRow.createCell(columnNum++).setCellValue("Service Duration - Occupational Therapist Assistant");
        headerRow.createCell(columnNum++).setCellValue("Total Attendances - Physio Therapist");
        headerRow.createCell(columnNum++).setCellValue("Service Duration - Physio Therapist");
        headerRow.createCell(columnNum++).setCellValue("Total Attendances - Physio Therapist Assistant");
        headerRow.createCell(columnNum++).setCellValue("Service Duration - Physio Therapist Assistant");
        headerRow.createCell(columnNum++).setCellValue("Total Attendances - Speech Language Pathologist");
        headerRow.createCell(columnNum++).setCellValue("Service Duration - Speech Language Pathologist");
        headerRow.createCell(columnNum++).setCellValue("Total Attendances - Communication Disorders Assistant");
        headerRow.createCell(columnNum++).setCellValue("Service Duration - Communication Disorders Assistant");
        
        headerRow.createCell(columnNum++).setCellValue("Outcome Measures Scores");
        headerRow.createCell(columnNum++).setCellValue("Date of First Vist");
        headerRow.createCell(columnNum++).setCellValue("Reason for Discharge");
        
        
		List<Appointment> appts = appointmentDao.findByDateRangeAndReasonCodeAndType(startDate, endDate, reasonCodeId, type);
				
		for(Appointment appt: appts) {
			
			//skip if this is a cancelled appointment
			if(appt.getStatus().equals("C")) {
				continue;
			}
			
			//find the appts leading up to this one including the initial visit
			Appointment initialVisit = appointmentDao.findLastApptByReasonBefore(appt.getDemographicNo(),appt.getType(),initialVisitReasonCodeId,appt.getAppointmentDate());
			if(initialVisit == null) {
				logger.info("skipping appt because no initial visit");
				continue;
			}
			
			//get appts in between - all Here visits
			List<Appointment> apptsInBetween = appointmentDao.findAllAppointmentsInBetween(appt.getDemographicNo(), initialVisit.getStartTimeAsFullDate() , appt.getStartTimeAsFullDate());
			
			
			List<Appointment> allApptsToCount = new ArrayList<Appointment>();
			allApptsToCount.add(initialVisit);
			allApptsToCount.addAll(apptsInBetween);
			allApptsToCount.add(appt);
			
			
			columnNum = 0;
			//check that appt isn't cancelled
			HSSFRow row = sheet.createRow(rowNum++);
	       // row.createCell(columnNum++).setCellValue(appt.getDemographicNo());
	       // row.createCell(columnNum++).setCellValue(appt.getName());
			
	        int demographicNo = appt.getDemographicNo();
	        Demographic demographic = demographicDao.getDemographicById(demographicNo);
	
	        //chart number
	        row.createCell(columnNum++).setCellValue(demographic.getDemographicNo());
	        
	        //HIN
	        row.createCell(columnNum++).setCellValue(demographic.getHin());
	        
	        //HC province
	        row.createCell(columnNum++).setCellValue(demographic.getHcType());
	        
	        //postal code
	        row.createCell(columnNum++).setCellValue(demographic.getPostal());
	        
	        //gender
	        row.createCell(columnNum++).setCellValue(demographic.getSex());
	        
	        //birth day
	        row.createCell(columnNum++).setCellValue(demographic.getFormattedDob());
	        
	        //birthday is estimated
	        DemographicExt dobEstExt = demographicExtDao.getDemographicExt(demographicNo, "dobEstimated");
	        if(dobEstExt != null) {
	        	if("yes".equals(dobEstExt.getValue())) {
	        		row.createCell(columnNum++).setCellValue("yes");
	        	} else {
	        		row.createCell(columnNum++).setCellValue("no");
	        	}
	        } else {
	        	row.createCell(columnNum++).setCellValue("");
	        }
	        
	        //MIS acct code
	        DemographicExt misFcExt = demographicExtDao.getDemographicExt(demographicNo, "misFcAccountCode");
	        if(misFcExt != null) {	
	        	row.createCell(columnNum++).setCellValue(misFcExt.getValue());
	        } else {
	        	row.createCell(columnNum++).setCellValue("");
	        }
	        
	        //referral source prior to ambulatory care visit 
	     /*
	        DemographicExt refSrcExt = demographicExtDao.getDemographicExt(demographicNo, "referralSourcePriorToAmbulatoryCareVisit");
	        if(refSrcExt != null) {	
	        	row.createCell(columnNum++).setCellValue(refSrcExt.getValue());
	        } else {
	        	row.createCell(columnNum++).setCellValue("");
	        }
	       */ 
	        if(!StringUtils.isEmpty(appt.getReferralSource())) {	
	        	row.createCell(columnNum++).setCellValue(appt.getReferralSource());
	        } else {
	        	row.createCell(columnNum++).setCellValue("");
	        }
	        
	        //institution from 
	        /*
	        DemographicExt insFromExt = demographicExtDao.getDemographicExt(demographicNo, "institutionFrom");
	        if(insFromExt != null) {	
	        	row.createCell(columnNum++).setCellValue(insFromExt.getValue());
	        } else {
	        	row.createCell(columnNum++).setCellValue("");
	        }
	        */
	        
	        if(!StringUtils.isEmpty(appt.getInstitutionFrom())) {	
	        	row.createCell(columnNum++).setCellValue(appt.getInstitutionFrom());
	        } else {
	        	row.createCell(columnNum++).setCellValue("");
	        }
	        
	        
	        //date ready for rehab / referral date  
	        /*
	        DemographicExt refDateExt = demographicExtDao.getDemographicExt(demographicNo, "referralDate");
	        if(refDateExt != null) {	
	        	row.createCell(columnNum++).setCellValue(refDateExt.getValue());
	        } else {
	        	row.createCell(columnNum++).setCellValue("");
	        }*/
	        
	        if(!StringUtils.isEmpty(appt.getReferralDate())) {	
	        	row.createCell(columnNum++).setCellValue(appt.getReferralDate());
	        } else {
	        	row.createCell(columnNum++).setCellValue("");
	        }
	        
	        
	        //mode of visit
	        //find a note associated with this appt
	        List<CaseManagementNote> notes = noteDao.getMostRecentNotesByAppointmentNo(appt.getId());
	        if(notes.isEmpty()) {
	        	row.createCell(columnNum++).setCellValue("");
	        } else {
	        	row.createCell(columnNum++).setCellValue(notes.get(0).getEncounter_type());
	        }
	        
	        //discharge date
	        row.createCell(columnNum++).setCellValue(formatter.format(appt.getAppointmentDate()));
	        
	        //visit disposition
	        row.createCell(columnNum++).setCellValue(appt.getVisitDisposition());
	        
	        //main problem
	        row.createCell(columnNum++).setCellValue(appt.getMainProblem());
		       
	        //other problem(s)
	        row.createCell(columnNum++).setCellValue(appt.getOtherProblem());
			   
	        //other problems prefix
	        row.createCell(columnNum++).setCellValue(appt.getOtherProblemPrefix());
			
	        //total OT
	        row.createCell(columnNum++).setCellValue(getProviderTeamCount("OT",allApptsToCount));
			
	        //service duration OT
	        row.createCell(columnNum++).setCellValue(getProviderTeamDurationCount("OT",allApptsToCount));
			
	        
	        //total OT ASst
	        row.createCell(columnNum++).setCellValue(getProviderTeamCount("OTASST",allApptsToCount));
	        //service duration OT Asst
	        row.createCell(columnNum++).setCellValue(getProviderTeamDurationCount("OTASST",allApptsToCount));
	        
	        //total PT
	        row.createCell(columnNum++).setCellValue(getProviderTeamCount("PT",allApptsToCount));
	        //service duration PT
	        row.createCell(columnNum++).setCellValue(getProviderTeamDurationCount("PT",allApptsToCount));
	        
	        //total PT Asst
	        row.createCell(columnNum++).setCellValue(getProviderTeamCount("PTASST",allApptsToCount));
	     	//service duration PT Asst
	        row.createCell(columnNum++).setCellValue(getProviderTeamDurationCount("PTASST",allApptsToCount));
	        
	        //total SLP
	        row.createCell(columnNum++).setCellValue(getProviderTeamCount("SLP",allApptsToCount));
	        //total SLP duration
	        row.createCell(columnNum++).setCellValue(getProviderTeamDurationCount("SLP",allApptsToCount));
	        
	        //total CDA
	        row.createCell(columnNum++).setCellValue(getProviderTeamCount("CDA",allApptsToCount));
	        //total CDA duration
	        row.createCell(columnNum++).setCellValue(getProviderTeamDurationCount("CDA",allApptsToCount));
	        
	
	        Integer score = null;
	        //TODO: fix name here
	        EForm eform = eformDao.findByName("Outpatient Rehab Discharge Form");
	        if(eform != null) {
	        	/*
	        	TreeSet<Integer> fids = new TreeSet<Integer>();
	        	fids.add(eform.getId());
	        	
	        	Date from = initialVisit.getAppointmentDate();
	        	
	        	Calendar cal = Calendar.getInstance();
	        	cal.setTime(appt.getAppointmentDate());
	        	cal.add(Calendar.DAY_OF_YEAR,1);
	        	Date to = cal.getTime();
	        	
	        	List<EFormData> forms = eformDataDao.findByFidsAndDatesByDemographic(fids, demographic.getId(), from,to);
	        	*/
	        	List<EFormData> forms = eformDataDao.findByDemographicIdAndFormName(demographicNo, "Outpatient Rehab Discharge Form 2");
	        	
	        	//logger.info("found " + forms.size() + " with eformid-" + eform.getId() + ",from=" + from + ", to=" + to);
	        	
	        	for(EFormData efd:forms) {
	        		//check if discharge date matches
	        		EFormValue efv = eformValueDao.findByFormDataIdAndKey(efd.getId(), "discharge_date");
	        		if(efv != null) {
	        			String eformDd = efv.getVarValue();
	        			String dd = formatter.format(appt.getAppointmentDate());
	        			if(eformDd.equals(dd)) {
	        				logger.info("Found a discharge form");
	        				score = 0;
	        				score += addToScore(efd,1);
	            			score += addToScore(efd,2);
	            			score += addToScore(efd,3);
	            			score += addToScore(efd,4);
	        			}
	        		}
	        	}
	        	
	        }
	        row.createCell(columnNum++).setCellValue(score != null ? String.valueOf(score) : "");
	        
	        
	        //date of first visit
	        String firstDate = formatter.format(initialVisit.getAppointmentDate());
	        row.createCell(columnNum++).setCellValue(firstDate);
	        
	        //reason for discharge
	        row.createCell(columnNum++).setCellValue(appt.getReasonForDischarge());
	        
		}
     
        
	
		try {    
			response.setContentType("application/octet-stream");
	        response.setHeader("Content-Disposition", "attachment; filename=\"OSCAR-NACRS.xls\"");

		    wb.write(response.getOutputStream());
		} catch(Exception e) {
		    MiscUtils.getLogger().error("Error", e);   
		}

		return null;
	}


	private Integer getProviderTeamCount(String team, List<Appointment> appts) {
		List<String> providers = providerDao.getProvidersInTeam(team);
		if(providers.size() == 0) {
			return 0;
		}
		
		int total = 0;
		for(Appointment appt: appts) {
			if(appt.getProviderNo() != null && providers.contains(appt.getProviderNo()) ) {
				total++;
			}
		}
		return total;
	}
	
	private Integer getProviderTeamDurationCount(String team, List<Appointment> appts) {
		List<String> providers = providerDao.getProvidersInTeam(team);
		if(providers.size() == 0) {
			return 0;
		}
		
		int total = 0;
		for(Appointment appt: appts) {
			if(appt.getProviderNo() != null && providers.contains(appt.getProviderNo()) ) {
				long diffInMillies = Math.abs(appt.getEndTime().getTime() - appt.getStartTime().getTime());
				long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
				total += (diff+1);
			}
		}
		
		//search SRM
		Appointment ia = appts.get(0);
		Appointment da = appts.get(appts.size()-1);
		logger.info("start date = " + ia.getAppointmentDate());
		logger.info("end date = " + da.getAppointmentDate());
		

		List<Measurement> measurements = measurementDao.findByDemographicNoTypeAndCreatedDateRange(da.getDemographicNo(), "SRM",ia.getAppointmentDate(),da.getAppointmentDate());
		for(Measurement m: measurements) {
			//is this measurement recorded by someone in this team
			if(providers.contains(m.getProviderNo())) {
			
				String srmAsStr = m.getDataField();
				try {
					Integer srm = Integer.parseInt(srmAsStr);
					total += srm;
				} catch(NumberFormatException e) {
					logger.warn("Can't parse SRM value. (" + srmAsStr + ") measurement.id  = " + m.getId()); 
				}
			}
		}
		
		return total;
	}
	
	//TODO: other conditions?*
	private int addToScore(EFormData efd, int index) {
		EFormValue efv = eformValueDao.findByFormDataIdAndKey(efd.getId(), "score_type"+index);
		if(efv != null) {
			EFormValue efvStart = eformValueDao.findByFormDataIdAndKey(efd.getId(), "score"+index+"_start");
			EFormValue efvEnd = eformValueDao.findByFormDataIdAndKey(efd.getId(), "score"+index+"_end");
			
			if(efvStart != null && efvEnd != null) {
				int start = Integer.parseInt(efvStart.getVarValue());
				int end = Integer.parseInt(efvEnd.getVarValue());
				
				logger.info("score += fdid " + efd.getId());
				
				return (end - start);
			}
		}
		
		return 0;
	}
}
