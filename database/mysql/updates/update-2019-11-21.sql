alter table appointment add referralSource varchar(255);
alter table appointment add referralDate varchar(255);
alter table appointment add institutionFrom varchar(255);

alter table appointmentArchive add referralSource varchar(255);
alter table appointmentArchive add referralDate varchar(255);
alter table appointmentArchive add institutionFrom varchar(255);
