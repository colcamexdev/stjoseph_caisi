alter table appointment add visitDisposition varchar(100);
alter table appointment add reasonForDischarge varchar(100);
alter table appointment add mainProblem varchar(100);
alter table appointment add otherProblem varchar(100);
alter table appointment add otherProblemPrefix varchar(100);

alter table appointmentArchive add visitDisposition varchar(100);
alter table appointmentArchive add reasonForDischarge varchar(100);
alter table appointmentArchive add mainProblem varchar(100);
alter table appointmentArchive add otherProblem varchar(100);
alter table appointmentArchive add otherProblemPrefix varchar(100);


